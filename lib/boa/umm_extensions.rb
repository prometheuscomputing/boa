require 'appellation'
module Qname
  def qualified_name
    c = container
    # associations don't have a name, so calling name or guid returns nil for them
    # TODO Better solution? -CD
    (c ? (c.qualified_name + '::') : '') + "#{(name || guid)}"
  end
end

module UmlMetamodel
  class Classifier
    include Qname
    derived_attribute :qualified_name, String
  end
  class Class
    derived_attribute :qualified_name, String
  end
  class Package
    include Qname
    derived_attribute :qualified_name, String
  end
  class Association
    include Qname
    derived_attribute :qualified_name, String
  end
  class Stereotype
    include Qname
    derived_attribute :qualified_name, String
  end
  
  class AppliedStereotype
    derived_attribute :qualified_name, String
    def qualified_name
      io = instance_of
      "Applied_#{(io ? io.qualified_name : 'Stereotype_ORPHAN')}"
    end
  end
  
  class AppliedTag
    derived_attribute :qualified_name, String
    def qualified_name
      io = instance_of
      "Applied_#{(io ? io.qualified_name : 'Tag_ORPHAN')}::#{value}"
    end
  end
  
  class EnumerationLiteral
    derived_attribute :qualified_name, String
    def qualified_name
      e = enumeration
      (e ? (e.qualified_name + '::') : '') + name
    end
  end
  
  class Project
    derived_attribute :qualified_name, String
    def qualified_name
      name
    end
  end
  
  class Property
    derived_attribute :qualified_name, String
    def qualified_name
      o = owner
      (o ? (o.qualified_name + '::') : '') + name.to_s
    end
  end
  
  class Tag
    derived_attribute :qualified_name, String
    def qualified_name
      s = stereotype
      (s ? (s.qualified_name + '::') : 'Tag_ORPHAN') + '::' + name
    end
  end
end
  