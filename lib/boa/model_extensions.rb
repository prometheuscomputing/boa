require_relative 'umm_extensions'
module UmlMetamodel
  class Classifier
    alias_attribute(:description, :documentation)
    # derived_attribute :description, String
    # def description
    #   documentation
    # end
    
    def base_uml
      self
    end
    
    def all_properties
      parents.collect { |parent| parent.all_properties }.flatten + properties
    end
  end
end

module Boa
  class ClassifierRestriction
    def after_change_tracker_create
      self.author = Ramaze::Current.session[:user]
      save
      super
    end
    
    derived_attribute :qualified_name, String
    def qualified_name
      # Use description as name?
      # ^^ No. The description information is available / displayable elsewhere.
      # Only show UML base? Then base_uml needed to differentiate (CR can restrict another CR without base)
      if base_uml
        "Restriction on #{_qualified_base_name}"
      else
        'NO UML BASE'
      end
    end
    
    def _qualified_base_name
      return unless base
      base.is_a?(UmlMetamodel::Classifier) ? base.qualified_name : base._qualified_base_name
    end
    
    def base_uml
      return unless base
      base.base_uml
    end
    
    def available_properties
      base_uml.all_properties
    end
    
    def available_associations
      assocs = available_properties.select { |prop| prop.association_count == 1 }
      # Reject inverse associations (no part of original BICEPS)
      assocs.reject { |prop| prop.name =~ /\Ainv_/ }
    end
    
    def available_attributes
      available_properties.select { |prop| prop.association_count == 0 }
    end
    
    # FIXME not everything is guaranteed to have a rolename...I think.  Hence the #compact below.
    derived_attribute :available_property_names, String
    def available_property_names
      available_properties.collect(&:name).compact.sort.join(', ')
    end
    
    # FIXME not everything is guaranteed to have a rolename...I think.  Hence the #compact below.
    derived_attribute :available_association_names, String
    def available_association_names
      '<strong>Restrictable Base Properties:</strong> ' + available_associations.collect(&:name).compact.sort.join(', ')
    end
    
    derived_attribute :available_attribute_names, String
    def available_attribute_names
      '<strong>Restrictable Base Properties:</strong> ' + available_attributes.collect(&:name).collect(&:to_s).sort.join(', ')
    end
    
    derived_attribute :available_attribute_names, String
    def available_attribute_names
      restricts.available_attribute_names
    end
    
    # Define co_owners derived association ------------------------------------
    base_methods = [
     :base_type,
     :type_of_base,
     :remove_base,
     :remove_from_base,
     :base_remove,
     :remove_all_base,
     :base_remove_all,
     :base,
     # :base_unassociated,
     :base_count,
     # :base_unassociated_count,
     # :base_unassociated_query,
     # :unassoc_get_many_query_base,
     :base_add,
     :base=,
     :base_is_polymorphic?,
     :base_info,
     :base_opp_info,
     # :base_query,
     # :get_many_query_base
    ]
    derived_association :allowed_base, 'Boa::RestrictionBase', :type => :many_to_one
    base_methods.each { |m| alias_method("allowed_#{m}".to_sym, m) }
    def allowed_base_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      puts Rainbow(filter.inspect).cyan
      caller[0..4].each {|x| puts x}; puts
      ba = base_unassociated(filter, limit, offset, order)#.reject{ |b| base_chain_ids(b).include?(self.id)}
      # limit ? ba[offset..offset+limit-1] : ba
      puts "Count #{ba.count}"
      ba
    end
    def allowed_base_unassociated_count(*args)    
      allowed_base_unassociated(*args).count
    end
    def base_chain_ids(base)
      return [] unless base
      (base.respond_to?(:base) && base.base) ? [base.id] + base_chain_ids(base.base) : [base.id] 
    end
    
    derived_attribute(:set_tree_view_root, ::NilClass)
    def set_tree_view_root
      puts "CALLED SET TREEVIEWROOT for #{self.class.name}"
      Gui::Controller.session[:tree_view_roots] ||= []
      Gui::Controller.session[:tree_view_roots].unshift(self)
      Gui::Controller.session[:tree_view_roots].uniq!
    end
    
    # This updates the tree view root if changes are saved (it isn't updated correctly otherwise)
    # TODO set_tree_view_root is sometimes called twice. why?
    def after_change_tracker_save
      set_tree_view_root if Gui::Controller.session[:tree_view_roots]&.first == self
      super
    end
    
  end
  
  class PropertyRestriction
    derived_attribute :qualified_name, String
    def qualified_name
      return unless base
      base.qualified_name
    end
  end
  
  class AttributeRestriction
    derived_attribute :available_attribute_names, String
    def available_attribute_names
      restricts.available_attribute_names
    end
    
    # Define co_owners derived association ------------------------------------
    base_methods = [
     :base_type,
     :type_of_base,
     :remove_base,
     :remove_from_base,
     :base_remove,
     :remove_all_base,
     :base_remove_all,
     :base,
     # :base_unassociated,
     :base_count,
     # :base_unassociated_count,
     # :base_unassociated_query,
     # :unassoc_get_many_query_base,
     :base_add,
     :base=,
     :base_is_polymorphic?,
     :base_info,
     :base_opp_info,
     # :base_query,
     # :get_many_query_base
    ]
    derived_association :allowed_base, 'UmlMetamodel::Property', :type => :many_to_one
    base_methods.each { |m| alias_method("allowed_#{m}".to_sym, m) }
    def allowed_base_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      # TODO If newly created restriction, restricts is not defined and self.id is nil -> allowed_base can't be determined 
      return [] unless restricts&.base_uml # TODO Advise user to first choose a base for the associated ClassifierRestriction - how to display?
      
      aa = restricts.available_attributes
      limit ? aa[offset..offset+limit-1] : aa
    end
    def allowed_base_unassociated_count(*args)    
      restricts&.base_uml ? restricts.available_attributes.count : 0
    end
  end
  
  class AssociationRestriction
    derived_attribute :available_association_names, String
    def available_association_names
      restricts.available_association_names
    end
    
    # Define co_owners derived association ------------------------------------
    base_methods = [
     :base_type,
     :type_of_base,
     :remove_base,
     :remove_from_base,
     :base_remove,
     :remove_all_base,
     :base_remove_all,
     :base,
     # :base_unassociated,
     :base_count,
     # :base_unassociated_count,
     # :base_unassociated_query,
     # :unassoc_get_many_query_base,
     :base_add,
     :base=,
     :base_is_polymorphic?,
     :base_info,
     :base_opp_info,
     # :base_query,
     # :get_many_query_base
    ]
    derived_association :allowed_base, 'UmlMetamodel::Property', :type => :many_to_one
    base_methods.each { |m| alias_method("allowed_#{m}".to_sym, m) }
    def allowed_base_unassociated(filter = {}, limit = nil, offset = 0, order = {})
      # TODO If newly created restriction, restricts is not defined and self.id is nil -> allowed_base can't be determined 
      return [] unless restricts&.base_uml # TODO Advise user to first choose a base for the associated ClassifierRestriction - how to display?
      
      aa = restricts.available_associations
      limit ? aa[offset..offset+limit-1] : aa
    end
    def allowed_base_unassociated_count(*args)    
      restricts&.base_uml ? restricts.available_associations.count : 0
    end
  end
end
