module GuiSpec
  class Home
    # This just totally doesn't work well due to limit and pagination....
    def devices(*args)
      args[1] = nil if args[1]
      classifier_restrictions(*args).select do |cr|
        obj = cr.is_a?(Hash) ? cr[:to] : cr
        v = obj.specification_type&.value
        v.to_s =~ /Device/
      end
    end
    def devices_type
      [Boa::ClassifierRestriction]
    end
    def devices_count(*args)
      devices(*args).count
    end
  end
end
