require_relative 'label_procs'
#Homepage

homepage_item :'Boa::ClassifierRestriction', :getter => 'classifier_restrictions'
homepage_item :'Boa::ClassifierRestriction', :getter => 'devices'
require_relative 'devices_hack'
homepage_item :'UmlMetamodel::Class',        :getter => 'classes'
homepage_item :'UmlMetamodel::Package',      :getter => 'packages'
homepage_item :'UmlMetamodel::Interface',    :getter => 'interfaces'
homepage_item :'UmlMetamodel::Datatype',     :getter => 'datatypes'
homepage_item :'UmlMetamodel::Primitive',    :getter => 'primitives'
homepage_item :'UmlMetamodel::Enumeration',  :getter => 'enumerations'
homepage_item :'UmlMetamodel::Project',      :getter => 'projects'


organizer(:Details, Home) {
  # ordered
  view_ref(:Summary, 'devices', :label => 'Device Specifications')
  view_ref(:Summary, 'classifier_restrictions', :label => 'Classifier Restrictions')
  html(:label => 'uml_section', :html => Boa::UML_SECTION)
  view_ref(:Summary, 'projects', :label => 'Projects')
  view_ref(:Summary, 'packages', :label => 'Packages')
  view_ref(:Summary, 'classes', :label => 'Classes')
  view_ref(:Summary, 'interfaces', :label => 'Interfaces')
  view_ref(:Summary, 'datatypes', :label => 'Datatypes')
  view_ref(:Summary, 'primitives', :label => 'Primitives')
  view_ref(:Summary, 'enumerations', :label => 'Enumerations')
  order('devices', 'classifier_restrictions', 'uml_section', 'projects', 'packages', 'classes', 'interfaces', 'datatypes', 'primitives', 'enumerations')
  disabled_widgets = ['projects', 'packages', 'classes', 'interfaces', 'datatypes', 'primitives', 'enumerations']
  disable_creation(*disabled_widgets)
  disable_deletion(*disabled_widgets)
  # reorder('classifier_restrictions', 'uml_section', 'projects', 'packages', 'classes', 'interfaces','datatypes', 'primitives','enumerations', :to_end => true)
}
