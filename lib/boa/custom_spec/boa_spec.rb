require_relative 'label_procs'

collection(:Summary, Boa::RestrictionBase) do
  string('qualified_name', :search_filter => :disabled)
  string('description')
  reorder('qualified_name', 'description', :to_beginning => true)
end

organizer(:Details, Boa::ClassifierRestriction) do
  view_ref(:Author, 'author', :label => 'Author')
  disable('author')
  hide('association_restriction')
  view_ref(:Summary,'allowed_base', :label => 'Restriction Base')
  hide('base')
  relabel('restrictions', 'Derivative Restrictions')
  button('set_tree_view_root', label: 'Tree View:', button_text: 'Set as Root', save_page: true)
  html(:label => 'meta_section', :html => Boa::META_SECTION)
  reorder('description', 'allowed_base', 'properties', 'restrictions', 'set_tree_view_root', 'meta_section', 'specification_type', :to_beginning => true)
  
  # Include update & creation datetime, not editable
  timestamp('created_at', :disabled => true)
  timestamp('updated_at', :disabled => true)
end

organizer(:Author, Gui_Builder_Profile::Person) do
  string('first_name', :label => 'First Name')
  string('last_name', :label => 'Last Name')
  string('email', :label => 'Email')
end

organizer(:Details, Boa::AttributeRestriction) do
  # message('available_attribute_names')
  view_ref(:Summary,'allowed_base', :label => 'Base Property')
  hide('base')
  reorder('description', 'lower','upper', 'available_association_names', 'allowed_base', :to_beginning => true)
end

organizer(:Details, Boa::AssociationRestriction) do
  # message('available_association_names')
  view_ref(:Summary,'allowed_base', :label => 'Base Property')
  hide('base')
  reorder('description', 'lower','upper', 'available_association_names', 'allowed_base', :to_beginning => true)
end

collection(:Summary, Boa::AssociationRestriction) do
  string('qualified_name')
  reorder('qualified_name', :to_beginning => true)
end
