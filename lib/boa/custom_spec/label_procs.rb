module Boa
  UML_SECTION = proc do
    haml = <<-EOS
    :sass
      .label_style
        // :text-align center
        :display block
        :font-size 16px
        :background-color #053aaa
        :border-radius 5px
        color: white
        text-align: center
        text-decoration: none
        margin-left: 20px

    %div.label_style
      UML
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  end
  
  META_SECTION = proc do
    haml = <<-EOS
    :sass
      .label_style
        // :text-align center
        :display block
        :font-size 16px
        :background-color #053aaa
        :border-radius 5px
        color: white
        text-align: center
        text-decoration: none
        margin-top: 40px
        margin-bottom: 20px

    %div.label_style
      Metadata
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  end
  
  LINK_LABEL = proc do
    haml = <<-EOS
    :sass
      $color:   #84f9eb

      .div_style
        // :text-align center
        :display block
        :font-size 16px
        :background-color #053aaa
        :border-radius 5px
        a
          color: $color
          text-align: center
          text-decoration: none
          margin-left: 20px

          &:visited
            color: #9cd5f3
          &:hover
            color: $color

    %div.div_style
      %a{:href => 'https://example.org', :target=> '_blank'} Example (click to visit)
    EOS
    Haml::Engine.new(haml.reset_indentation).render(self)
  end
end
