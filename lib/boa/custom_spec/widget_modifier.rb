require 'gui_builder/widget_controller/abstract_view_controller'

module Gui::Widget
  
  class AbstractViewController
    
    # Hack to disable creation of UmlMetamodel classes as ClassifierRestriction#base
    alias_method :original_create_constructable_list, :create_constructable_list
    def create_constructable_list(parent_obj, getter, klass, through_klass = nil)
      constructors = original_create_constructable_list(parent_obj, getter, klass, through_klass)
      constructors.reject { |c| c[:new][:name].deconstantize == "UmlMetamodel" }
    end
        
  end
end
