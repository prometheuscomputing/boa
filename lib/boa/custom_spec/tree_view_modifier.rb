require 'gui_site/tree_viewer'

module Gui::TreeViewer
  
  class TreeViewNode
    
    # Configure entries of tree view to display '<Object Info>' [<Class name> (on <UML base>)]
    # @Override
    def set_tree_view_display_name
      obj = self.dom_obj
      klass_name = obj.class.name
      # Use qualified name for UMM elements (if added as RestrictionBase for AssociationRestriction)
      if klass_name.deconstantize == "UmlMetamodel"
        return obj.respond_to?(:qualified_name) ? obj.qualified_name : klass_name.demodulize 
      end
      
      obj_info = obj.description if obj.respond_to?(:description)
      obj_info ||= obj.value if obj.respond_to?(:value)
      obj_info &&= "'#{obj_info}'"
      
      klass_name = klass_name.demodulize
      if obj.respond_to?(:_qualified_base_name)
        klass_name << " on #{obj._qualified_base_name}" if obj._qualified_base_name
      elsif obj.respond_to?(:qualified_name) && obj.qualified_name
        klass_name << " on #{obj.qualified_name}"
      end 
      if obj_info
        klass_name &&= "[#{klass_name}]"
      end
      
      [obj_info, klass_name].join(' ').strip
    end
    
    def parent_objects
      parent ? [parent.dom_obj] + parent.parent_objects : []
    end 
      
  end
  
  class TreeViewTree
    
    # Hack to include AssociationRestriction -> RestrictionBase relations into tree view
    alias_method :original_get_ordered_child_data, :get_ordered_child_data
    def get_ordered_child_data(node)
      return [] if node.dom_obj.is_a?(UmlMetamodel::Element)
      
      (original_get_ordered_child_data(node) << get_type_restrictions(node)).compact
    end
    
    def get_type_restrictions(node)
      dom_obj = node.dom_obj
      return unless dom_obj.is_a?(Boa::AssociationRestriction) && dom_obj.type_restrictions.any?
      
      restrictions = dom_obj.type_restrictions
      # Prevent infinite loops if ClassR -> AssocR -> [...] -> same ClassR. Should this be possible to create in the first place?
      diff = restrictions - node.parent_objects
      puts "Warning: Loop in tree view nodes found! For #{node.set_tree_view_display_name}" unless restrictions == diff
      
      [:type_restrictions, {:value => diff, :data => dom_obj.type_restrictions_info}]
    end
    
    # Configure tree building so that nodes are created even if node for dom_obj already there
    # -> nodes are not added multiple times if one dom_obj referenced twice  
    def build_down_from(node)#, bottom_graph=TreeViewTree.new())
      add_node(node)
      # child_data = node.compositions
      child_data = get_ordered_child_data(node)
      child_data.each do |getter, info_hash|
        label      = (info_hash[:data][:display_name] || getter.to_s).downcase
        child_objs = info_hash[:value]
        child_objs.each do |child_obj|
          # puts "About to get child node for #{node.unique_name} -- #{child_obj.class}"
          if child_obj.is_a? Sequel::Model
            # child_node = @nodes.find{|n| n.dom_obj == child_obj} || TreeViewNode.new(child_obj, label, node)
            child_node = TreeViewNode.new(child_obj, label, node)
            child_node.parent ||= node 
            child_node.label ||= label
            add_node(child_node)
            add_edge(node, child_node)
            build_down_from(child_node)
          else
            child_node = TreeViewNode.new(child_obj, label, node) 
            add_node(child_node)
            add_edge(node, child_node)
          end
        end
      end
    end
    
  end
end
