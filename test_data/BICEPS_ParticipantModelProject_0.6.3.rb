project "BICEPS_ParticipantModelProject", :version => "0.6.3", :generated_at => "2019-08-14 10:12:35 UTC" do
  package "BICEPS_ParticipantModel" do
    applied_stereotype :instance_of => "XML_Schema::XML_Schema" do
      applied_tag :instance_of => "XML_Schema::XML_Schema::prefix", :value => "pm"
    end
    association :properties => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor::AlertSystem", "BICEPS_ParticipantModel::AlertSystemDescriptor::inv_AbstractComplexDeviceComponentDescriptor_AlertSystem"]
    association :properties => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor::Sco", "BICEPS_ParticipantModel::ScoDescriptor::inv_AbstractComplexDeviceComponentDescriptor_Sco"]
    association :properties => ["BICEPS_ParticipantModel::AbstractContextState::Identification", "BICEPS_ParticipantModel::InstanceIdentifier::inv_AbstractContextState_Identification"]
    association :properties => ["BICEPS_ParticipantModel::AbstractContextState::Validator", "BICEPS_ParticipantModel::InstanceIdentifier::inv_AbstractContextState_Validator"]
    association :properties => ["BICEPS_ParticipantModel::AbstractDescriptor::Extension", "ExtensionPoint::ExtensionType::inv_AbstractDescriptor_Extension"]
    association :properties => ["BICEPS_ParticipantModel::AbstractDescriptor::Type", "BICEPS_ParticipantModel::CodedValue::inv_AbstractDescriptor_Type"]
    association :properties => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor::ProductionSpecification", "BICEPS_ParticipantModel::ProductionSpecificationType_1::inv_AbstractDeviceComponentDescriptor_ProductionSpecification"]
    association :properties => ["BICEPS_ParticipantModel::AbstractDeviceComponentState::CalibrationInfo", "BICEPS_ParticipantModel::CalibrationInfo::inv_AbstractDeviceComponentState_CalibrationInfo"]
    association :properties => ["BICEPS_ParticipantModel::AbstractDeviceComponentState::NextCalibration", "BICEPS_ParticipantModel::CalibrationInfo::inv_AbstractDeviceComponentState_NextCalibration"]
    association :properties => ["BICEPS_ParticipantModel::AbstractDeviceComponentState::PhysicalConnector", "BICEPS_ParticipantModel::PhysicalConnectorInfo::inv_AbstractDeviceComponentState_PhysicalConnector"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricDescriptor::BodySite", "BICEPS_ParticipantModel::CodedValue::inv_AbstractMetricDescriptor_BodySite"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricDescriptor::Relation", "BICEPS_ParticipantModel::RelationType_1::inv_AbstractMetricDescriptor_Relation"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricDescriptor::Unit", "BICEPS_ParticipantModel::CodedValue::inv_AbstractMetricDescriptor_Unit"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricState::BodySite", "BICEPS_ParticipantModel::CodedValue::inv_AbstractMetricState_BodySite"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricState::PhysicalConnector", "BICEPS_ParticipantModel::PhysicalConnectorInfo::inv_AbstractMetricState_PhysicalConnector"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricValue::Annotation", "BICEPS_ParticipantModel::AnnotationType_1::inv_AbstractMetricValue_Annotation"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricValue::Extension", "ExtensionPoint::ExtensionType::inv_AbstractMetricValue_Extension"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMetricValue::MetricQuality", "BICEPS_ParticipantModel::MetricQualityType_1::inv_AbstractMetricValue_MetricQuality"]
    association :properties => ["BICEPS_ParticipantModel::AbstractMultiState::Category", "BICEPS_ParticipantModel::CodedValue::inv_AbstractMultiState_Category"]
    association :properties => ["BICEPS_ParticipantModel::AbstractState::Extension", "ExtensionPoint::ExtensionType::inv_AbstractState_Extension"]
    association :properties => ["BICEPS_ParticipantModel::ActivateOperationDescriptor::Argument", "BICEPS_ParticipantModel::ArgumentType_1::inv_ActivateOperationDescriptor_Argument"]
    association :properties => ["BICEPS_ParticipantModel::AlertConditionDescriptor::CauseInfo", "BICEPS_ParticipantModel::CauseInfo::inv_AlertConditionDescriptor_CauseInfo"]
    association :properties => ["BICEPS_ParticipantModel::AlertSystemDescriptor::AlertCondition", "BICEPS_ParticipantModel::AlertConditionDescriptor::inv_AlertSystemDescriptor_AlertCondition"]
    association :properties => ["BICEPS_ParticipantModel::AlertSystemDescriptor::AlertSignal", "BICEPS_ParticipantModel::AlertSignalDescriptor::inv_AlertSystemDescriptor_AlertSignal"]
    association :properties => ["BICEPS_ParticipantModel::AlertSystemState::PresentPhysiologicalAlarmConditions_Attribute", "BICEPS_ParticipantModel::AlertConditionReference::inv_AlertSystemState_PresentPhysiologicalAlarmConditions"]
    association :properties => ["BICEPS_ParticipantModel::AlertSystemState::PresentTechnicalAlarmConditions_Attribute", "BICEPS_ParticipantModel::AlertConditionReference::inv_AlertSystemState_PresentTechnicalAlarmConditions"]
    association :properties => ["BICEPS_ParticipantModel::AlertSystemState::SystemSignalActivation", "BICEPS_ParticipantModel::SystemSignalActivation::inv_AlertSystemState_SystemSignalActivation"]
    association :properties => ["BICEPS_ParticipantModel::AllowedValueType_1::Characteristic", "BICEPS_ParticipantModel::Measurement::inv_AllowedValueType_1_Characteristic"]
    association :properties => ["BICEPS_ParticipantModel::AllowedValueType_1::Identification", "BICEPS_ParticipantModel::InstanceIdentifier::inv_AllowedValueType_1_Identification"]
    association :properties => ["BICEPS_ParticipantModel::AllowedValueType_1::Type", "BICEPS_ParticipantModel::CodedValue::inv_AllowedValueType_1_Type"]
    association :properties => ["BICEPS_ParticipantModel::AnnotationType_1::Extension", "ExtensionPoint::ExtensionType::inv_AnnotationType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::AnnotationType_1::Type", "BICEPS_ParticipantModel::CodedValue::inv_AnnotationType_1_Type"]
    association :properties => ["BICEPS_ParticipantModel::ApprovedJurisdictions::ApprovedJurisdiction", "BICEPS_ParticipantModel::InstanceIdentifier::inv_ApprovedJurisdictions_ApprovedJurisdiction"]
    association :properties => ["BICEPS_ParticipantModel::ArgumentType_1::ArgName", "BICEPS_ParticipantModel::CodedValue::inv_ArgumentType_1_ArgName"]
    association :properties => ["BICEPS_ParticipantModel::BaseDemographics::Extension", "ExtensionPoint::ExtensionType::inv_BaseDemographics_Extension"]
    association :properties => ["BICEPS_ParticipantModel::BatteryDescriptor::CapacityFullCharge", "BICEPS_ParticipantModel::Measurement::inv_BatteryDescriptor_CapacityFullCharge"]
    association :properties => ["BICEPS_ParticipantModel::BatteryDescriptor::CapacitySpecified", "BICEPS_ParticipantModel::Measurement::inv_BatteryDescriptor_CapacitySpecified"]
    association :properties => ["BICEPS_ParticipantModel::BatteryDescriptor::VoltageSpecified", "BICEPS_ParticipantModel::Measurement::inv_BatteryDescriptor_VoltageSpecified"]
    association :properties => ["BICEPS_ParticipantModel::BatteryState::CapacityRemaining", "BICEPS_ParticipantModel::Measurement::inv_BatteryState_CapacityRemaining"]
    association :properties => ["BICEPS_ParticipantModel::BatteryState::Current", "BICEPS_ParticipantModel::Measurement::inv_BatteryState_Current"]
    association :properties => ["BICEPS_ParticipantModel::BatteryState::RemainingBatteryTime", "BICEPS_ParticipantModel::Measurement::inv_BatteryState_RemainingBatteryTime"]
    association :properties => ["BICEPS_ParticipantModel::BatteryState::Temperature", "BICEPS_ParticipantModel::Measurement::inv_BatteryState_Temperature"]
    association :properties => ["BICEPS_ParticipantModel::BatteryState::Voltage", "BICEPS_ParticipantModel::Measurement::inv_BatteryState_Voltage"]
    association :properties => ["BICEPS_ParticipantModel::CalibrationDocumentationType_1::CalibrationResult", "BICEPS_ParticipantModel::CalibrationResultType_1::inv_CalibrationDocumentationType_1_CalibrationResult"]
    association :properties => ["BICEPS_ParticipantModel::CalibrationDocumentationType_1::Documentation", "BICEPS_ParticipantModel::LocalizedText::inv_CalibrationDocumentationType_1_Documentation"]
    association :properties => ["BICEPS_ParticipantModel::CalibrationInfo::CalibrationDocumentation", "BICEPS_ParticipantModel::CalibrationDocumentationType_1::inv_CalibrationInfo_CalibrationDocumentation"]
    association :properties => ["BICEPS_ParticipantModel::CalibrationInfo::Extension", "ExtensionPoint::ExtensionType::inv_CalibrationInfo_Extension"]
    association :properties => ["BICEPS_ParticipantModel::CalibrationResultType_1::Code", "BICEPS_ParticipantModel::CodedValue::inv_CalibrationResultType_1_Code"]
    association :properties => ["BICEPS_ParticipantModel::CalibrationResultType_1::Value", "BICEPS_ParticipantModel::Measurement::inv_CalibrationResultType_1_Value"]
    association :properties => ["BICEPS_ParticipantModel::CauseInfo::Description", "BICEPS_ParticipantModel::LocalizedText::inv_CauseInfo_Description"]
    association :properties => ["BICEPS_ParticipantModel::CauseInfo::Extension", "ExtensionPoint::ExtensionType::inv_CauseInfo_Extension"]
    association :properties => ["BICEPS_ParticipantModel::CauseInfo::RemedyInfo", "BICEPS_ParticipantModel::RemedyInfo::inv_CauseInfo_RemedyInfo"]
    association :properties => ["BICEPS_ParticipantModel::ChannelDescriptor::Metric", "BICEPS_ParticipantModel::AbstractMetricDescriptor::inv_ChannelDescriptor_Metric"]
    association :properties => ["BICEPS_ParticipantModel::ClinicalInfo::Code", "BICEPS_ParticipantModel::CodedValue::inv_ClinicalInfo_Code"]
    association :properties => ["BICEPS_ParticipantModel::ClinicalInfo::Description", "BICEPS_ParticipantModel::LocalizedText::inv_ClinicalInfo_Description"]
    association :properties => ["BICEPS_ParticipantModel::ClinicalInfo::Extension", "ExtensionPoint::ExtensionType::inv_ClinicalInfo_Extension"]
    association :properties => ["BICEPS_ParticipantModel::ClinicalInfo::RelatedMeasurement", "BICEPS_ParticipantModel::RelatedMeasurementType_1::inv_ClinicalInfo_RelatedMeasurement"]
    association :properties => ["BICEPS_ParticipantModel::ClinicalInfo::Type", "BICEPS_ParticipantModel::CodedValue::inv_ClinicalInfo_Type"]
    association :properties => ["BICEPS_ParticipantModel::ClockDescriptor::TimeProtocol", "BICEPS_ParticipantModel::CodedValue::inv_ClockDescriptor_TimeProtocol"]
    association :properties => ["BICEPS_ParticipantModel::ClockState::ActiveSyncProtocol", "BICEPS_ParticipantModel::CodedValue::inv_ClockState_ActiveSyncProtocol"]
    association :properties => ["BICEPS_ParticipantModel::CodedValue::CodingSystemName", "BICEPS_ParticipantModel::LocalizedText::inv_CodedValue_CodingSystemName"]
    association :properties => ["BICEPS_ParticipantModel::CodedValue::ConceptDescription", "BICEPS_ParticipantModel::LocalizedText::inv_CodedValue_ConceptDescription"]
    association :properties => ["BICEPS_ParticipantModel::CodedValue::Extension", "ExtensionPoint::ExtensionType::inv_CodedValue_Extension"]
    association :properties => ["BICEPS_ParticipantModel::CodedValue::Translation", "BICEPS_ParticipantModel::TranslationType_1::inv_CodedValue_Translation"]
    association :properties => ["BICEPS_ParticipantModel::ContainmentTree::Entry", "BICEPS_ParticipantModel::ContainmentTreeEntry::inv_ContainmentTree_Entry"]
    association :properties => ["BICEPS_ParticipantModel::ContainmentTree::Extension", "ExtensionPoint::ExtensionType::inv_ContainmentTree_Extension"]
    association :properties => ["BICEPS_ParticipantModel::ContainmentTreeEntry::Extension", "ExtensionPoint::ExtensionType::inv_ContainmentTreeEntry_Extension"]
    association :properties => ["BICEPS_ParticipantModel::ContainmentTreeEntry::Type", "BICEPS_ParticipantModel::CodedValue::inv_ContainmentTreeEntry_Type"]
    association :properties => ["BICEPS_ParticipantModel::DistributionSampleArrayMetricDescriptor::DistributionRange", "BICEPS_ParticipantModel::Range::inv_DistributionSampleArrayMetricDescriptor_DistributionRange"]
    association :properties => ["BICEPS_ParticipantModel::DistributionSampleArrayMetricDescriptor::DomainUnit", "BICEPS_ParticipantModel::CodedValue::inv_DistributionSampleArrayMetricDescriptor_DomainUnit"]
    association :properties => ["BICEPS_ParticipantModel::DistributionSampleArrayMetricDescriptor::TechnicalRange", "BICEPS_ParticipantModel::Range::inv_DistributionSampleArrayMetricDescriptor_TechnicalRange"]
    association :properties => ["BICEPS_ParticipantModel::DistributionSampleArrayMetricState::MetricValue", "BICEPS_ParticipantModel::SampleArrayValue::inv_DistributionSampleArrayMetricState_MetricValue"]
    association :properties => ["BICEPS_ParticipantModel::DistributionSampleArrayMetricState::PhysiologicalRange", "BICEPS_ParticipantModel::Range::inv_DistributionSampleArrayMetricState_PhysiologicalRange"]
    association :properties => ["BICEPS_ParticipantModel::EnumStringMetricDescriptor::AllowedValue", "BICEPS_ParticipantModel::AllowedValueType_1::inv_EnumStringMetricDescriptor_AllowedValue"]
    association :properties => ["BICEPS_ParticipantModel::ImagingProcedure::AccessionIdentifier", "BICEPS_ParticipantModel::InstanceIdentifier::inv_ImagingProcedure_AccessionIdentifier"]
    association :properties => ["BICEPS_ParticipantModel::ImagingProcedure::Extension", "ExtensionPoint::ExtensionType::inv_ImagingProcedure_Extension"]
    association :properties => ["BICEPS_ParticipantModel::ImagingProcedure::Modality", "BICEPS_ParticipantModel::CodedValue::inv_ImagingProcedure_Modality"]
    association :properties => ["BICEPS_ParticipantModel::ImagingProcedure::ProtocolCode", "BICEPS_ParticipantModel::CodedValue::inv_ImagingProcedure_ProtocolCode"]
    association :properties => ["BICEPS_ParticipantModel::ImagingProcedure::RequestedProcedureId", "BICEPS_ParticipantModel::InstanceIdentifier::inv_ImagingProcedure_RequestedProcedureId"]
    association :properties => ["BICEPS_ParticipantModel::ImagingProcedure::ScheduledProcedureStepId", "BICEPS_ParticipantModel::InstanceIdentifier::inv_ImagingProcedure_ScheduledProcedureStepId"]
    association :properties => ["BICEPS_ParticipantModel::ImagingProcedure::StudyInstanceUid", "BICEPS_ParticipantModel::InstanceIdentifier::inv_ImagingProcedure_StudyInstanceUid"]
    association :properties => ["BICEPS_ParticipantModel::InstanceIdentifier::Extension", "ExtensionPoint::ExtensionType::inv_InstanceIdentifier_Extension"]
    association :properties => ["BICEPS_ParticipantModel::InstanceIdentifier::IdentifierName", "BICEPS_ParticipantModel::LocalizedText::inv_InstanceIdentifier_IdentifierName"]
    association :properties => ["BICEPS_ParticipantModel::InstanceIdentifier::Type", "BICEPS_ParticipantModel::CodedValue::inv_InstanceIdentifier_Type"]
    association :properties => ["BICEPS_ParticipantModel::LimitAlertConditionDescriptor::MaxLimits", "BICEPS_ParticipantModel::Range::inv_LimitAlertConditionDescriptor_MaxLimits"]
    association :properties => ["BICEPS_ParticipantModel::LimitAlertConditionState::Limits", "BICEPS_ParticipantModel::Range::inv_LimitAlertConditionState_Limits"]
    association :properties => ["BICEPS_ParticipantModel::LocationContextState::LocationDetail", "BICEPS_ParticipantModel::LocationDetail::inv_LocationContextState_LocationDetail"]
    association :properties => ["BICEPS_ParticipantModel::LocationDetail::Extension", "ExtensionPoint::ExtensionType::inv_LocationDetail_Extension"]
    association :properties => ["BICEPS_ParticipantModel::LocationReference::Extension", "ExtensionPoint::ExtensionType::inv_LocationReference_Extension"]
    association :properties => ["BICEPS_ParticipantModel::LocationReference::Identification", "BICEPS_ParticipantModel::InstanceIdentifier::inv_LocationReference_Identification"]
    association :properties => ["BICEPS_ParticipantModel::LocationReference::LocationDetail", "BICEPS_ParticipantModel::LocationDetail::inv_LocationReference_LocationDetail"]
    association :properties => ["BICEPS_ParticipantModel::MdDescription::Extension", "ExtensionPoint::ExtensionType::inv_MdDescription_Extension"]
    association :properties => ["BICEPS_ParticipantModel::MdDescription::Mds", "BICEPS_ParticipantModel::MdsDescriptor::inv_MdDescription_Mds"]
    association :properties => ["BICEPS_ParticipantModel::MdState::Extension", "ExtensionPoint::ExtensionType::inv_MdState_Extension"]
    association :properties => ["BICEPS_ParticipantModel::MdState::State", "BICEPS_ParticipantModel::AbstractState::inv_MdState_State"]
    association :properties => ["BICEPS_ParticipantModel::Mdib::Extension", "ExtensionPoint::ExtensionType::inv_Mdib_Extension"]
    association :properties => ["BICEPS_ParticipantModel::Mdib::MdDescription", "BICEPS_ParticipantModel::MdDescription::inv_Mdib_MdDescription"]
    association :properties => ["BICEPS_ParticipantModel::Mdib::MdState", "BICEPS_ParticipantModel::MdState::inv_Mdib_MdState"]
    association :properties => ["BICEPS_ParticipantModel::MdsDescriptor::ApprovedJurisdictions", "BICEPS_ParticipantModel::ApprovedJurisdictions::inv_MdsDescriptor_ApprovedJurisdictions"]
    association :properties => ["BICEPS_ParticipantModel::MdsDescriptor::Battery", "BICEPS_ParticipantModel::BatteryDescriptor::inv_MdsDescriptor_Battery"]
    association :properties => ["BICEPS_ParticipantModel::MdsDescriptor::Clock", "BICEPS_ParticipantModel::ClockDescriptor::inv_MdsDescriptor_Clock"]
    association :properties => ["BICEPS_ParticipantModel::MdsDescriptor::MetaData", "BICEPS_ParticipantModel::MetaDataType_1::inv_MdsDescriptor_MetaData"]
    association :properties => ["BICEPS_ParticipantModel::MdsDescriptor::SystemContext", "BICEPS_ParticipantModel::SystemContextDescriptor::inv_MdsDescriptor_SystemContext"]
    association :properties => ["BICEPS_ParticipantModel::MdsDescriptor::Vmd", "BICEPS_ParticipantModel::VmdDescriptor::inv_MdsDescriptor_Vmd"]
    association :properties => ["BICEPS_ParticipantModel::MdsState::OperatingJurisdiction", "BICEPS_ParticipantModel::OperatingJurisdiction::inv_MdsState_OperatingJurisdiction"]
    association :properties => ["BICEPS_ParticipantModel::Measurement::Extension", "ExtensionPoint::ExtensionType::inv_Measurement_Extension"]
    association :properties => ["BICEPS_ParticipantModel::Measurement::MeasurementUnit", "BICEPS_ParticipantModel::CodedValue::inv_Measurement_MeasurementUnit"]
    association :properties => ["BICEPS_ParticipantModel::MetaDataType_1::Extension", "ExtensionPoint::ExtensionType::inv_MetaDataType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::MetaDataType_1::Manufacturer", "BICEPS_ParticipantModel::LocalizedText::inv_MetaDataType_1_Manufacturer"]
    association :properties => ["BICEPS_ParticipantModel::MetaDataType_1::ModelName", "BICEPS_ParticipantModel::LocalizedText::inv_MetaDataType_1_ModelName"]
    association :properties => ["BICEPS_ParticipantModel::MetaDataType_1::Udi", "BICEPS_ParticipantModel::UdiType_1::inv_MetaDataType_1_Udi"]
    association :properties => ["BICEPS_ParticipantModel::MetricQualityType_1::Extension", "ExtensionPoint::ExtensionType::inv_MetricQualityType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData::BirthLength", "BICEPS_ParticipantModel::Measurement::inv_NeonatalPatientDemographicsCoreData_BirthLength"]
    association :properties => ["BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData::BirthWeight", "BICEPS_ParticipantModel::Measurement::inv_NeonatalPatientDemographicsCoreData_BirthWeight"]
    association :properties => ["BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData::GestationalAge", "BICEPS_ParticipantModel::Measurement::inv_NeonatalPatientDemographicsCoreData_GestationalAge"]
    association :properties => ["BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData::HeadCircumference", "BICEPS_ParticipantModel::Measurement::inv_NeonatalPatientDemographicsCoreData_HeadCircumference"]
    association :properties => ["BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData::Mother", "BICEPS_ParticipantModel::PersonReference::inv_NeonatalPatientDemographicsCoreData_Mother"]
    association :properties => ["BICEPS_ParticipantModel::NumericMetricDescriptor::TechnicalRange", "BICEPS_ParticipantModel::Range::inv_NumericMetricDescriptor_TechnicalRange"]
    association :properties => ["BICEPS_ParticipantModel::NumericMetricState::MetricValue", "BICEPS_ParticipantModel::NumericMetricValue::inv_NumericMetricState_MetricValue"]
    association :properties => ["BICEPS_ParticipantModel::NumericMetricState::PhysiologicalRange", "BICEPS_ParticipantModel::Range::inv_NumericMetricState_PhysiologicalRange"]
    association :properties => ["BICEPS_ParticipantModel::OperationGroupType_1::Extension", "ExtensionPoint::ExtensionType::inv_OperationGroupType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::OperationGroupType_1::Operations_Attribute", "BICEPS_ParticipantModel::OperationRef::inv_OperationGroupType_1_Operations"]
    association :properties => ["BICEPS_ParticipantModel::OperationGroupType_1::Type", "BICEPS_ParticipantModel::CodedValue::inv_OperationGroupType_1_Type"]
    association :properties => ["BICEPS_ParticipantModel::OperatorContextState::OperatorDetails", "BICEPS_ParticipantModel::BaseDemographics::inv_OperatorContextState_OperatorDetails"]
    association :properties => ["BICEPS_ParticipantModel::OrderDetail::Extension", "ExtensionPoint::ExtensionType::inv_OrderDetail_Extension"]
    association :properties => ["BICEPS_ParticipantModel::OrderDetail::ImagingProcedure", "BICEPS_ParticipantModel::ImagingProcedure::inv_OrderDetail_ImagingProcedure"]
    association :properties => ["BICEPS_ParticipantModel::OrderDetail::Performer", "BICEPS_ParticipantModel::PersonParticipation::inv_OrderDetail_Performer"]
    association :properties => ["BICEPS_ParticipantModel::OrderDetail::Service", "BICEPS_ParticipantModel::CodedValue::inv_OrderDetail_Service"]
    association :properties => ["BICEPS_ParticipantModel::PatientContextState::CoreData", "BICEPS_ParticipantModel::PatientDemographicsCoreData::inv_PatientContextState_CoreData"]
    association :properties => ["BICEPS_ParticipantModel::PatientDemographicsCoreData::DateOfBirth", "BICEPS_ParticipantModel::DateOfBirthType_1::inv_PatientDemographicsCoreData_DateOfBirth"]
    association :properties => ["BICEPS_ParticipantModel::PatientDemographicsCoreData::Height", "BICEPS_ParticipantModel::Measurement::inv_PatientDemographicsCoreData_Height"]
    association :properties => ["BICEPS_ParticipantModel::PatientDemographicsCoreData::Race", "BICEPS_ParticipantModel::CodedValue::inv_PatientDemographicsCoreData_Race"]
    association :properties => ["BICEPS_ParticipantModel::PatientDemographicsCoreData::Weight", "BICEPS_ParticipantModel::Measurement::inv_PatientDemographicsCoreData_Weight"]
    association :properties => ["BICEPS_ParticipantModel::PerformedOrderDetailType_1::FillerOrderNumber", "BICEPS_ParticipantModel::InstanceIdentifier::inv_PerformedOrderDetailType_1_FillerOrderNumber"]
    association :properties => ["BICEPS_ParticipantModel::PerformedOrderDetailType_1::ResultingClinicalInfo", "BICEPS_ParticipantModel::ClinicalInfo::inv_PerformedOrderDetailType_1_ResultingClinicalInfo"]
    association :properties => ["BICEPS_ParticipantModel::PersonParticipation::Role", "BICEPS_ParticipantModel::CodedValue::inv_PersonParticipation_Role"]
    association :properties => ["BICEPS_ParticipantModel::PersonReference::Extension", "ExtensionPoint::ExtensionType::inv_PersonReference_Extension"]
    association :properties => ["BICEPS_ParticipantModel::PersonReference::Identification", "BICEPS_ParticipantModel::InstanceIdentifier::inv_PersonReference_Identification"]
    association :properties => ["BICEPS_ParticipantModel::PersonReference::Name", "BICEPS_ParticipantModel::BaseDemographics::inv_PersonReference_Name"]
    association :properties => ["BICEPS_ParticipantModel::PhysicalConnectorInfo::Extension", "ExtensionPoint::ExtensionType::inv_PhysicalConnectorInfo_Extension"]
    association :properties => ["BICEPS_ParticipantModel::PhysicalConnectorInfo::Label", "BICEPS_ParticipantModel::LocalizedText::inv_PhysicalConnectorInfo_Label"]
    association :properties => ["BICEPS_ParticipantModel::ProductionSpecificationType_1::ComponentId", "BICEPS_ParticipantModel::InstanceIdentifier::inv_ProductionSpecificationType_1_ComponentId"]
    association :properties => ["BICEPS_ParticipantModel::ProductionSpecificationType_1::SpecType", "BICEPS_ParticipantModel::CodedValue::inv_ProductionSpecificationType_1_SpecType"]
    association :properties => ["BICEPS_ParticipantModel::Range::Extension", "ExtensionPoint::ExtensionType::inv_Range_Extension"]
    association :properties => ["BICEPS_ParticipantModel::RealTimeSampleArrayMetricDescriptor::TechnicalRange", "BICEPS_ParticipantModel::Range::inv_RealTimeSampleArrayMetricDescriptor_TechnicalRange"]
    association :properties => ["BICEPS_ParticipantModel::RealTimeSampleArrayMetricState::MetricValue", "BICEPS_ParticipantModel::SampleArrayValue::inv_RealTimeSampleArrayMetricState_MetricValue"]
    association :properties => ["BICEPS_ParticipantModel::RealTimeSampleArrayMetricState::PhysiologicalRange", "BICEPS_ParticipantModel::Range::inv_RealTimeSampleArrayMetricState_PhysiologicalRange"]
    association :properties => ["BICEPS_ParticipantModel::ReferenceRangeType_1::Meaning", "BICEPS_ParticipantModel::CodedValue::inv_ReferenceRangeType_1_Meaning"]
    association :properties => ["BICEPS_ParticipantModel::ReferenceRangeType_1::Range", "BICEPS_ParticipantModel::Range::inv_ReferenceRangeType_1_Range"]
    association :properties => ["BICEPS_ParticipantModel::RelatedMeasurementType_1::ReferenceRange", "BICEPS_ParticipantModel::ReferenceRangeType_1::inv_RelatedMeasurementType_1_ReferenceRange"]
    association :properties => ["BICEPS_ParticipantModel::RelatedMeasurementType_1::Value", "BICEPS_ParticipantModel::Measurement::inv_RelatedMeasurementType_1_Value"]
    association :properties => ["BICEPS_ParticipantModel::RelationType_1::Code", "BICEPS_ParticipantModel::CodedValue::inv_RelationType_1_Code"]
    association :properties => ["BICEPS_ParticipantModel::RelationType_1::Entries_Attribute", "BICEPS_ParticipantModel::EntryRef::inv_RelationType_1_Entries"]
    association :properties => ["BICEPS_ParticipantModel::RelationType_1::Extension", "ExtensionPoint::ExtensionType::inv_RelationType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::RelationType_1::Identification", "BICEPS_ParticipantModel::InstanceIdentifier::inv_RelationType_1_Identification"]
    association :properties => ["BICEPS_ParticipantModel::RemedyInfo::Description", "BICEPS_ParticipantModel::LocalizedText::inv_RemedyInfo_Description"]
    association :properties => ["BICEPS_ParticipantModel::RemedyInfo::Extension", "ExtensionPoint::ExtensionType::inv_RemedyInfo_Extension"]
    association :properties => ["BICEPS_ParticipantModel::RequestedOrderDetailType_1::PlacerOrderNumber", "BICEPS_ParticipantModel::InstanceIdentifier::inv_RequestedOrderDetailType_1_PlacerOrderNumber"]
    association :properties => ["BICEPS_ParticipantModel::RequestedOrderDetailType_1::ReferringPhysician", "BICEPS_ParticipantModel::PersonReference::inv_RequestedOrderDetailType_1_ReferringPhysician"]
    association :properties => ["BICEPS_ParticipantModel::RequestedOrderDetailType_1::RequestingPhysician", "BICEPS_ParticipantModel::PersonReference::inv_RequestedOrderDetailType_1_RequestingPhysician"]
    association :properties => ["BICEPS_ParticipantModel::SampleArrayValue::ApplyAnnotation", "BICEPS_ParticipantModel::ApplyAnnotationType_1::inv_SampleArrayValue_ApplyAnnotation"]
    association :properties => ["BICEPS_ParticipantModel::SampleArrayValue::Samples_Attribute", "BICEPS_ParticipantModel::RealTimeValueType::inv_SampleArrayValue_Samples"]
    association :properties => ["BICEPS_ParticipantModel::ScoDescriptor::Operation", "BICEPS_ParticipantModel::AbstractOperationDescriptor::inv_ScoDescriptor_Operation"]
    association :properties => ["BICEPS_ParticipantModel::ScoState::InvocationRequested_Attribute", "BICEPS_ParticipantModel::OperationRef::inv_ScoState_InvocationRequested"]
    association :properties => ["BICEPS_ParticipantModel::ScoState::InvocationRequired_Attribute", "BICEPS_ParticipantModel::OperationRef::inv_ScoState_InvocationRequired"]
    association :properties => ["BICEPS_ParticipantModel::ScoState::OperationGroup", "BICEPS_ParticipantModel::OperationGroupType_1::inv_ScoState_OperationGroup"]
    association :properties => ["BICEPS_ParticipantModel::SetStringOperationState::AllowedValues", "BICEPS_ParticipantModel::AllowedValuesType_1::inv_SetStringOperationState_AllowedValues"]
    association :properties => ["BICEPS_ParticipantModel::SetValueOperationState::AllowedRange", "BICEPS_ParticipantModel::Range::inv_SetValueOperationState_AllowedRange"]
    association :properties => ["BICEPS_ParticipantModel::StringMetricState::MetricValue", "BICEPS_ParticipantModel::StringMetricValue::inv_StringMetricState_MetricValue"]
    association :properties => ["BICEPS_ParticipantModel::SystemContextDescriptor::EnsembleContext", "BICEPS_ParticipantModel::EnsembleContextDescriptor::inv_SystemContextDescriptor_EnsembleContext"]
    association :properties => ["BICEPS_ParticipantModel::SystemContextDescriptor::LocationContext", "BICEPS_ParticipantModel::LocationContextDescriptor::inv_SystemContextDescriptor_LocationContext"]
    association :properties => ["BICEPS_ParticipantModel::SystemContextDescriptor::MeansContext", "BICEPS_ParticipantModel::MeansContextDescriptor::inv_SystemContextDescriptor_MeansContext"]
    association :properties => ["BICEPS_ParticipantModel::SystemContextDescriptor::OperatorContext", "BICEPS_ParticipantModel::OperatorContextDescriptor::inv_SystemContextDescriptor_OperatorContext"]
    association :properties => ["BICEPS_ParticipantModel::SystemContextDescriptor::PatientContext", "BICEPS_ParticipantModel::PatientContextDescriptor::inv_SystemContextDescriptor_PatientContext"]
    association :properties => ["BICEPS_ParticipantModel::SystemContextDescriptor::WorkflowContext", "BICEPS_ParticipantModel::WorkflowContextDescriptor::inv_SystemContextDescriptor_WorkflowContext"]
    association :properties => ["BICEPS_ParticipantModel::TranslationType_1::Extension", "ExtensionPoint::ExtensionType::inv_TranslationType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::UdiType_1::Extension", "ExtensionPoint::ExtensionType::inv_UdiType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::UdiType_1::Issuer", "BICEPS_ParticipantModel::InstanceIdentifier::inv_UdiType_1_Issuer"]
    association :properties => ["BICEPS_ParticipantModel::UdiType_1::Jurisdiction", "BICEPS_ParticipantModel::InstanceIdentifier::inv_UdiType_1_Jurisdiction"]
    association :properties => ["BICEPS_ParticipantModel::VmdDescriptor::ApprovedJurisdictions", "BICEPS_ParticipantModel::ApprovedJurisdictions::inv_VmdDescriptor_ApprovedJurisdictions"]
    association :properties => ["BICEPS_ParticipantModel::VmdDescriptor::Channel", "BICEPS_ParticipantModel::ChannelDescriptor::inv_VmdDescriptor_Channel"]
    association :properties => ["BICEPS_ParticipantModel::VmdState::OperatingJurisdiction", "BICEPS_ParticipantModel::OperatingJurisdiction::inv_VmdState_OperatingJurisdiction"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowContextState::WorkflowDetail", "BICEPS_ParticipantModel::WorkflowDetailType_1::inv_WorkflowContextState_WorkflowDetail"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::AssignedLocation", "BICEPS_ParticipantModel::LocationReference::inv_WorkflowDetailType_1_AssignedLocation"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::DangerCode", "BICEPS_ParticipantModel::CodedValue::inv_WorkflowDetailType_1_DangerCode"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::Extension", "ExtensionPoint::ExtensionType::inv_WorkflowDetailType_1_Extension"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::Patient", "BICEPS_ParticipantModel::PersonReference::inv_WorkflowDetailType_1_Patient"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::PerformedOrderDetail", "BICEPS_ParticipantModel::PerformedOrderDetailType_1::inv_WorkflowDetailType_1_PerformedOrderDetail"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::RelevantClinicalInfo", "BICEPS_ParticipantModel::ClinicalInfo::inv_WorkflowDetailType_1_RelevantClinicalInfo"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::RequestedOrderDetail", "BICEPS_ParticipantModel::RequestedOrderDetailType_1::inv_WorkflowDetailType_1_RequestedOrderDetail"]
    association :properties => ["BICEPS_ParticipantModel::WorkflowDetailType_1::VisitNumber", "BICEPS_ParticipantModel::InstanceIdentifier::inv_WorkflowDetailType_1_VisitNumber"]
    klass "AbstractAlertDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstractAlertDescriptor acts as a base class for all alert descriptors that contain static alert meta information.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AbstractAlertState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nAbstractAlertState acts as a base class for all alert states that contain dynamic/volatile alert meta information.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ActivationState_Attribute", :type => "BICEPS_ParticipantModel::AlertActivation", :lower => 1, :documentation => "Documentation:\nSee pm:AlertActivation."
    end
    klass "AbstractComplexDeviceComponentDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nAbstractComplexDeviceComponentDescriptor adds an OPTIONAL pm:AlertSystemDescriptor and pm:ScoDescriptor to pm:AbstractDeviceComponentDescriptor.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AlertSystem", :type => "BICEPS_ParticipantModel::AlertSystemDescriptor", :aggregation => :shared, :documentation => "Documentation:\nAn OPTIONAL ALERT SYSTEM that supervises conditions for all sublevels in the hierarchy including the ALERT SYSTEM hosting node itself." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Sco", :type => "BICEPS_ParticipantModel::ScoDescriptor", :aggregation => :shared, :documentation => "Documentation:\nA service control object to define remote control operations. Any pm:AbstractOperationDescriptor/@OperationTarget within this SCO SHALL only reference this or child descriptors within the CONTAINMENT TREE.\n\nNOTE—In modular systems, dynamically plugged-in modules would typically be modeled as VMDs. Such VMDs potentially have their own SCO. In every other case, SCO operations are modeled in pm:MdsDescriptor/pm:Sco." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractComplexDeviceComponentState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nAbstractComplexDeviceComponentState acts as a base class for DEVICE COMPONENT states that have alerting and SCO capabilities.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AbstractContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstract base class for objects that specify that the MDS is able to provide context information that MAY be of relevance for the state data that is present at the communication interface at a certain point of time or time period.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AbstractContextState", :parents => ["BICEPS_ParticipantModel::AbstractMultiState"], :documentation => "Documentation:\nBase type of a context state. Every context state can be identified as valid by a validator instance. Moreover, a context state's lifecycle is determined by a start and end. AbstractContextState bundles these information.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "BindingEndTime_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nPoint in time when a binding of a context state to an MDS ends."
      property "BindingMdibVersion_Attribute", :type => "BICEPS_ParticipantModel::ReferencedVersion", :documentation => "Documentation:\nBindingMdibVersion points to the version of an MDIB when a binding of the context state to an MDS starts."
      property "BindingStartTime_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nPoint in time when a binding of a context state to an MDS starts."
      property "ContextAssociation_Attribute", :type => "BICEPS_ParticipantModel::ContextAssociation", :documentation => "Documentation:\nAssociation of a context. The implied value SHALL be \"No\".\n\n__R5027: Before a SERVICE PROVIDER decides to remove a specific context state from its MDIB, it SHALL change the context association of that context state to \"No\".__\n\nNOTE—BICEPS supports no special state removal flag. Therefore, a SERVICE CONSUMER has to rely on the context association in order to decide if a context state can be kept in memory or removed from memory."
      property "Identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL ordered list of identifiers for the context. The list is ordered by the position of the identifiers in the list where the ELEMENT with the lower list index has a higher relevance than any entry with a higher list index. The SERVICE PROVIDER defines the relevance and MAY reorder the list at any time.\n\nNOTE 1—Identification can be used to span a communication context between SERVICE PROVIDERs and SERVICE CONSUMERs.\nNOTE 2—Identification can be empty, e.g., if pm:AbstractContextState/@ContextAssociation is \"No\"." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "UnbindingMdibVersion_Attribute", :type => "BICEPS_ParticipantModel::ReferencedVersion", :documentation => "Documentation:\nUnbindingMdibVersion points to the version of an MDIB when a binding of a context state to an MDS ends (i.e., the version where the context association was disassociated the first time)."
      property "Validator", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL list of actors (e.g., persons, devices or any identifiable systems) which have confirmed that a binding of a context state to an MDS is correct." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractDescriptor", :documentation => "Documentation:\nAbstractDescriptor defines foundational meta information of any object that is included in the descriptive part of the MDIB. Any descriptor object is derived from pm:AbstractDescriptor. The AbstractDescriptor's counterpart is pm:AbstractState.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "DescriptorVersion_Attribute", :type => "BICEPS_ParticipantModel::VersionCounter", :documentation => "Documentation:\nDescriptorVersion is incremented by one with every descriptor modification. The implied value for the initial descriptor instance SHALL be \"0\"."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Handle_Attribute", :type => "BICEPS_ParticipantModel::Handle", :lower => 1, :documentation => "Documentation:\nThe unique HANDLE of the descriptor. The HANDLE can be used by pm:AbstractState to reference the descriptor."
      property "SafetyClassification_Attribute", :type => "BICEPS_ParticipantModel::SafetyClassification", :documentation => "Documentation:\nThe safety classification of the data that is described with this descriptor. The implied value SHALL be \"Inf\"."
      property "Type", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nThe descriptor type that provides specific information about the descriptor instance, e.g., an pm:MdsDescriptor that designates an anesthesia workstation." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractDeviceComponentDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstractDeviceComponentDescriptor describes a basic DEVICE COMPONENT.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ProductionSpecification", :type => "BICEPS_ParticipantModel::ProductionSpecificationType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nList of production specifications of the component. The production specification describes ELEMENTs such as part numbers, serial numbers, revisions, etc." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractDeviceComponentState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nState of a component that is part of an MDS.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ActivationState_Attribute", :type => "BICEPS_ParticipantModel::ComponentActivation", :documentation => "Documentation:\nSee pm:ComponentActivation. The implied value SHALL be \"On\"."
      property "CalibrationInfo", :type => "BICEPS_ParticipantModel::CalibrationInfo", :aggregation => :shared, :documentation => "Documentation:\nProvides information about the last calibration that was performed." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "NextCalibration", :type => "BICEPS_ParticipantModel::CalibrationInfo", :aggregation => :shared, :documentation => "Documentation:\nProvides information about the next calibration that will be performed." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "OperatingCycles_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nOPTIONAL amount of operarting cycles, e.g., the number of measurements taken within the component. There are no further semantics defined."
      property "OperatingHours_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nOPTIONAL amount of operating hours (e.g., an OR light). There are no further semantics defined."
      property "PhysicalConnector", :type => "BICEPS_ParticipantModel::PhysicalConnectorInfo", :aggregation => :shared, :documentation => "Documentation:\nThe physical connector number for this component, see pm:PhysicalConnectorInfo." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAn abstract descriptor for a METRIC.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ActivationDuration_Attribute", :type => "UML::String", :documentation => "Documentation:\nActivationDuration defines the maximum time period pm:AbstractMetricState/@Activation is \"On\" before it changes to any other state.\n\nNOTE—ActivationDuration is used to indicate the time that a measurement is performed after it has been activated. Example: if automatically measured NIBP is limited to a certain time period only in order to ensure that blood flow in the arm is ensured, then ActivationDuration could have a value of, e.g., five minutes."
      property "BodySite", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL list of CODED VALUEs that describe the body sites where the METRIC is derived from or where it is applied to." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "DerivationMethod_Attribute", :type => "BICEPS_ParticipantModel::DerivationMethod", :documentation => "Documentation:\nSee pm:DerivationMethod. The default value SHALL be applied, depending on pm:AbstractDescriptor/@MetricCategory.\n\n- If pm:AbstractDescriptor/@MetricCategory is \"Set\" or \"Preset\", then the default value of DerivationMethod is \"Man\"\n- If pm:AbstractDescriptor/@MetricCategory is \"Clc\", \"Msrmt\", \"Rcmm\", then the default value of DerivationMethod is \"Auto\"\n- If pm:AbstractDescriptor/@MetricCategory is \"Unspec\", then no default value is being implied"
      property "DeterminationPeriod_Attribute", :type => "UML::String", :documentation => "Documentation:\nThe maximum time interval between two determination steps of determined values under normal conditions.\n\n- For METRICs with sample arrays as determined values where the availability is not continuous, this is the period of time between two determination steps, e.g., waveform snippets that are periodically determined.\n- For METRICs with sample arrays as determined values where the availability is continuous, this is the period of time until the next waveform frame is generated.\n\nNOTE—The determination period that is defined in the descriptor, might not be the currently active determination period. The active determination period is part of the METRIC state."
      property "LifeTimePeriod_Attribute", :type => "UML::String", :documentation => "Documentation:\nGiven the timestamp of a measured METRIC value. The OPTIONAL ATTRIBUTE LifeTimePeriod defines the duration after the measured METRIC value is not useful anymore.\n\nExample: a non-invasive blood pressure measured intermittently might only be considered useful for 24 hours. Hence, LifeTimePeriod would be \"PT24H\"."
      property "MaxDelayTime_Attribute", :type => "UML::String", :documentation => "Documentation:\nMaximum delay to real time. \nFor a measurement or calculation, the maximum delay to real time is the estimated or known maximum difference between the point in time when a physical variable value has been present and when the value has been computed and is ready for communication. This MAY include an averaging period, but it does not include the communication delay. \nFor a setting, the maximum delay to real time is the estimated or known maximum difference between the point in time when a setting has been confirmed to be applicable and the time when the setting becomes effective on the actuator. This does not include any communication delay."
      property "MaxMeasurementTime_Attribute", :type => "UML::String", :documentation => "Documentation:\nMaximum duration between start and stop time of measurment."
      property "MetricAvailability_Attribute", :type => "BICEPS_ParticipantModel::MetricAvailability", :lower => 1, :documentation => "Documentation:\nSee pm:MetricAvailability."
      property "MetricCategory_Attribute", :type => "BICEPS_ParticipantModel::MetricCategory", :lower => 1, :documentation => "Documentation:\nSee pm:MetricCategory."
      property "Relation", :type => "BICEPS_ParticipantModel::RelationType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nRelation allows the modelling of relationships between a metric and other containtment tree entries. Related containment tree entries are defined in ./@Entries, whereby the flavor of a relationship can be set up in ./@Kind.\n\nThe cardinality of Relation is zero or more in order to express relations of different flavors for the same METRIC.\n\nNOTE—Example: some settings of high frequency cutting devices cause changes in measurements (e.g., current form can influences the maximum emitted power). If such a setting is controllable by external means, presumably the SERVICE CONSUMER wants to be able to gain knowledge of affected measurements, which might be then accessed through the Relation element." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Unit", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nCODED VALUE for the unit of a METRIC.\n\nNOTE—If the METRIC is dimensionless and has no unit, use the corresponding CODE from the CODING SYSTEM (e.g., 262656 (4::512) for IEEE 11073-10101 MDC_DIM_DIMLESS)." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_ChannelDescriptor_Metric", :type => "BICEPS_ParticipantModel::ChannelDescriptor"
    end
    klass "AbstractMetricState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nAbstract state of a METRIC.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ActivationState_Attribute", :type => "BICEPS_ParticipantModel::ComponentActivation", :documentation => "Documentation:\nThe activation state of a METRIC. The implied value SHALL be \"On\"."
      property "ActiveDeterminationPeriod_Attribute", :type => "UML::String", :documentation => "Documentation:\nOPTIONAL information of the currently active determination repetition time if it is different from the default determination time that is defined in the descriptive part. ActiveDeterminationPeriod is not necessarily the same as the update period of the periodic event service."
      property "BodySite", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL list of CODED VALUEs that describe the body sites where the measurement is performed or where the setting is applied to.\nThis list of body sites MAY provide more details to location of the measurement or setting that are be available at runtime only or that changes at runtime of the POC MEDICAL DEVICE. It SHOULD NOT contradict the location that has been listed in the descriptor.\n\nExample: if in the descriptor the location \"Upper Abdomen\" is defined, than the state's body site give more details to where the measurement is performed like \"Upper Right Quadrant\" and \"Liver\"." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "LifeTimePeriod_Attribute", :type => "UML::String", :documentation => "Documentation:\nOPTIONAL currently active life-time period that supersedes pm:AbstractMetricDescriptor/@LifeTimePeriod."
      property "PhysicalConnector", :type => "BICEPS_ParticipantModel::PhysicalConnectorInfo", :aggregation => :shared, :documentation => "Documentation:\nThe physical connector number for this METRIC, see pm:PhysicalConnectorInfo." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractMetricValue", :documentation => "Documentation:\nAbstract value of a METRIC.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Annotation", :type => "BICEPS_ParticipantModel::AnnotationType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nAnnotation of a METRIC state value." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "DeterminationTime_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nTime when determined value has been derived from measurement."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MetricQuality", :type => "BICEPS_ParticipantModel::MetricQualityType_1", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe quality state of the determined value of a METRIC." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "StartTime_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nTime when measurement activity was started."
      property "StopTime_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nTime when measurement activity was stopped."
    end
    klass "AbstractMultiState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nAbstractMultiState is derived from pm:AbstractState. In contrast to pm:AbstractState, AbstractMultiState possesses a HANDLE name. The HANDLE name uniquely identifies the state, which is required if the relation to a descriptor is ambiguous.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Category", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nA CODED VALUE that allows to categorize a multi state inside the set of multi states that belong to a descriptor of a certain type.\nNOTE—By using the pm:AbstractMultiState/pm:Category it is possible to represent, e.g., different steps in a workflow (multiple states) with different association states. This is in contrast to the pm:WorkflowContextDescriptor/pm:Type where different workflow types are described that the POC MEDICAL DEVICE supports. See Clause 5.2.6 for more details." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Handle_Attribute", :type => "BICEPS_ParticipantModel::Handle", :lower => 1, :documentation => "Documentation:\nA name to uniquely identify the state."
    end
    klass "AbstractOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstract description of an operation that is exposed on the SCO.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AccessLevel_Attribute", :type => "BICEPS_ParticipantModel::AccessLevelType_1", :documentation => "Documentation:\nAccessLevel defines a user group to whom access to the operation is granted. The implied value SHALL be \"Usr\".\n\t\t\t\t\t\t\n__R5054: Access to the invocation of the operation SHALL be restricted to the defined user group by a SERVICE CONSUMER.__"
      property "InvocationEffectiveTimeout_Attribute", :type => "UML::String", :documentation => "Documentation:\nInvocationEffectiveTimeout defines a time period in which the result of an invocation is effective after it has been successfully finished. When the time is up and the operation has not been retriggered, then the SERVICE PROVIDER MAY revert the operation target to another state.\n\nNOTE—Example: if an arbitrary client remotely controls the cutter of a high frequency cutting device, due to safety reasons the cutter trigger might have a timeout until it stops automatically. To enable continuous activation, the client has to send repeated triggers within the given InvocationEffectiveTimeout duration."
      property "MaxTimeToFinish_Attribute", :type => "UML::String", :documentation => "Documentation:\nMaxTimeToFinish defines the maximum time an operation takes to get from the initial receiving of the command to a successful end."
      property "OperationTarget_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :lower => 1, :documentation => "Documentation:\nA HANDLE reference this operation is targeted to. In case of a single state this is the HANDLE of the descriptor. In case that multiple states may belong to one descriptor (pm:AbstractMultiState), OperationTarget is the HANDLE of one of the state instances (if the state is modified by the operation)."
      property "Retriggerable_Attribute", :type => "UML::Boolean", :documentation => "Documentation:\nRetriggerable is only applicable if ./@InvocationEffectiveTimeout is set. If set to \"true\", then Retriggerable indicates that a call to the activate operation resets the current ./@InvocationEffectiveTimeout, otherwise it will be left as it is until ./@InvocationEffectiveTimeout times out.\n\nThe implied value SHALL be \"true\"."
      property "inv_ScoDescriptor_Operation", :type => "BICEPS_ParticipantModel::ScoDescriptor"
    end
    klass "AbstractOperationState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nState of an operation that is exposed on the SCO.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "OperatingMode_Attribute", :type => "BICEPS_ParticipantModel::OperatingMode", :lower => 1, :documentation => "Documentation:\nOperating mode that defines if the operation is accessible."
    end
    klass "AbstractSetStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractOperationDescriptor"], :documentation => "Documentation:\nAbstract description of an operation that is exposed on the SCO and is intended to be used to set a complete state.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ModifiableData", :type => "UML::String", :upper => Float::INFINITY, :documentation => "Documentation:\nModifiableData describes a list of ATTRIBUTEs and ELEMENTs the underlying operation modifies on invocation by means of XPath expressions.\n\n__R5010: If the ModifiableData list is empty, then all ELEMENTs/ATTRIBUTEs SHALL be modifiable except for pm:AbstractMultiState/@Handle, pm:AbstractState/@DescriptorHandle, pm:AbstractState/@StateVersion, and pm:AbstractState/@DescriptorVersion.__\n\n__R5011: The root ELEMENT of the XPath expressions SHALL be the state of the CONTAINMENT TREE ENTRY referenced by pm:AbstractOperationDescriptor/@OperationTarget.__" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractState", :documentation => "Documentation:\nAbstractState defines foundational meta information of any object that is included in the state part of the MDIB. Any state object is derived from pm:AbstractState. The pm:AbstractState's counterpart is pm:AbstractDescriptor.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "DescriptorHandle_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :lower => 1, :documentation => "Documentation:\nThe HANDLE reference of a descriptor to which the state belongs."
      property "DescriptorVersion_Attribute", :type => "BICEPS_ParticipantModel::ReferencedVersion", :documentation => "Documentation:\nThe current version of the descriptor to that the state belongs to. The implied value for the initial state instance SHALL be \"0\"."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "StateVersion_Attribute", :type => "BICEPS_ParticipantModel::VersionCounter", :documentation => "Documentation:\nStateVersion is incremented by one with every state modification. The implied value for the initial state instance SHALL be \"0\"."
      property "inv_MdState_State", :type => "BICEPS_ParticipantModel::MdState"
    end
    klass "ActivateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes an activate operation that is exposed on the SCO. Activate operations are any parameterized operations that trigger an arbitrary action. The action that is triggered SHALL be defined by the pm:AbstractDescriptor/pm:Type ELEMENT." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Argument", :type => "BICEPS_ParticipantModel::ArgumentType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nArgument description for an activate operation." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ActivateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of an activate operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AlertConditionDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractAlertDescriptor"], :documentation => "Documentation:\nAn ALERT CONDITION contains the information about a potentially or actually HAZARDOUS SITUATION. \n\nExamples: a physiological alarm limit has been exceeded or a sensor has been unplugged." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "CanDeescalate_Attribute", :type => "BICEPS_ParticipantModel::CanDeescalateType_1", :documentation => "Documentation:\nIndicates if an alert condition can deescalate from one priority to another."
      property "CanEscalate_Attribute", :type => "BICEPS_ParticipantModel::CanEscalateType_1", :documentation => "Documentation:\nIndicates if an alert condition can escalate from one priority to another."
      property "CauseInfo", :type => "BICEPS_ParticipantModel::CauseInfo", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nInformation about possible causes if the ALERT CONDITION is present." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "DefaultConditionGenerationDelay_Attribute", :type => "UML::String", :documentation => "Documentation:\nDefaultConditionGenerationDelay is the period that describes delay from the physical fullfilment of an ALERT CONDITION to the generation of the ALERT CONDITION on the POC MEDICAL DEVICE. The implied value SHALL be \"PT0S\"."
      property "Kind_Attribute", :type => "BICEPS_ParticipantModel::AlertConditionKind", :lower => 1, :documentation => "Documentation:\nSee pm:AlertConditionKind."
      property "Priority_Attribute", :type => "BICEPS_ParticipantModel::AlertConditionPriority", :lower => 1, :documentation => "Documentation:\nSee pm:AlertConditionPriority.\n\nNOTE—If the ATTRIBUTE pm:AlertConditionState/@ActualPriority is present, the priority defined for pm:AlertConditionDescriptor MAY not reflect the current severity of the potential or actual hazard that exists if the ALERT CONDITION is present."
      property "Source", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY, :documentation => "Documentation:\nA list of HANDLE references to sources (e.g., METRICs) that cause the ALERT CONDITION.\n\nExample if a source is present: the heart rate METRIC is the source for a \"heart rate high\" ALERT CONDITION.\nExample if no source is present: a \"cable disconnection\" ALERT CONDITION has no source." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AlertSystemDescriptor_AlertCondition", :type => "BICEPS_ParticipantModel::AlertSystemDescriptor"
    end
    klass "AlertConditionReference", :documentation => "Documentation:\nA list of HANDLE references that point to ALERT CONDITIONs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      applied_stereotype :instance_of => "XML_Schema::list"
      property "inv_AlertSystemState_PresentPhysiologicalAlarmConditions", :type => "BICEPS_ParticipantModel::AlertSystemState"
      property "inv_AlertSystemState_PresentTechnicalAlarmConditions", :type => "BICEPS_ParticipantModel::AlertSystemState"
      property "listItem_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY
    end
    klass "AlertConditionState", :parents => ["BICEPS_ParticipantModel::AbstractAlertState"], :documentation => "Documentation:\nAlertConditionState contains the dynamic/volatile information of an ALERT CONDITION. See pm:AlertConditionDescriptor for static information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ActualConditionGenerationDelay_Attribute", :type => "UML::String", :documentation => "Documentation:\nActualConditionGenerationDelay overrides pm:AlertConditionDescriptor/@DefaultConditionGenerationDelay."
      property "ActualPriority_Attribute", :type => "BICEPS_ParticipantModel::AlertConditionPriority", :documentation => "Documentation:\nThe current priority of the ALERT CONDITION that has been modified by an escalation or de-escalation process. \n\nNOTE—If this ATTRIBUTE is present in an pm:AlertConditonState ELEMENT, the related pm:AlertConditionDescritptor/pm:Priority ELEMENT MAY NOT reflect the current severity of the potential or actual hazard that exists if this ALERT CONDITION is present."
      property "DeterminationTime_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nTimepoint when the ALERT CONDITION has changed its presence the last time."
      property "Presence_Attribute", :type => "UML::Boolean", :documentation => "Documentation:\nThe Presence ATTRIBUTE is set to \"true\" if the ALERT CONDITION has been detected and is still present. Otherwise it is set to \"false\". The implied value SHALL be \"false\"."
      property "Rank_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nThe rank is an optional ATTRIBUTE allowing finer distinction of ALERT CONDITION priorities. A ranking is a relationship between a set of items such that, for any two items, the first is either \"ranked higher than\", \"ranked lower than\" or \"ranked equal to\" the second."
    end
    klass "AlertSignalDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractAlertDescriptor"], :documentation => "Documentation:\nAlertSignalDescriptor represents an ALERT SIGNAL. An ALERT SIGNAL contains information about the way an ALERT CONDITION is communicated to a human. It is generated by an ALERT SYSTEM to indicate the presence or occurrence of an ALERT CONDITION.\n\nExample: a signal could be a lamp (see pm:AlertSignalDescriptor/pm:Manifestation) on a remote POC MEDICAL DEVICE, such as the nurses handheld device (see pm:AlertSignalDescriptor/pm:SignalDelegationSupported), which starts flashing when the heart rate is exceeding 150bmp (see pm:AlertSignalDescriptor/pm:ConditionSignaled) for more than 2 seconds (see pm:AlertSignalDescriptor/pm:DefaultSignalGenerationDelay), and keeps flashing until the nurse confirms the alarm, even if the alarm condition is not present anymore (see pm:AlertSignalDescriptor/pm:Latching)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AcknowledgeTimeout_Attribute", :type => "UML::String", :documentation => "Documentation:\nIndicates the acknowledgment timeout if the signal supports acknowledgment. If no duration is defined, an indefinite acknowledgment timeout SHALL be supported."
      property "AcknowledgementSupported_Attribute", :type => "UML::Boolean", :documentation => "Documentation:\nAcknowledgementSupported is set to \"true\" to indicate if the ALERT SIGNAL supports acknowledgment. The implied value SHALL be \"false\"."
      property "ConditionSignaled_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :documentation => "Documentation:\nReference to an ALERT CONDITION communicated by the ALERT SIGNAL. The ALERT CONDITION signaled has to be in the same ALERT SYSTEM or in an ALERT SYSTEM that is underneath the ALERT SYSTEM of this ALERT SIGNAL in the CONTAINMENT TREE.\n\nExample: assume an MDS possesses two VMDs and an ALERT SYSTEM A, and each of these VMDs possess itself an ALERT SYSTEM (B and C). An ALERT SIGNAL from the ALERT SYSTEM A of the MDS is allowed to reference an ALERT CONDITION from the ALERT SYSTEMs A, B and C. In contrast to this an ALERT SIGNAL from the ALERT SYSTEM B is allowed to reference only ALERT CONDITIONs from the ALERT SYSTEM B and not from the ALERT SYSTEM A or C."
      property "DefaultSignalGenerationDelay_Attribute", :type => "UML::String", :documentation => "Documentation:\nDefaultSignalGenerationDelay is the default period from the onset of an ALERT CONDITION to the generation of the ALERT SIGNAL. The implied value SHALL be \"PT0S\".\n\nExample: if the heart rate exceeds a limit periodically as the actual rate oscillates around the limit value, it might be desirable to not directly generate the ALERT SIGNAL whenever the limit is exceeded, but to wait for the given delay first.\n\nIn the case of a FALLBACK ALERT SIGNAL, DefaultSignalGenerationDelay is the default period of time from when a PARTICIPANT last retriggered the SetAlertStateOperation to the generation of the FALLBACK ALERT SIGNAL."
      property "Latching_Attribute", :type => "UML::Boolean", :lower => 1, :documentation => "Documentation:\nAn ALERT SIGNAL is latching if it outlives its triggering ALERT CONDITION until it is stopped by deliberate action.\n\nExample: if the patient's heart rate exceeds a given limit for a certain time, but then normalizes such that the ALERT CONDITION no longer exists, it might be desirable to keep the ALERT SIGNAL alive until, e.g., the nurse confirms it. Otherwise the ALERT CONDITION could pass unnoticed."
      property "Manifestation_Attribute", :type => "BICEPS_ParticipantModel::AlertSignalManifestation", :lower => 1, :documentation => "Documentation:\nSee pm:AlertSignalManifestation."
      property "MaxSignalGenerationDelay_Attribute", :type => "UML::String", :documentation => "Documentation:\nOPTIONAL maximum delay of pm:AlertSignalState/@ActualSignalGenerationDelay."
      property "MinSignalGenerationDelay_Attribute", :type => "UML::String", :documentation => "Documentation:\nOPTIONAL minimum delay of pm:AlertSignalState/@ActualSignalGenerationDelay."
      property "SignalDelegationSupported_Attribute", :type => "UML::Boolean", :documentation => "Documentation:\nSignalDelegationSupported is set to \"true\" to indicate if the signal can be generated at another PARTICIPANT as primary ALERT SIGNAL, otherwise \"false\". The implied value SHALL be \"false\"."
      property "inv_AlertSystemDescriptor_AlertSignal", :type => "BICEPS_ParticipantModel::AlertSystemDescriptor"
    end
    klass "AlertSignalState", :parents => ["BICEPS_ParticipantModel::AbstractAlertState"], :documentation => "Documentation:\nAlertSignalState contains the dynamic/volatile information of an ALERT SIGNAL. See pm:AlertSignalDescriptor for static information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ActualSignalGenerationDelay_Attribute", :type => "UML::String", :documentation => "Documentation:\nActualSignalGenerationDelay overrides pm:AlertSignalDescriptor/@DefaultSignalGenerationDelay."
      property "Location_Attribute", :type => "BICEPS_ParticipantModel::AlertSignalPrimaryLocation", :documentation => "Documentation:\nSee pm:AlertSignalPrimaryLocation. The implied value SHALL be \"Loc\"."
      property "Presence_Attribute", :type => "BICEPS_ParticipantModel::AlertSignalPresence", :documentation => "Documentation:\nSee pm:AlertSignalPresence. The implied value SHALL be \"Off\"."
      property "Slot_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nThe slot is a 0-based index that allows a prioritization of the ALERT SIGNAL w.r.t. signal tangibility. The Slot SHOULD be used if the medium for signal generation has only a limited capability of parallel signal generation. The smaller the slot index, the higher is the priority in generation of the signal.\n\nExample: if a signal is audible and there are different audio signals for different ALERT SIGNALs, and more than one ALERT SIGNAL has to be generated, the generating system has to decide which of the ALERT SIGNALs it generates as overlaying audio signals might not be desirable. For example, if the first ALERT SIGNAL has a slot number of 0 and the second ALERT SIGNAL has a slot number 1 and both signals are active, than the ALERT SYSTEM generates only the ALERT SIGNAL with the slot number 0."
    end
    klass "AlertSystemDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractAlertDescriptor"], :documentation => "Documentation:\nAlertSystemDescriptor describes an ALERT SYSTEM to detect ALERT CONDITIONs and generate ALERT SIGNALs, which belong to specific ALERT CONDITIONs.\n\nALERT CONDITIONs are represented by a list of pm:AlertConditionDescriptor ELEMENTs and ALERT SIGNALs are represented by a list of pm:AlertSignalDescriptor ELEMENTs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AlertCondition", :type => "BICEPS_ParticipantModel::AlertConditionDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nDescription of all ALERT CONDITIONs that can be detected by the surrounding ALERT SYSTEM." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "AlertSignal", :type => "BICEPS_ParticipantModel::AlertSignalDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nDescription of all ALERT SIGNALs that MAY be generated by the surrounding ALERT SYSTEM as a consequence of a detected ALERT CONDITIONs." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MaxPhysiologicalParallelAlarms_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nThe maximum number of physiological alarm conditions that can be present at a point of time. If no value is given, an unlimited number SHALL be assumed."
      property "MaxTechnicalParallelAlarms_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nThe maximum number of technical alarm conditions that can be present at a point of time. If no value is given, an unlimited number SHALL be assumed."
      property "SelfCheckPeriod_Attribute", :type => "UML::String", :documentation => "Documentation:\nThe self check time period defines the value after which a self test of the ALERT SYSTEM is performed. This self check period MAY be used to detect if an ALERT SYSTEM is still operating."
      property "inv_AbstractComplexDeviceComponentDescriptor_AlertSystem", :type => "BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor"
    end
    klass "AlertSystemState", :parents => ["BICEPS_ParticipantModel::AbstractAlertState"], :documentation => "Documentation:\nAlertSystemState contains the dynamic/volatile information of an ALERT SYSTEM. See pm:AlertSystemDescriptor for static information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "LastSelfCheck_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nTimepoint when the ALERT SYSTEM has performed a self check the last time. LastSelfCheck SHALL be updated on every pm:AlertSystemDescriptor/pm:SelfCheckPeriod."
      property "PresentPhysiologicalAlarmConditions_Attribute", :type => "BICEPS_ParticipantModel::AlertConditionReference", :aggregation => :shared, :documentation => "Documentation:\nList of HANDLE references to the present physiological alarm conditions that have been determined by the ALERT SYSTEM."
      property "PresentTechnicalAlarmConditions_Attribute", :type => "BICEPS_ParticipantModel::AlertConditionReference", :aggregation => :shared, :documentation => "Documentation:\nList of HANDLE references to the present technical alarm conditions that have been determined by the ALERT SYSTEM."
      property "SelfCheckCount_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nNumber of self checks performed.\n\nThis specification does not prescribe the origin of the value, i.e., whether the counter is incremented since the last boot or whether the counter represents the self checks ever performed."
      property "SystemSignalActivation", :type => "BICEPS_ParticipantModel::SystemSignalActivation", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nDescribes a compound ALERT SIGNAL activation for local audible, visible, and tangible ALERT SIGNALs within the ALERT SYSTEM.\n\nIf a SystemSignalActivation SSA is set for any ALERT SIGNAL manifestation within an ALERT SYSTEM, then the following rules SHALL apply for any local ALERT SIGNAL ASi within the ALERT SYSTEM where SSA/@Manifestation is equal to ASi/@Manifestation:\n\n- If SSA/@State is \"On\", any ASi/@ActivationState is \"On\", \"Off\", or \"Psd\"\n- If SSA/@State is \"Psd\", any ASi/@ActivationState is \"Psd\" or \"Off\"\n- If SSA/@State is \"Off\", all ASi/@ActivationState are \"Off\"\n- If any ASi/@ActivationState is \"On\" then SSA/@State is \"On\"\n- If all ASi/@ActivationState are \"Psd\" then SSA/@State is \"Psd\"\n- If all ASi/@ActivationState are \"Off\" then SSA/@State is \"Off\"\n\nNOTE 1—A local ALERT SIGNAL is an ALERT SIGNAL where pm:AlertSignalState/@Location is set to \"Loc\", whereas a remote ALERT SIGNAL is an ALERT SIGNAL where pm:AlertSignalState/@Location is set to \"Rem\".\nNOTE 2—Remote signals are not considered by SystemSignalActivation." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AllowedValueType_1", :documentation => "Documentation:\nList of values that the enumerated string METRIC accepts as a valid value." do
      property "Characteristic", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL field to attach a dimensional measurement to each allowed value." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL identification to apply instance identifiers to each allowed value." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Type", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL pm:CodedValue to semantically describe the allowed value." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Value", :type => "UML::String", :lower => 1, :documentation => "Documentation:\nAccepted string value." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_EnumStringMetricDescriptor_AllowedValue", :type => "BICEPS_ParticipantModel::EnumStringMetricDescriptor"
    end
    klass "AllowedValuesType_1", :documentation => "Documentation:\nAn OPTIONAL list of currently allowed string values that can be requested. If the list is empty, then there is not limitation." do
      property "Value", :type => "UML::String", :lower => 1, :upper => Float::INFINITY, :documentation => "Documentation:\nA single allowed value that can be requested." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_SetStringOperationState_AllowedValues", :type => "BICEPS_ParticipantModel::SetStringOperationState"
    end
    klass "AnnotationType_1", :documentation => "Documentation:\nAnnotation of a METRIC state value." do
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Type", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe CODED VALUE that describes the annotation of the ELEMENT.\n\nExample: attach triggers in waveform curves." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AbstractMetricValue_Annotation", :type => "BICEPS_ParticipantModel::AbstractMetricValue"
    end
    klass "ApplyAnnotationType_1", :documentation => "Documentation:\nAnnotations MAY only apply to specific values in the real-time sample array. The ApplyAnnotation set relates annotations to sample indices. If no ApplyAnnotation ELEMENT is provided all annotations are valid for all values in the context." do
      property "AnnotationIndex_Attribute", :type => "UML::Integer", :lower => 1, :documentation => "Documentation:\nIndex number of the annotation that is addressed by the ApplyAnnotation ELEMENT. The index number refers to the (n+1)-nth pm:AbstractMetricValue/pm:Annotation ELEMENT. Hence, numbering is zero-based."
      property "SampleIndex_Attribute", :type => "UML::Integer", :lower => 1, :documentation => "Documentation:\nIndex number of the sample the defined annotation refers to. The index number addresses the (n+1)-nth number in the pm:RealTimeSampleArrayValue/pm:Samples ATTRIBUTE. Hence, numbering is zero-based."
      property "inv_SampleArrayValue_ApplyAnnotation", :type => "BICEPS_ParticipantModel::SampleArrayValue"
    end
    klass "ApprovedJurisdictions", :documentation => "Documentation:\nList of regions in which a DEVICE COMPONENT is approved to be operated. If the list does not contain any entries, then the DEVICE COMPONENT is not approved for any region." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ApprovedJurisdiction", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nRegion in which the DEVICE COMPONENT is approved to be operated.\n\nNOTE—See also: https://unstats.un.org/unsd/methodology/m49/" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MdsDescriptor_ApprovedJurisdictions", :type => "BICEPS_ParticipantModel::MdsDescriptor"
      property "inv_VmdDescriptor_ApprovedJurisdictions", :type => "BICEPS_ParticipantModel::VmdDescriptor"
    end
    klass "ArgumentType_1", :documentation => "Documentation:\nArgument description for an activate operation." do
      property "Arg", :type => "UML::String", :lower => 1, :documentation => "Documentation:\nData type of the argument, defined by a qualified name." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ArgName", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nCODED VALUE that describes this argument." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_ActivateOperationDescriptor_Argument", :type => "BICEPS_ParticipantModel::ActivateOperationDescriptor"
    end
    klass "BaseDemographics", :documentation => "Documentation:\nDefinition of basic demographic information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Birthname", :type => "UML::String", :documentation => "Documentation:\nBirth name of a person." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Familyname", :type => "UML::String", :documentation => "Documentation:\nFamily name of a person." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Givenname", :type => "UML::String", :documentation => "Documentation:\nGiven name of a person." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Middlename", :type => "UML::String", :upper => Float::INFINITY, :documentation => "Documentation:\nMiddle name of a person." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Title", :type => "UML::String", :documentation => "Documentation:\nTitle of a person." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_OperatorContextState_OperatorDetails", :type => "BICEPS_ParticipantModel::OperatorContextState"
      property "inv_PersonReference_Name", :type => "BICEPS_ParticipantModel::PersonReference"
    end
    klass "BatteryDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nFor battery-powered devices, battery information can be contained in this object." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "CapacityFullCharge", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nCurrent battery capacity after a full charge." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "CapacitySpecified", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nRated capacity the manufacturer claims for the battery." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "VoltageSpecified", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nSpecified battery voltage." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MdsDescriptor_Battery", :type => "BICEPS_ParticipantModel::MdsDescriptor"
    end
    klass "BatteryState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nState of a battery of an MDS.\n\nThe current power source is designated by ./@ActivationState:\n\n- If ./@ActivationState equals \"On\", the device is running on battery\n- If ./@ActivationState equals \"Off\", the device is in mains operation and currently not able to be run on battery\n- If ./@ActivationState equals \"StndBy\", the device is in mains operation and can be switched to run on battery\n- If ./@ActivationState equals \"Fail\", the battery has a malfunction. Detailed error information SHOULD be communicated by using an ALERT SYSTEM.\n\nEnumerations \"Shtdn\" and \"NotRdy\" are undefined for BatteryState." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "CapacityRemaining", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nRemaining capacity at current load." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ChargeCycles_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nNumber of charge/discharge cycles."
      property "ChargeStatus_Attribute", :type => "BICEPS_ParticipantModel::ChargeStatusType_1", :documentation => "Documentation:\nCurrent charge status of the battery."
      property "Current", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nElectric current delivered by a battery during its discharge; negative if battery is charge. See also IEC 60050-482 International Electrotechnical Vocabulary, 482-03-24." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RemainingBatteryTime", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nCurrent remaining time until battery is discharged." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Temperature", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nCurrent battery temperature." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Voltage", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nVoltage between the terminals of a cell or battery when being discharged. See also IEC 60050-482 International Electrotechnical Vocabulary, 482-03-28." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "CalibrationDocumentationType_1", :documentation => "Documentation:\nCalibrationDocumentation provides information regarding necessary or performed calibration steps including potential calibration results like accuracy." do
      property "CalibrationResult", :type => "BICEPS_ParticipantModel::CalibrationResultType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\n" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Documentation", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nHuman-readable documentation of a CalibrationDocumentation entry." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_CalibrationInfo_CalibrationDocumentation", :type => "BICEPS_ParticipantModel::CalibrationInfo"
    end
    klass "CalibrationInfo", :documentation => "Documentation:\nProvides information in terms of component calibration. By default, it only maintains a calibration flag." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "CalibrationDocumentation", :type => "BICEPS_ParticipantModel::CalibrationDocumentationType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nCalibrationDocumentation provides information regarding necessary or performed calibration steps including potential calibration results like accuracy." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ComponentCalibrationState_Attribute", :type => "BICEPS_ParticipantModel::CalibrationState", :documentation => "Documentation:\nATTRIBUTE definition of ComponentCalibration."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Time_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nTime of the calibration."
      property "Type_Attribute", :type => "BICEPS_ParticipantModel::CalibrationType", :documentation => "Documentation:\nType of the calibration. The implied value SHALL be \"Unspec\"."
      property "inv_AbstractDeviceComponentState_CalibrationInfo", :type => "BICEPS_ParticipantModel::AbstractDeviceComponentState"
      property "inv_AbstractDeviceComponentState_NextCalibration", :type => "BICEPS_ParticipantModel::AbstractDeviceComponentState"
    end
    klass "CalibrationResultType_1", :documentation => "Documentation:\n" do
      property "Code", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nCode that semantically describes the calibration result." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Value", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nMeasurement that represents the value related to that specific calibration result." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_CalibrationDocumentationType_1_CalibrationResult", :type => "BICEPS_ParticipantModel::CalibrationDocumentationType_1"
    end
    klass "CauseInfo", :documentation => "Documentation:\nCause information for an ALERT CONDITION." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Description", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL human-readable texts that describe the cause." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RemedyInfo", :type => "BICEPS_ParticipantModel::RemedyInfo", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL information on how to remedy the ALERT CONDITION." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AlertConditionDescriptor_CauseInfo", :type => "BICEPS_ParticipantModel::AlertConditionDescriptor"
    end
    klass "ChannelDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nChannelDescriptor describes a CHANNEL to group METRICs and alerts. It is used for organizational purposes only.\n\nExample: an example would be a blood pressure VMD with one CHANNEL to group together all METRICs that deal with the blood pressure (e.g., pressure value, pressure waveform). A second CHANNEL object could be used to group together METRICs that deal with heart rate." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Metric", :type => "BICEPS_ParticipantModel::AbstractMetricDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nList of METRICs that are grouped into the CHANNEL. The list is ordered by the position of the METRIC in the list where the ELEMENT with a lower list index has a higher clinical relevance than any entry with a higher list index. The SERVICE PROVIDER defines the clinical relevance and MAY reorder the list at any time." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_VmdDescriptor_Channel", :type => "BICEPS_ParticipantModel::VmdDescriptor"
    end
    klass "ChannelState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nThe state of a CHANNEL." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "ClinicalInfo", :documentation => "Documentation:\nThis type describes a minimal clinical observation." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Code", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nCode that specifies the kind of the type of observation, e.g., a specific allergy or a specific diagnosis." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Criticality", :type => "BICEPS_ParticipantModel::CriticalityType_1", :documentation => "Documentation:\nPotential clinical harm if this clinical information is not considered while treating the patient. The implied value SHALL be \"Lo\"." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Description", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nList of possible (localized) free text descriptions of the clinical information.\n\nIf a pm:CodedValue for this clinical information is available, it is encouraged to be set as pm:ClinicalInfo/pm:Code." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RelatedMeasurement", :type => "BICEPS_ParticipantModel::RelatedMeasurementType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nRelated measurements for this clinical observation if applicable." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Type", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nType of clinical information, e.g., allergy, intolerance, clinical condition, diagnosis, problem, etc." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_PerformedOrderDetailType_1_ResultingClinicalInfo", :type => "BICEPS_ParticipantModel::PerformedOrderDetailType_1"
      property "inv_WorkflowDetailType_1_RelevantClinicalInfo", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
    klass "ClockDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nClockDescriptor describes the capabilities of an MDS regarding date/time handling and synchronization. The presense of a ClockDescriptor does not imply any specific hardware or software support." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Resolution_Attribute", :type => "UML::String", :documentation => "Documentation:\nTime between actual ticks of the clock in microseconds. If none is given, the resolution is unknown."
      property "TimeProtocol", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL list of protocols that can be used to set the POC MEDICAL DEVICE's clock. An empty list implies that the clock cannot be set and is therefore uncalibrated and unsynchronized (e.g., equal to MDC:MDC_TIME_SYNC_NONE).   \nExample: {MDC:MDC_TIME_SYNC_EBWW, MDC:MDC_TIME_SYNC_SNTPV4, MDC:MDC_TIME_SYNC_NTPV3} if the clock supports synchronization using manually setting on the POC MEDICAL DEVICE, SNTP v4.0 (RFC 2030) and NTP v3.0 (RFC 1305)." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MdsDescriptor_Clock", :type => "BICEPS_ParticipantModel::MdsDescriptor"
    end
    klass "ClockState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nState of a clock of an MDS." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Accuracy_Attribute", :type => "UML::Float", :documentation => "Documentation:\nAccuracy is a decimal number indicating the maximum error in seconds of the absolute time relative to a primary reference clock source.\n\nIn systems where time synchronization is not used and the clock is set manually by \"eyeball and wristwatch\" (EBWW), this SHALL be initialized to three minutes when the clock time is set. If NTP is used, this is equivalent to Root Dispersion + 1⁄2 Root Delay."
      property "ActiveSyncProtocol", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nProtocol that is actively being used for time sync.\n\nExamples: MDC:MDC_TIME_SYNC_NTPV3 if the clock is synchronized using NTP v3.0 (RFC 1305) or MDC:MDC_TIME_SYNC_NONE if the clock is not synchronized." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "CriticalUse_Attribute", :type => "UML::Boolean", :documentation => "Documentation:\nIdentifies that the clock information is actively being used in care delivery algorithms/protocols. The implied value SHALL be \"false\"."
      property "DateAndTime_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nCurrent date/time setting. As the current date/time changes at a high frequency, a change of this value SHALL NOT cause an update of the state version unless it has been synchronized either remotely or manually.\n\nNOTE—DateAndTime could be filled when the clock is explicitly requested."
      property "LastSet_Attribute", :type => "BICEPS_ParticipantModel::Timestamp", :documentation => "Documentation:\nTime point when the absolute time was set or synchronized.\n\nNOTE 1—If a time synchronization protocol is used that \"changes\" the time and date at a high frequency, it is proposed to update this value at a lower periodicity (e.g., once every 10 minutes or once an hour), so as not to consume communications bandwidth unnecessarily.\nNOTE 2—Synchronization might be achieved by slewing the time. This means that the virtual frequency of the software clock is adjusted to make the clock go faster or slower until it is corrected."
      property "ReferenceSource", :type => "UML::String", :upper => Float::INFINITY, :documentation => "Documentation:\nIdentifies the clock's external reference source(s), e.g., NTP server addresses." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RemoteSync_Attribute", :type => "UML::Boolean", :lower => 1, :documentation => "Documentation:\nIndicates if the time is synchronized to an external source or set by an operator."
      property "TimeZone_Attribute", :type => "BICEPS_ParticipantModel::TimeZone", :documentation => "Documentation:\nIdentifies the time zone and DST of the clock."
    end
    klass "CodedValue", :documentation => "Documentation:\nIn general, in an interoperability format, objects, attributes, and methods are identified by nomenclature codes. CodedValue offers the ability to represent such nomenclature codes.\n\nTwo CodedValue objects C1 and C2 are equivalent, if\n\n- C1/@Code equals C2/@Code\n- C1/@CodingSystem equals C2/@CodingSystem, both with expanded default values\n- C1/@CodingSystemVersion equals C2/@CodingSystemVersion\n- If there exists a CodedValue object T1 in C1/pm:Translation and a CodedValue object T2 in C2/pm:Translation such that T1 and T2 are equivalent, C1 and T2 are equivalent, or C2 and T1 are equivalent.\n\nNOTE 1—In case that ./@CodingSystem is not explicitly defined in CodedValue, it is replaced implicitly by a default identifier. The ./@CodingSystem ATTRIBUTE is then called \"expanded\". \nNOTE 2—As prescribed in ./@CodingSystemVersion, a version is set only if a unique version identification by ./@CodingSystem is not possible. Hence, there can be no implicit version mismatch.\nNOTE 3—Equivalence between CodedValue objects is not necessarily transitive.\n\t\t\t" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Code_Attribute", :type => "BICEPS_ParticipantModel::CodeIdentifier", :lower => 1, :documentation => "Documentation:\nREQUIRED unique identifier of the CODE in the CODING SYSTEM.\n\nExample: as \"20720\" from Block 2, of IEEE 11073-10101:2004, is the id for \"MDC_PRESS_AWAY\", the context-free CODE (CF_Code10) is \"151792\" which represents the unique identifier used within IEEE 11073-10101:2004."
      property "CodingSystemName", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nHuman-readable name of the CODING SYSTEM that is described by pm:CodedValue/@CodingSystem." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "CodingSystemVersion_Attribute", :type => "UML::String", :documentation => "Documentation:\nCodingSystemVersion is a particular version of the CODING SYSTEM defined by pm:CodedValue/@CodingSystem. CodingSystemVersion SHALL be set if multiple versions of the underlying CODING SYSTEM exist and a unique identification of the CODED VALUE is not possible by other means.\n\nExample: \"20041215\" for the ISO/IEC 11073-10101:2004, as it is the release date of the standard's first edition."
      property "CodingSystem_Attribute", :type => "UML::Uri", :documentation => "Documentation:\nUnique identifier of a CODING SYSTEM that pm:CodedValue/@Code originating from.\n\nIf no CODING SYSTEM is defined, the implied value SHALL be \"urn:oid:1.2.840.10004.1.1.1.0.0.1\", which refers to ISO/IEC 11073-10101.\n\nExample: \"urn:oid:1.2.840.10004.1.1.1.0.0.1\" for the ISO/IEC 11073-10101."
      property "ConceptDescription", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nMultiple OPTIONAL human-readable texts that describe the CODE in more detail." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "SymbolicCodeName_Attribute", :type => "BICEPS_ParticipantModel::SymbolicCodeName", :documentation => "Documentation:\nSee pm:SymbolicCodeName."
      property "Translation", :type => "BICEPS_ParticipantModel::TranslationType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nSet of alternative or equivalent representations." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AbstractDescriptor_Type", :type => "BICEPS_ParticipantModel::AbstractDescriptor"
      property "inv_AbstractMetricDescriptor_BodySite", :type => "BICEPS_ParticipantModel::AbstractMetricDescriptor"
      property "inv_AbstractMetricDescriptor_Unit", :type => "BICEPS_ParticipantModel::AbstractMetricDescriptor"
      property "inv_AbstractMetricState_BodySite", :type => "BICEPS_ParticipantModel::AbstractMetricState"
      property "inv_AbstractMultiState_Category", :type => "BICEPS_ParticipantModel::AbstractMultiState"
      property "inv_AllowedValueType_1_Type", :type => "BICEPS_ParticipantModel::AllowedValueType_1"
      property "inv_AnnotationType_1_Type", :type => "BICEPS_ParticipantModel::AnnotationType_1"
      property "inv_ArgumentType_1_ArgName", :type => "BICEPS_ParticipantModel::ArgumentType_1"
      property "inv_CalibrationResultType_1_Code", :type => "BICEPS_ParticipantModel::CalibrationResultType_1"
      property "inv_ClinicalInfo_Code", :type => "BICEPS_ParticipantModel::ClinicalInfo"
      property "inv_ClinicalInfo_Type", :type => "BICEPS_ParticipantModel::ClinicalInfo"
      property "inv_ClockDescriptor_TimeProtocol", :type => "BICEPS_ParticipantModel::ClockDescriptor"
      property "inv_ClockState_ActiveSyncProtocol", :type => "BICEPS_ParticipantModel::ClockState"
      property "inv_ContainmentTreeEntry_Type", :type => "BICEPS_ParticipantModel::ContainmentTreeEntry"
      property "inv_DistributionSampleArrayMetricDescriptor_DomainUnit", :type => "BICEPS_ParticipantModel::DistributionSampleArrayMetricDescriptor"
      property "inv_ImagingProcedure_Modality", :type => "BICEPS_ParticipantModel::ImagingProcedure"
      property "inv_ImagingProcedure_ProtocolCode", :type => "BICEPS_ParticipantModel::ImagingProcedure"
      property "inv_InstanceIdentifier_Type", :type => "BICEPS_ParticipantModel::InstanceIdentifier"
      property "inv_Measurement_MeasurementUnit", :type => "BICEPS_ParticipantModel::Measurement"
      property "inv_OperationGroupType_1_Type", :type => "BICEPS_ParticipantModel::OperationGroupType_1"
      property "inv_OrderDetail_Service", :type => "BICEPS_ParticipantModel::OrderDetail"
      property "inv_PatientDemographicsCoreData_Race", :type => "BICEPS_ParticipantModel::PatientDemographicsCoreData"
      property "inv_PersonParticipation_Role", :type => "BICEPS_ParticipantModel::PersonParticipation"
      property "inv_ProductionSpecificationType_1_SpecType", :type => "BICEPS_ParticipantModel::ProductionSpecificationType_1"
      property "inv_ReferenceRangeType_1_Meaning", :type => "BICEPS_ParticipantModel::ReferenceRangeType_1"
      property "inv_RelationType_1_Code", :type => "BICEPS_ParticipantModel::RelationType_1"
      property "inv_WorkflowDetailType_1_DangerCode", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
    klass "ContainmentTree", :documentation => "Documentation:\nCONTAINMENT TREE part of an ELEMENT of an MDS CONTAINMENT TREE." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ChildrenCount_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nNumber of child ELEMENTs that the CONTAINMENT TREE entry possesses." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "Entry", :type => "BICEPS_ParticipantModel::ContainmentTreeEntry", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nAn entry of a CONTAINMENT TREE ENTRY." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "EntryType_Attribute", :type => "UML::String", :documentation => "Documentation:\nQualified name of the descriptor that the CONTAINMENT TREE entry represents." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "HandleRef_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :documentation => "Documentation:\nHandle reference to the descriptor that the CONTAINMENT TREE entry represents." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "ParentHandleRef_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :documentation => "Documentation:\nHandle reference to the parent descriptor of the descriptor that this CONTAINMENT TREE entry represents." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
    end
    klass "ContainmentTreeEntry", :documentation => "Documentation:\nAn entry in a CONTAINMENT TREE." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ChildrenCount_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nNumber of child ELEMENTs that the CONTAINMENT TREE entry possesses." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "EntryType_Attribute", :type => "UML::String", :documentation => "Documentation:\nQualified name of the descriptor that the CONTAINMENT TREE entry represents." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "HandleRef_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :documentation => "Documentation:\nHandle reference to the descriptor that the CONTAINMENT TREE entry represents." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "ParentHandleRef_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :documentation => "Documentation:\nHandle reference to the parent descriptor of the descriptor that this CONTAINMENT TREE entry represents." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:ContainmentTreeInfo"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "Type", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nIf given, pm:AbstractDescriptor/pm:Type of the descriptor that is conveyed with the CONTAINMENT TREE entry." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_ContainmentTree_Entry", :type => "BICEPS_ParticipantModel::ContainmentTree"
    end
    klass "DateDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "Value_Attribute", :type => "UML::Date"
    end
    klass "DateTimeDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "Value_Attribute", :type => "UML::Datetime"
    end
    klass "DistributionSampleArrayMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nDeclares a sample array that represents linear value distributions in the form of arrays containing scaled sample values. In contrast to real-time sample arrays, distribution sample arrays provide observed spatial values, not time points.\n\nNOTE—An example for a distribution sample array metric might be a fourier-transformed electroencephalogram to derive frequency distribution." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "DistributionRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nMinimum and maximum domain values. A linear scale is assumed unless a step width is given." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "DomainUnit", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe CODED VALUE that is used for domain values (x axis)." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Resolution_Attribute", :type => "UML::Float", :lower => 1, :documentation => "Documentation:\nThe resolution of the means to determine the METRIC's value. Resolution is the minimum determinable difference between two determined values."
      property "TechnicalRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe maximum range of the values of the distribution sample array." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "DistributionSampleArrayMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a distribution sample array METRIC descriptor. It contains a list of sample values. This sample array is used to transport spatial range information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "MetricValue", :type => "BICEPS_ParticipantModel::SampleArrayValue", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL current value of the METRIC." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "PhysiologicalRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe physiological reasonable range of determined values.\n\nNOTE—This is not an alarming range." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "EnsembleContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide ensemble information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_SystemContextDescriptor_EnsembleContext", :type => "BICEPS_ParticipantModel::SystemContextDescriptor"
    end
    klass "EnsembleContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state that identifies an ensemble of POC MEDICAL DEVICEs. How the ensemble is grouped and what meaning is conveyed by the ensemble, is determined by other means." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "EntryRef", :documentation => "Documentation:\nA list of CONTAINMENT TREE ENTRY handle references." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      applied_stereotype :instance_of => "XML_Schema::list"
      property "inv_RelationType_1_Entries", :type => "BICEPS_ParticipantModel::RelationType_1"
      property "listItem_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY
    end
    klass "EnumStringMetricDescriptor", :parents => ["BICEPS_ParticipantModel::StringMetricDescriptor"], :documentation => "Documentation:\nAn enumerated string METRIC represents a textual status or annotation information with a constrained set of possible values.\n\nExample: a ventilation mode." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AllowedValue", :type => "BICEPS_ParticipantModel::AllowedValueType_1", :aggregation => :shared, :lower => 1, :upper => Float::INFINITY, :documentation => "Documentation:\nList of values that the enumerated string METRIC accepts as a valid value." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "EnumStringMetricState", :parents => ["BICEPS_ParticipantModel::StringMetricState"], :documentation => "Documentation:\nState of an enumerated string METRIC." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "GYearDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "Value_Attribute", :type => "UML::String"
    end
    klass "GYearMonthDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "Value_Attribute", :type => "UML::String"
    end
    klass "ImagingProcedure", :documentation => "Documentation:\nImagingProcedure provides identifiers used by the DICOM and HL7 standard to identify the requested imaging procedures resulting from an order in a the hospital. Often these identifiers are created/assigned by the main hospital information system or departmental information systems and are taken over into any medical images by DICOM equipment in the context of this procedure.\nThe listed ELEMENTs have been taken over from the IHE Radiology Technical Framework's RAD-4 transaction (\"Procedure Scheduled\") and re-uses the identifiers listed for the HL7 Version 2.5.1 IPC segment group of the OBR segment. Therefore, it is recommended to comply to the underlying HL7 and DICOM data types in order to have seamless integration with other clinical IT such as DICOM modalities or image archives (PACS).\n\nIn order to comply to the hierarchy behind the given identifiers, the following rules (taken from IHE) SHALL apply: if a Requested Procedure is comprised of multiple Scheduled Procedure Steps and/or if a Scheduled Procedure Step is comprised of multiple Protocol Codes, each applicable Scheduled Procedure Step / Protocol Code combination is included as a separate ProcedureDetails structure, i.e., the complex type \"ProcedureDetails\" occurs the same amount of times as there are different Scheduled Procedure Step IDs plus the amount of different Scheduled Procedure Step / Protocol Code combinations." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AccessionIdentifier", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe Accession Identifier (in DICOM \"Accession ID\") is an identifier of an \"Imaging Service Request\", and is (in this ProcedureDetails context) at the top of the hierarchy. A limit of sixteen (16) characters is required to allow compatibility with DICOM." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Modality", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nThe field modality describes the type of equipment (usually DICOM equipment) used. DICOM offers a list of short identifiers for different device categories, e.g. CT for \"Computer Tomography\" or US for \"Ultrasound\". It is advised to follow the list of terms defined in the DICOM standard part 3. A limit of sixteen (16) characters for the first component is required to allow compatibility with DICOM." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ProtocolCode", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nBelow each Scheduled Procedure Step the work can be defined in more detail by defining one or more Protocol Codes under it. A limit of sixteen (16) characters for the first component and sixty-four (64) characters for the second component is required to allow compatibility with DICOM." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RequestedProcedureId", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nAn pm:ImagingProcedure/pm:AccessionIdentifier can result in various Requested Procedures, each identified uniquely (within the context of the pm:ImagingProcedure/pm:AccessionIdentifier) through a RequestedProcedureID. A limit of sixteen (16) characters is required to allow compatibility with DICOM." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ScheduledProcedureStepId", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nUnder a Study (i.e. a Study Instance UID), a Procedure reflected by these ProcedureDetails can be planned in a finer granularity by scheduling different steps that should be performed (usually at a DICOM modality), the so-called \"Scheduled Procedure Step\"s. Each of these steps is identified by a Scheduled Procedure Step ID. A limit of sixteen (16) characters is required to allow compatibility with DICOM." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "StudyInstanceUid", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe Study Instance UID is a world-wide unique identifier used by DICOM modalities to group together images in a so-called Study. This grouping is REQUIRED. Under the Study, the modality creates one or more so-called Series which again contain the images. The Series identification (\"Series Instance UID\") is part of the Procedure Details which refer to a planning process, since a modality is responsible to decide how many Series are created and how their identifiers should look like.\nA limit of sixty-four (64) characters is required to allow compatibility with DICOM, with only numbers and dot characters permitted (e.g. 1.2.134124.4.12.34)." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_OrderDetail_ImagingProcedure", :type => "BICEPS_ParticipantModel::OrderDetail"
    end
    klass "InstanceIdentifier", :documentation => "Documentation:\nAn identifier that uniquely identifies a thing or object.\n\nExamples: object identifiers for medical record numbers, order ids, location ids, etc. InstanceIdentifier is defined in accordance to [InstanceIdentifier].\n\n./@Root and ./@Extension of an instance identifier do not identify the type of the object being identified, or the type of the association between the object and the identifier - they only form the identifier itself. The identifier type SHALL be expressed by ./Type." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension_Attribute", :type => "BICEPS_ParticipantModel::ExtensionType_1", :documentation => "Documentation:\nA character string as a unique identifier within the scope of pm:InstanceIdentifier/pm:Root.\nIf a non-null Extension exists, pm:InstanceIdentifier/pm:Root specifies a namespace (\"assigning authority\" or \"identifier type\").\n\n__R5008: Extension MAY be empty if pm:InstanceIdentifier/pm:Root is the complete unique identifier.__\n\n__R5009: If pm:InstanceIdentifier/pm:Root is not a complete unique identifier and Extension is not known, then Extension SHALL be populated with a null-flavor \"Unknown\".__\n\nNOTE—How the null-flavor \"Unknown\" is encoded, depends on the use-case and type of pm:InstanceIdentifier/pm:Root URI."
      property "IdentifierName", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nIdentifierName is a localized human-readable name for the namespace represented in ./@Root.\n\nNOTE—IdentifierName has no computational value and hence can never modify the meaning of ./@Root. The purpose of IdentifierName is to assist an unaided human interpreter of an instance identifier value to interpret the identifier. Applications ought not to perform any decision-making, matching, filtering or other processing based on this presence or value of this property. It is for display and development assistance only." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Root_Attribute", :type => "BICEPS_ParticipantModel::RootType_1", :documentation => "Documentation:\nA unique identifier that guarantees the global uniqueness of the instance identifier. Root alone is allowed to build the entire instance identifier.\n\nIf ./@Extension is present, Root is the unique identifier for the \"namespace\" of the identifier in ./@Extension.\n\nIn situations where ./@Extension is known, but Root is not known, the implied value of a non-existent Root SHALL match the value defined in R0135. \n\nNOTE—Example: a POC MEDICAL DEVICE with an attached simple bar code scanner could create a new instance identifier with an unknown root and an extension that is set to the bar code number. Root is then applied later in time."
      property "Type", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nType designates the type of the instance identifier, e.g., whether it is an MRN, license number, visit number etc.\n\nSince it is possible that semantic meaning of an instance identifier is comprehensively conveyed using the encompassing XML ELEMENTs, Type is OPTIONAL." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AbstractContextState_Identification", :type => "BICEPS_ParticipantModel::AbstractContextState"
      property "inv_AbstractContextState_Validator", :type => "BICEPS_ParticipantModel::AbstractContextState"
      property "inv_AllowedValueType_1_Identification", :type => "BICEPS_ParticipantModel::AllowedValueType_1"
      property "inv_ApprovedJurisdictions_ApprovedJurisdiction", :type => "BICEPS_ParticipantModel::ApprovedJurisdictions"
      property "inv_ImagingProcedure_AccessionIdentifier", :type => "BICEPS_ParticipantModel::ImagingProcedure"
      property "inv_ImagingProcedure_RequestedProcedureId", :type => "BICEPS_ParticipantModel::ImagingProcedure"
      property "inv_ImagingProcedure_ScheduledProcedureStepId", :type => "BICEPS_ParticipantModel::ImagingProcedure"
      property "inv_ImagingProcedure_StudyInstanceUid", :type => "BICEPS_ParticipantModel::ImagingProcedure"
      property "inv_LocationReference_Identification", :type => "BICEPS_ParticipantModel::LocationReference"
      property "inv_PerformedOrderDetailType_1_FillerOrderNumber", :type => "BICEPS_ParticipantModel::PerformedOrderDetailType_1"
      property "inv_PersonReference_Identification", :type => "BICEPS_ParticipantModel::PersonReference"
      property "inv_ProductionSpecificationType_1_ComponentId", :type => "BICEPS_ParticipantModel::ProductionSpecificationType_1"
      property "inv_RelationType_1_Identification", :type => "BICEPS_ParticipantModel::RelationType_1"
      property "inv_RequestedOrderDetailType_1_PlacerOrderNumber", :type => "BICEPS_ParticipantModel::RequestedOrderDetailType_1"
      property "inv_UdiType_1_Issuer", :type => "BICEPS_ParticipantModel::UdiType_1"
      property "inv_UdiType_1_Jurisdiction", :type => "BICEPS_ParticipantModel::UdiType_1"
      property "inv_WorkflowDetailType_1_VisitNumber", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
    klass "LimitAlertConditionDescriptor", :parents => ["BICEPS_ParticipantModel::AlertConditionDescriptor"], :documentation => "Documentation:\nLimitAlertConditionDescriptor is a specialization of an ALERT CONDITION that is active if at least one limit for a referenced METRIC has been violated." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AutoLimitSupported_Attribute", :type => "UML::Boolean", :documentation => "Documentation:\nAutoLimitSupported indicates whether (true) or not (false) a limit ALERT CONDITION provides support for automatic limit adaption. The implied value SHALL be \"false\"."
      property "MaxLimits", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe maximum possible range for the limit bounds.\n\nExample: the ECG values can be measured in a specific range. At some point the measured values are afflicted with noise, which makes them not valid for limits. Therefore, the maximum allowed range can be restricted.\n\nThe unit of the limits in pm:Range SHALL be the unit of the referenced pm:AlertConditionDescriptor/pm:Source." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "LimitAlertConditionState", :parents => ["BICEPS_ParticipantModel::AlertConditionState"], :documentation => "Documentation:\nA state of a limit ALERT CONDITION." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AutoLimitActivationState_Attribute", :type => "BICEPS_ParticipantModel::AlertActivation", :documentation => "Documentation:\nIndicates if the limits for the limit ALERT CONDITION are adjusted automatically.\n\n\"On\": limit is adjusted automatically\n\"Off\": limit is not adjusted automatically\n\"Psd\": limit is temporarily not adjusted automatically"
      property "Limits", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nLimit values for the pm:LimitAlertConditionState type.\n\nThe unit of the limits in pm:Range SHALL be the unit of the referenced pm:AlertConditionDescriptor/pm:Source." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MonitoredAlertLimits_Attribute", :type => "BICEPS_ParticipantModel::AlertConditionMonitoredLimits", :lower => 1, :documentation => "Documentation:\nMonitored alert limits of the limit ALERT CONDITION. See pm:AlertConditionMonitoredLimits\n\nNOTE—If the pm:AlertConditionState/@ActivationState ATTRIBUTE is not set to \"On\", the presence of the limit ALERT CONDITION is not detected. From a consumer perspective this is equivalent to the case when the pm:AlertConditionState/@ActivationState ATTRIBUTE is \"On\" and pm:AlertConditionState/@MonitoredAlertLimits is \"None\". The difference is that in the latter case the source for the ALERT CONDITION is still supervised, but the presence flag is not generated even if the limits are violated."
    end
    klass "LocalizedText", :documentation => "Documentation:\nLocalizedText is a bundled ELEMENT to reference texts in different languages or to provide a text in a specific language.\n\nThe goal of text references is to shrink the overall size of the MDIB by only providing a single reference to a text file that translates a text into multiple languages instead of flooding the MDIB with all translated texts. Referenced texts can be requested by the LOCALIZATION SERVICE. If no LOCALIZATION SERVICE exist, the application can make use of LocalizedText to represent a text in a single language.\n\n__R5047: If ./@Lang and ./@Ref are present, then the text SHALL be only available in the language specified by ./@Lang.__\n\n__R5048: If ./@Lang is present and ./@Ref is not present, then ./@Lang SHALL specify the language of the LocalizedText's content. The Text is not available through the LOCALIZATION SERVICE.__\n\n__R5049: If ./@Lang is not present and ./@Ref is present, then the text SHALL be available through the LOCALIZATION SERVICE.__\n\n__R5050: If ./@Lang and ./@Ref are not present, then the language of the LocalizedText's content is unknown. The text SHALL NOT be available through the LOCALIZATION SERVICE.__" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Lang_Attribute", :type => "UML::String", :documentation => "Documentation:\nLang specifies the language of the localized text."
      property "LocalizedText_Attribute", :type => "BICEPS_ParticipantModel::LocalizedTextContent", :lower => 1
      property "Ref_Attribute", :type => "BICEPS_ParticipantModel::LocalizedTextRef", :documentation => "Documentation:\nReferences a text in a localized text file.\n\nText references SHALL be unique regardless of any HANDLE name."
      property "TextWidth_Attribute", :type => "BICEPS_ParticipantModel::LocalizedTextWidth", :documentation => "Documentation:\nText width as defined in pm:LocalizedTextWidth."
      property "Version_Attribute", :type => "BICEPS_ParticipantModel::ReferencedVersion", :documentation => "Documentation:\nVersion defines the current revision of the referenced text in the localized text file.\n\n__R5006: Texts might change over time, but references are per definition unique and typically do not change. To check if a text that is referenced by ./@Ref has changed, pm:LocalizedText SHALL include Version if ./@Ref is set.\n\nNOTE—This saves to query a localized text file if the referencing ELEMENT has changed, but the referenced text has not. If Version is not given, a client has to assume that the text changes every time the referencing ELEMENT changes. In this case, the client is encouraged to query the localized text file on each modification.__\n\n__R5007: To keep things simple, every translated text in a localized text file that belongs to a particular reference SHALL share the same version number.__\n\nNOTE—From this it follows that if Version has changed, every translation of a referenced text is obsolete even if only a single translation has changed."
      property "inv_CalibrationDocumentationType_1_Documentation", :type => "BICEPS_ParticipantModel::CalibrationDocumentationType_1"
      property "inv_CauseInfo_Description", :type => "BICEPS_ParticipantModel::CauseInfo"
      property "inv_ClinicalInfo_Description", :type => "BICEPS_ParticipantModel::ClinicalInfo"
      property "inv_CodedValue_CodingSystemName", :type => "BICEPS_ParticipantModel::CodedValue"
      property "inv_CodedValue_ConceptDescription", :type => "BICEPS_ParticipantModel::CodedValue"
      property "inv_InstanceIdentifier_IdentifierName", :type => "BICEPS_ParticipantModel::InstanceIdentifier"
      property "inv_MetaDataType_1_Manufacturer", :type => "BICEPS_ParticipantModel::MetaDataType_1"
      property "inv_MetaDataType_1_ModelName", :type => "BICEPS_ParticipantModel::MetaDataType_1"
      property "inv_PhysicalConnectorInfo_Label", :type => "BICEPS_ParticipantModel::PhysicalConnectorInfo"
      property "inv_RemedyInfo_Description", :type => "BICEPS_ParticipantModel::RemedyInfo"
    end
    klass "LocationContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide information regarding the current spatial position." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_SystemContextDescriptor_LocationContext", :type => "BICEPS_ParticipantModel::SystemContextDescriptor"
    end
    klass "LocationContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state that identifies a location in a hospital." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "LocationDetail", :type => "BICEPS_ParticipantModel::LocationDetail", :aggregation => :shared, :documentation => "Documentation:\nLocationDetail provides human-readable detailed location information. LocationDetail SHOULD NOT be used to form location-based logical systems of devices.\n\nNOTE—Instead, pm:AbstractContextState/pm:Identification can be used to build logical groupings." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "LocationDetail", :documentation => "Documentation:\nDetails about a location. This information is derived from the HL7 PV1-3 PL." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Bed_Attribute", :type => "UML::String", :documentation => "Documentation:\nName of the bed of a location."
      property "Building_Attribute", :type => "UML::String", :documentation => "Documentation:\nName of the building of a location."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Facility_Attribute", :type => "UML::String", :documentation => "Documentation:\nName of the facility of a location."
      property "Floor_Attribute", :type => "UML::String", :documentation => "Documentation:\nName of the floor of a building."
      property "PoC_Attribute", :type => "UML::String", :documentation => "Documentation:\nName of a point of care unit, e.g., nursing unit, department, or clinic."
      property "Room_Attribute", :type => "UML::String", :documentation => "Documentation:\nName of the room of a location."
      property "inv_LocationContextState_LocationDetail", :type => "BICEPS_ParticipantModel::LocationContextState"
      property "inv_LocationReference_LocationDetail", :type => "BICEPS_ParticipantModel::LocationReference"
    end
    klass "LocationReference", :documentation => "Documentation:\nA reference to an identifiable location with human readable location details." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :upper => Float::INFINITY, :documentation => "Documentation:\nThe list of identifiers for the location." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "LocationDetail", :type => "BICEPS_ParticipantModel::LocationDetail", :aggregation => :shared, :documentation => "Documentation:\nHuman readable location details which are intended for information purposes only." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_WorkflowDetailType_1_AssignedLocation", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
    klass "MdDescription", :documentation => "Documentation:\nMdDescription is the root container to represent the descriptive part of the MDIB. The descriptive part describes the capabilities provided by a POC MEDICAL DEVICE, e.g., which measurements, alerts and settings it provides. As the descriptive part does not change as frequently as the state part, it is well-known as the (almost) static part of the MDIB. The MdDescription's counterpart is pm:MdState." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "DescriptionVersion_Attribute", :type => "BICEPS_ParticipantModel::VersionCounter", :documentation => "Documentation:\nVersion number of the description. The version number is incremented by one every time the descriptive part changes. The implied value SHALL be \"0\"."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Mds", :type => "BICEPS_ParticipantModel::MdsDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nList of MDSs that are contained in the MDIB." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_Mdib_MdDescription", :type => "BICEPS_ParticipantModel::Mdib"
    end
    klass "MdState", :documentation => "Documentation:\nMdState is the root container to represent the state part of the MDIB. The state part describes the values provided by a POC MEDICAL DEVICE, e.g., which measurement or alert values as well as patient demographics it provides. As the state part most often changes very frequently, it is well-known as the dynamic part of the MDIB. The MdState's counterpart is pm:MdDescription." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "State", :type => "BICEPS_ParticipantModel::AbstractState", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nList of states that describe the volatile status of the objects in the MDIB." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "StateVersion_Attribute", :type => "BICEPS_ParticipantModel::VersionCounter", :documentation => "Documentation:\nVersion number of the states. The version number is incremented by one every time the state part changes. The implied value SHALL be \"0\"."
      property "inv_Mdib_MdState", :type => "BICEPS_ParticipantModel::Mdib"
    end
    klass "Mdib", :documentation => "Documentation:\nRoot object that comprises the capability description of the represented MDSs in pm:MdDescription (descriptive part) as well as the current state in pm:MdState (state part)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "InstanceId_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nValue that indicates an instantiation counter.\n\n__R5004: If InstanceId is used, it SHALL be incremented by a positive value (>= 1) when the SequenceId has changed.__\n\nNOTE—This occurs each time the POC MEDICAL DEVICE has gone down, lost state, and came back up again.\n\n__R5005: InstanceId SHOULD NOT be incremented otherwise than defined in R5004.__\n\nNOTE 1—Means to set this value include, but are not limited to:\n\n- A counter that is incremented on each cold boot\n- The boot time of the POC MEDICAL DEVICE, expressed as seconds elapsed since midnight January 1, 1970\n\nNOTE 2—The MDIB version's instance id is different to the pm:InstanceIdentifier data type." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:MdibVersionGroup"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "MdDescription", :type => "BICEPS_ParticipantModel::MdDescription", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MdState", :type => "BICEPS_ParticipantModel::MdState", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MdibVersion_Attribute", :type => "BICEPS_ParticipantModel::VersionCounter", :documentation => "Documentation:\nThe unique change version number of the MDIB. The implied value of the initial version SHALL be \"0\"." do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:MdibVersionGroup"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "SequenceId_Attribute", :type => "UML::Uri", :lower => 1, :documentation => "Documentation:\nSequenceId identifies a sequence within the context of ./@InstanceId.\n\n__R5029: SequenceId SHALL be compared per RFC 3986 Section 6.2.1 Simple String Comparison [RFC3986].__" do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:MdibVersionGroup"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
    end
    klass "MdsDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor"], :documentation => "Documentation:\nMdsDescriptor represents an MDS that in turn represents a POC MEDICAL DEVICE such as an anesthesia workstation. It contains an abstraction of the hardware specification of a POC MEDICAL DEVICE plus a list of VMDs, contextual information and clock object.\n\nNOTE—The IEEE 11073-10201 has different specializations that are all representable by MdsDescriptor." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ApprovedJurisdictions", :type => "BICEPS_ParticipantModel::ApprovedJurisdictions", :aggregation => :shared, :documentation => "Documentation:\nList of regions in which the the MDS is approved to be operated. If the list does not contain any entries, then the MDS is not approved for any region. If the list is not specified, then the MDS is approved to be operated in any region." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Battery", :type => "BICEPS_ParticipantModel::BatteryDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nIf supported, an MDS inserts battery capabilities here." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Clock", :type => "BICEPS_ParticipantModel::ClockDescriptor", :aggregation => :shared, :documentation => "Documentation:\nIf supported, an MDS inserts date/time capabilities here." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MetaData", :type => "BICEPS_ParticipantModel::MetaDataType_1", :aggregation => :shared, :documentation => "Documentation:\nDescribes POC MEDICAL DEVICE meta data." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "SystemContext", :type => "BICEPS_ParticipantModel::SystemContextDescriptor", :aggregation => :shared, :documentation => "Documentation:\nSee pm:SystemContextDescriptor." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Vmd", :type => "BICEPS_ParticipantModel::VmdDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOrdered list of VMDs that belongs to the MDS. The list is ordered by the position of the VMD in the list where the ELEMENT with a lower list index has a higher clinical relevance than any entry with a higher list index. The SERVICE PROVIDER defines the clinical relevance and MAY reorder the list at any time." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MdDescription_Mds", :type => "BICEPS_ParticipantModel::MdDescription"
    end
    klass "MdsState", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentState"], :documentation => "Documentation:\nDefinition of the state of an pm:MdsDescriptor." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Lang_Attribute", :type => "UML::String", :documentation => "Documentation:\nThe current locale information that is configured for an MDS in accordance with RFC 5646 (see http://tools.ietf.org/html/rfc5646). For example, this is the language that is used for display purposes on the UI. The implied value SHALL be \"en\"."
      property "OperatingJurisdiction", :type => "BICEPS_ParticipantModel::OperatingJurisdiction", :aggregation => :shared, :documentation => "Documentation:\nThe current region information that is configured for the MDS. See also pm:OperatingJurisdiction. OperatingJurisdiction SHALL NOT be inserted if there is no pm:MdsDescriptor/pm:ApprovedJurisdictions list present." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "OperatingMode_Attribute", :type => "BICEPS_ParticipantModel::MdsOperatingMode", :documentation => "Documentation:\nThe operating mode of an MDS. Typically, an MDS operates in normal mode, so the implied value SHALL be \"Nml\"."
    end
    klass "MeansContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide information about utilized means." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_SystemContextDescriptor_MeansContext", :type => "BICEPS_ParticipantModel::SystemContextDescriptor"
    end
    klass "MeansContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state to identify a means that is utilized by an MDS or a part of it." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "Measurement", :documentation => "Documentation:\nMeasurement describes a measurement and is used only for stateful object attributes that do not have a reference to a descriptor object.\n\nExample: Weight of a patient." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MeasuredValue_Attribute", :type => "UML::Float", :lower => 1, :documentation => "Documentation:\nThe value of pm:Measurement."
      property "MeasurementUnit", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe unit (dimension) of pm:Measurement." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AllowedValueType_1_Characteristic", :type => "BICEPS_ParticipantModel::AllowedValueType_1"
      property "inv_BatteryDescriptor_CapacityFullCharge", :type => "BICEPS_ParticipantModel::BatteryDescriptor"
      property "inv_BatteryDescriptor_CapacitySpecified", :type => "BICEPS_ParticipantModel::BatteryDescriptor"
      property "inv_BatteryDescriptor_VoltageSpecified", :type => "BICEPS_ParticipantModel::BatteryDescriptor"
      property "inv_BatteryState_CapacityRemaining", :type => "BICEPS_ParticipantModel::BatteryState"
      property "inv_BatteryState_Current", :type => "BICEPS_ParticipantModel::BatteryState"
      property "inv_BatteryState_RemainingBatteryTime", :type => "BICEPS_ParticipantModel::BatteryState"
      property "inv_BatteryState_Temperature", :type => "BICEPS_ParticipantModel::BatteryState"
      property "inv_BatteryState_Voltage", :type => "BICEPS_ParticipantModel::BatteryState"
      property "inv_CalibrationResultType_1_Value", :type => "BICEPS_ParticipantModel::CalibrationResultType_1"
      property "inv_NeonatalPatientDemographicsCoreData_BirthLength", :type => "BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData"
      property "inv_NeonatalPatientDemographicsCoreData_BirthWeight", :type => "BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData"
      property "inv_NeonatalPatientDemographicsCoreData_GestationalAge", :type => "BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData"
      property "inv_NeonatalPatientDemographicsCoreData_HeadCircumference", :type => "BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData"
      property "inv_PatientDemographicsCoreData_Height", :type => "BICEPS_ParticipantModel::PatientDemographicsCoreData"
      property "inv_PatientDemographicsCoreData_Weight", :type => "BICEPS_ParticipantModel::PatientDemographicsCoreData"
      property "inv_RelatedMeasurementType_1_Value", :type => "BICEPS_ParticipantModel::RelatedMeasurementType_1"
    end
    klass "MetaDataType_1", :documentation => "Documentation:\nDescribes POC MEDICAL DEVICE meta data." do
      property "ExpirationDate", :type => "UML::Datetime", :documentation => "Documentation:\nOPTIONAL date and time of expiry of the device (if applicable)." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "LotNumber", :type => "UML::String", :documentation => "Documentation:\nOPTIONAL lot number of manufacturer." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ManufactureDate", :type => "UML::Datetime", :documentation => "Documentation:\nOPTIONAL date when the device was made." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Manufacturer", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL texts that describe the manufacturer name." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ModelName", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL texts that describe the model name." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ModelNumber", :type => "UML::String", :documentation => "Documentation:\nOPTIONAL model number of the MDS." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "SerialNumber", :type => "UML::String", :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL serial numbers of the system." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Udi", :type => "BICEPS_ParticipantModel::UdiType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nUDI fragments as defined by the FDA.\n\nNOTE 1—The amount of ELEMENTs is unbounded in order to support the provision of UDIs from different jurisdictions.\nNOTE 2—If needed, the UDI's distinct identification code can be inserted as an extension to the MetaData object." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MdsDescriptor_MetaData", :type => "BICEPS_ParticipantModel::MdsDescriptor"
    end
    klass "MetricQualityType_1", :documentation => "Documentation:\nThe quality state of the determined value of a METRIC." do
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Mode_Attribute", :type => "BICEPS_ParticipantModel::GenerationMode", :documentation => "Documentation:\nDescribes whether data is generated by a real METRIC source or is part of any test or demo data. The implied value SHALL be \"Real\"."
      property "Qi_Attribute", :type => "BICEPS_ParticipantModel::QualityIndicator", :documentation => "Documentation:\nSee pm:QualityIndicator. The implied value SHALL be \"1\"."
      property "Validity_Attribute", :type => "BICEPS_ParticipantModel::MeasurementValidity", :lower => 1, :documentation => "Documentation:\nWhile Validity is \"Ong\" or \"NA\", the enclosing METRIC value SHALL not possess a determined value. See also pm:MeasurementValidity.\n\nNOTE—In case of other values the enclosing METRIC is allowed to possess a determined value."
      property "inv_AbstractMetricValue_MetricQuality", :type => "BICEPS_ParticipantModel::AbstractMetricValue"
    end
    klass "NeonatalPatientDemographicsCoreData", :parents => ["BICEPS_ParticipantModel::PatientDemographicsCoreData"], :documentation => "Documentation:\nNeonatalPatientDemographicsCoreData constitutes patient demographics for neonates." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "BirthLength", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nPatient length at birth time." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "BirthWeight", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nPatient weight at birth time." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "GestationalAge", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nGestational age." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "HeadCircumference", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nHead circumference at birth time." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Mother", :type => "BICEPS_ParticipantModel::PersonReference", :aggregation => :shared, :documentation => "Documentation:\nInformation about the mother of the neonate." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "NumericMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nSpecification of a METRIC descriptor type that represents a single numerical measurement and status information. Example: a heart rate measurement." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AveragingPeriod_Attribute", :type => "UML::String", :documentation => "Documentation:\nTimespan from where the measured values are used to determine the METRIC's value by averaging with some algorithm.\n\nNOTE—The averaging period defined in the descriptor might be not the currently active averaging period. The active averaging period is part of pm:NumericMetricState."
      property "Resolution_Attribute", :type => "UML::Float", :lower => 1, :documentation => "Documentation:\nThe resolution of the means to determine the METRIC's value. The resolution is the minimum determinable difference between two determined values."
      property "TechnicalRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe technical possible range of determined values." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "NumericMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a numeric METRIC." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ActiveAveragingPeriod_Attribute", :type => "UML::String", :documentation => "Documentation:\nOPTIONAL information of the currently active time period used to average values if it is different from the default value that is defined in the descriptor."
      property "MetricValue", :type => "BICEPS_ParticipantModel::NumericMetricValue", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL current value of the METRIC." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "PhysiologicalRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe physiological reasonable range of determined values.\n\nNOTE—This is not an alarming range." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "NumericMetricValue", :parents => ["BICEPS_ParticipantModel::AbstractMetricValue"], :documentation => "Documentation:\nNumeric value of a METRIC state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Value_Attribute", :type => "UML::Float", :documentation => ""
      property "inv_NumericMetricState_MetricValue", :type => "BICEPS_ParticipantModel::NumericMetricState"
    end
    klass "OperatingJurisdiction", :parents => ["BICEPS_ParticipantModel::InstanceIdentifier"], :documentation => "Documentation:\nThe current region information that is configured for a component. The preferred root SHOULD be https://unstats.un.org/unsd/methodology/m49, which addresses the \"Standard country or area codes for statistical use (M49)\". Example: a root of \"https://unstats.un.org/unsd/methodology/m49\" with an extension value of \"276\" addresses Germany." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_MdsState_OperatingJurisdiction", :type => "BICEPS_ParticipantModel::MdsState"
      property "inv_VmdState_OperatingJurisdiction", :type => "BICEPS_ParticipantModel::VmdState"
    end
    klass "OperationGroupType_1", :documentation => "Documentation:\nOperationGroup defines groups of operations in order to allow clinical grouping and prioritization of operations." do
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "OperatingMode_Attribute", :type => "BICEPS_ParticipantModel::OperatingMode", :documentation => "Documentation:\nOperatingMode defines the operating mode of the whole operation group, see also pm:OperatingMode."
      property "Operations_Attribute", :type => "BICEPS_ParticipantModel::OperationRef", :aggregation => :shared, :documentation => "Documentation:\nHandle references to all operations enclosed by the operation group. The list is ordered such that the entry with a lower list index has a higher clinical relevance than any entry with a higher list index. The SERVICE PROVIDER defines the clinical relevance and MAY reorder the list at any time."
      property "Type", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nType sematically describes the operation group." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_ScoState_OperationGroup", :type => "BICEPS_ParticipantModel::ScoState"
    end
    klass "OperationRef", :documentation => "Documentation:\nA list of operation handle references." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      applied_stereotype :instance_of => "XML_Schema::list"
      property "inv_OperationGroupType_1_Operations", :type => "BICEPS_ParticipantModel::OperationGroupType_1"
      property "inv_ScoState_InvocationRequested", :type => "BICEPS_ParticipantModel::ScoState"
      property "inv_ScoState_InvocationRequired", :type => "BICEPS_ParticipantModel::ScoState"
      property "listItem_Attribute", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY
    end
    klass "OperatorContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide operator information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_SystemContextDescriptor_OperatorContext", :type => "BICEPS_ParticipantModel::SystemContextDescriptor"
    end
    klass "OperatorContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state that identifies an operator of an MDS or a part of it." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "OperatorDetails", :type => "BICEPS_ParticipantModel::BaseDemographics", :aggregation => :shared, :documentation => "Documentation:\nHuman-readable details (i.e., name) about the operator." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "OrderDetail", :documentation => "Documentation:\nDetails of an order that will be performed or that has been performed." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "End", :type => "UML::Datetime", :documentation => "Documentation:\nData for end of requested/performed procedure." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ImagingProcedure", :type => "BICEPS_ParticipantModel::ImagingProcedure", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nImagingProcedure provide identifiers used by the DICOM and HL7 standard to identify the requested imaging procedures resulting from an order in a the hospital." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Performer", :type => "BICEPS_ParticipantModel::PersonParticipation", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nNames with roles of attending staff." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Service", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nIdentifier and textual descriptions of requested/performed procedures " do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Start", :type => "UML::Datetime", :documentation => "Documentation:\nData for start of requested/performed procedure." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PatientContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS possesses a patient-device association." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_SystemContextDescriptor_PatientContext", :type => "BICEPS_ParticipantModel::SystemContextDescriptor"
    end
    klass "PatientContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nObserved information about a patient, e.g., demographics.\n\nNOTE—PatientContextState contains information that is typical for a header in an anamnesis questionnaire." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "CoreData", :type => "BICEPS_ParticipantModel::PatientDemographicsCoreData", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PatientDemographicsCoreData", :parents => ["BICEPS_ParticipantModel::BaseDemographics"], :documentation => "Documentation:\nThe patient demographics data as defined in ISO/IEEE 11073-10201:2004 (6.10.1 Patient Demographics object).\n\t\t\t\n__R5012: If the POC MEDICAL DEVICE itself has patient-related observations (e.g., weight, height, etc.) as in- or output, these SHOULD be modelled as METRICs.__\n\nNOTE—In contrast to PatientDemographicsCoreData, METRICs provide a sophisticated observation description, e.g., regarding quality and time-related attributes.\n\n__R5013: The pm:PatientDemographicsCoreData type is intended to be used for information purposes only. Whenever a value is available, it is considered as valid. Invalid values SHALL not be transmitted.__" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "DateOfBirth", :type => "BICEPS_ParticipantModel::DateOfBirthType_1", :aggregation => :shared, :documentation => "Documentation:\nDate of birth of the patient.\n\nIf the timepoint of birth matters, the value SHALL be populated with a time zone." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Height", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nHeight of the patient." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "PatientType", :type => "BICEPS_ParticipantModel::PatientType", :documentation => "Documentation:\nCategory of the patient. It refers to the ISO/IEEE 11073-10201:2004 PatientType." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Race", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nRace of the patient." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Sex", :type => "BICEPS_ParticipantModel::Sex", :documentation => "Documentation:\nSex of the patient." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Weight", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :documentation => "Documentation:\nWeight of the patient." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_PatientContextState_CoreData", :type => "BICEPS_ParticipantModel::PatientContextState"
    end
    klass "PerformedOrderDetailType_1", :parents => ["BICEPS_ParticipantModel::OrderDetail"], :documentation => "Documentation:\nRecent state of order details after order has been performed." do
      property "FillerOrderNumber", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :documentation => "Documentation:\nReference key of the order filler system/ departmental system completing or changing order details." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ResultingClinicalInfo", :type => "BICEPS_ParticipantModel::ClinicalInfo", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nClinical information resulting from the service." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_WorkflowDetailType_1_PerformedOrderDetail", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
    klass "PersonParticipation", :parents => ["BICEPS_ParticipantModel::PersonReference"], :documentation => "Documentation:\nA reference to an identifiable person with a name that participates in a role." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Role", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nRoles the referenced person acts in the relationship." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_OrderDetail_Performer", :type => "BICEPS_ParticipantModel::OrderDetail"
    end
    klass "PersonReference", :documentation => "Documentation:\nA reference to an identifiable person with a name." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :upper => Float::INFINITY, :documentation => "Documentation:\nThe list of identifiers for the person." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Name", :type => "BICEPS_ParticipantModel::BaseDemographics", :aggregation => :shared, :documentation => "Documentation:\nThe name of the person." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_NeonatalPatientDemographicsCoreData_Mother", :type => "BICEPS_ParticipantModel::NeonatalPatientDemographicsCoreData"
      property "inv_RequestedOrderDetailType_1_ReferringPhysician", :type => "BICEPS_ParticipantModel::RequestedOrderDetailType_1"
      property "inv_RequestedOrderDetailType_1_RequestingPhysician", :type => "BICEPS_ParticipantModel::RequestedOrderDetailType_1"
      property "inv_WorkflowDetailType_1_Patient", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
    klass "PhysicalConnectorInfo", :documentation => "Documentation:\nPhysicalConnectorInfo defines a number in order to allow to guide the clinical user for a failure, e.g., in case of a disconnection of a sensor or an ultrasonic handpiece." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Label", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nA human-readable label that describes the physical connector." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Number_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nNumber designates the connector number of the physical connector."
      property "inv_AbstractDeviceComponentState_PhysicalConnector", :type => "BICEPS_ParticipantModel::AbstractDeviceComponentState"
      property "inv_AbstractMetricState_PhysicalConnector", :type => "BICEPS_ParticipantModel::AbstractMetricState"
    end
    klass "ProductionSpecificationType_1", :documentation => "Documentation:\nList of production specifications of the component. The production specification describes ELEMENTs such as part numbers, serial numbers, revisions, etc." do
      property "ComponentId", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :documentation => "Documentation:\nDescribes the internal component unique identification. This is a provision for manufacturer specific standard components using a private object identifier (OID)." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ProductionSpec", :type => "UML::String", :lower => 1, :documentation => "Documentation:\nProductionSpec describes the printable string of the production specification ELEMENT." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "SpecType", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nSpecType is the specification type, such as serial number, part number, hardware revision, software revision, etc." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AbstractDeviceComponentDescriptor_ProductionSpecification", :type => "BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"
    end
    klass "Range", :documentation => "Documentation:\nA range of decimal values which provides a lower and an upper bound as well as a step width." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AbsoluteAccuracy_Attribute", :type => "UML::Float", :documentation => "Documentation:\nMaximum absolute error in relation to the correct value within the given range."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Lower_Attribute", :type => "UML::Float", :documentation => "Documentation:\nThe including lower bound of the range."
      property "RelativeAccuracy_Attribute", :type => "UML::Float", :documentation => "Documentation:\nMaximum relative error in relation to the correct value within the given range."
      property "StepWidth_Attribute", :type => "UML::Float", :documentation => "Documentation:\nThe numerical distance between two values in the range of the given upper and lower bound."
      property "Upper_Attribute", :type => "UML::Float", :documentation => "Documentation:\nThe including upper bound of the range."
      property "inv_DistributionSampleArrayMetricDescriptor_DistributionRange", :type => "BICEPS_ParticipantModel::DistributionSampleArrayMetricDescriptor"
      property "inv_DistributionSampleArrayMetricDescriptor_TechnicalRange", :type => "BICEPS_ParticipantModel::DistributionSampleArrayMetricDescriptor"
      property "inv_DistributionSampleArrayMetricState_PhysiologicalRange", :type => "BICEPS_ParticipantModel::DistributionSampleArrayMetricState"
      property "inv_LimitAlertConditionDescriptor_MaxLimits", :type => "BICEPS_ParticipantModel::LimitAlertConditionDescriptor"
      property "inv_LimitAlertConditionState_Limits", :type => "BICEPS_ParticipantModel::LimitAlertConditionState"
      property "inv_NumericMetricDescriptor_TechnicalRange", :type => "BICEPS_ParticipantModel::NumericMetricDescriptor"
      property "inv_NumericMetricState_PhysiologicalRange", :type => "BICEPS_ParticipantModel::NumericMetricState"
      property "inv_RealTimeSampleArrayMetricDescriptor_TechnicalRange", :type => "BICEPS_ParticipantModel::RealTimeSampleArrayMetricDescriptor"
      property "inv_RealTimeSampleArrayMetricState_PhysiologicalRange", :type => "BICEPS_ParticipantModel::RealTimeSampleArrayMetricState"
      property "inv_ReferenceRangeType_1_Range", :type => "BICEPS_ParticipantModel::ReferenceRangeType_1"
      property "inv_SetValueOperationState_AllowedRange", :type => "BICEPS_ParticipantModel::SetValueOperationState"
    end
    klass "RealTimeSampleArrayMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nDeclares a sample array that represents a real-time continuous waveform. An example would be an electrocardiogram (ECG) real-time wave." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Resolution_Attribute", :type => "UML::Float", :lower => 1, :documentation => "Documentation:\nThe resolution of the means to determine the METRIC's value. The Resolution is the minimum determinable difference between two determined values."
      property "SamplePeriod_Attribute", :type => "UML::String", :lower => 1, :documentation => "Documentation:\nThe sample period of the real-time sample array, i.e., how often waveform samples are generated. SamplePeriod is always given as a period between samples, e.g., 5 milliseconds."
      property "TechnicalRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe maximum range of the values of the real-time sample array." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "RealTimeSampleArrayMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a stream METRIC descriptor. It contains a list of sample values. This sample array is used to transport waveform stream information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "MetricValue", :type => "BICEPS_ParticipantModel::SampleArrayValue", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL current value of the METRIC." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "PhysiologicalRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe physiological reasonable range of determined values.\n\nNOTE—This is not an alarming range." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "RealTimeValueType", :documentation => "Documentation:\nDefines the real-time sample array value type comprising a whitespace separated list of decimal numbers." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      applied_stereotype :instance_of => "XML_Schema::list"
      property "inv_SampleArrayValue_Samples", :type => "BICEPS_ParticipantModel::SampleArrayValue"
      property "listItem_Attribute", :type => "UML::Float", :upper => Float::INFINITY
    end
    klass "ReferenceRangeType_1", :documentation => "Documentation:\nRepresentation of the normal or abnormal reference range for the measurement." do
      property "Meaning", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nIndicates the meaning of the reference range." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Range", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nRange that SHALL be populated at least with ./@Lower or ./@Upper." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_RelatedMeasurementType_1_ReferenceRange", :type => "BICEPS_ParticipantModel::RelatedMeasurementType_1"
    end
    klass "RelatedMeasurementType_1", :documentation => "Documentation:\nRelated measurements for this clinical observation if applicable." do
      property "ReferenceRange", :type => "BICEPS_ParticipantModel::ReferenceRangeType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nRepresentation of the normal or abnormal reference range for the measurement." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Validity_Attribute", :type => "BICEPS_ParticipantModel::MeasurementValidity", :documentation => "Documentation:\nValidity of the related measurement. See also pm:MeasurementValidity."
      property "Value", :type => "BICEPS_ParticipantModel::Measurement", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nThe related measurement's value." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_ClinicalInfo_RelatedMeasurement", :type => "BICEPS_ParticipantModel::ClinicalInfo"
    end
    klass "RelationType_1", :documentation => "Documentation:\nRelation allows the modelling of relationships between a metric and other containtment tree entries. Related containment tree entries are defined in ./@Entries, whereby the flavor of a relationship can be set up in ./@Kind.\n\nThe cardinality of Relation is zero or more in order to express relations of different flavors for the same METRIC.\n\nNOTE—Example: some settings of high frequency cutting devices cause changes in measurements (e.g., current form can influences the maximum emitted power). If such a setting is controllable by external means, presumably the SERVICE CONSUMER wants to be able to gain knowledge of affected measurements, which might be then accessed through the Relation element." do
      property "Code", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :documentation => "Documentation:\nCode allows to semantically describe the relationship between the METRIC and the list of related containment tree entries defined in ./pm:Relation/@Entries." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Entries_Attribute", :type => "BICEPS_ParticipantModel::EntryRef", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nList of HANDLE references that relate to the METRIC. The relationship flavor is defined in ./pm:Relation/@Kind."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :documentation => "Documentation:\nIdentification allow relations to be grouped by instance identifiers.\n\nNOTE—By that a SERVICE PROVIDER can, e.g., group sets of recommendations or presettings in order to allow easy identification." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Kind_Attribute", :type => "BICEPS_ParticipantModel::KindType_1", :lower => 1, :documentation => "Documentation:\nKind specifies the relationship between the METRIC and referenced containment tree entries. Referenced containment tree entries are defined in ./pm:Relation/@Entries."
      property "inv_AbstractMetricDescriptor_Relation", :type => "BICEPS_ParticipantModel::AbstractMetricDescriptor"
    end
    klass "RemedyInfo", :documentation => "Documentation:\nRemedy information for a cause of an ALERT CONDITION." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Description", :type => "BICEPS_ParticipantModel::LocalizedText", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOPTIONAL human-readable texts that describe the remedy information." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_CauseInfo_RemedyInfo", :type => "BICEPS_ParticipantModel::CauseInfo"
    end
    klass "RequestedOrderDetailType_1", :parents => ["BICEPS_ParticipantModel::OrderDetail"], :documentation => "Documentation:\nInitial order details at the time of order release." do
      property "PlacerOrderNumber", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nReference key of the order generating system." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "ReferringPhysician", :type => "BICEPS_ParticipantModel::PersonReference", :aggregation => :shared, :documentation => "Documentation:\nPhysician as the initiator of the clinical process, e.g. the general practitioner." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RequestingPhysician", :type => "BICEPS_ParticipantModel::PersonReference", :aggregation => :shared, :documentation => "Documentation:\nSource that initiated the order." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_WorkflowDetailType_1_RequestedOrderDetail", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
    klass "SampleArrayValue", :parents => ["BICEPS_ParticipantModel::AbstractMetricValue"], :documentation => "Documentation:\nType that contains sequences of values, i.e., sample arrays.\n\nThe ./pmMetricQuality ELEMENT relates to all samples.\n\nNOTE 1—pm:Timestamp (see base: pm:AbstractMetricValue) refers to the first value of the array. The individual timestamps of the values can thus be computed from the sample rate (see pm:RealTimeSampleArrayMetricDescriptor).\nNOTE 2—If ./pmMetricQuality cannot be applied to all samples due to, e.g., some invalid values, a SERVICE PROVIDER can decide to set ./pmMetricQuality/@Validity to \"Qst\" or \"Inv\"." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ApplyAnnotation", :type => "BICEPS_ParticipantModel::ApplyAnnotationType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nAnnotations MAY only apply to specific values in the real-time sample array. The ApplyAnnotation set relates annotations to sample indices. If no ApplyAnnotation ELEMENT is provided all annotations are valid for all values in the context." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Samples_Attribute", :type => "BICEPS_ParticipantModel::RealTimeValueType", :aggregation => :shared, :documentation => ""
      property "inv_DistributionSampleArrayMetricState_MetricValue", :type => "BICEPS_ParticipantModel::DistributionSampleArrayMetricState"
      property "inv_RealTimeSampleArrayMetricState_MetricValue", :type => "BICEPS_ParticipantModel::RealTimeSampleArrayMetricState"
    end
    klass "ScoDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nScoDescriptor describes the capabilities of the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Operation", :type => "BICEPS_ParticipantModel::AbstractOperationDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nA list of operations that are provided by the SCO. The list is ordered by the position of the operation in the list where the ELEMENT with a lower list index has a higher clinical relevance than any entry with a higher list index. The SERVICE PROVIDER defines the clinical relevance and MAY reorder the list at any time." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_AbstractComplexDeviceComponentDescriptor_Sco", :type => "BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor"
    end
    klass "ScoState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nCorresponding state of pm:ScoDescriptor." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "InvocationRequested_Attribute", :type => "BICEPS_ParticipantModel::OperationRef", :aggregation => :shared, :documentation => "Documentation:\nPrioritized list of operations that are requested to be invoked by a SERVICE CONSUMER."
      property "InvocationRequired_Attribute", :type => "BICEPS_ParticipantModel::OperationRef", :aggregation => :shared, :documentation => "Documentation:\nPrioritized list of operations that are required to be invoked by a SERVICE CONSUMER."
      property "OperationGroup", :type => "BICEPS_ParticipantModel::OperationGroupType_1", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOperationGroup defines groups of operations in order to allow clinical grouping and prioritization of operations." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "SetAlertStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes an alert state set operation for a specific alert state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetAlertStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of an alert state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetComponentStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes a component state set operation for a specific component state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetComponentStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a component state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetContextStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes a context state set operation for a specific context state in the MDIB that is exposed on SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetContextStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a context state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetMetricStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes a METRIC state set operation for a specific METRIC state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetMetricStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a METRIC state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetStringOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractOperationDescriptor"], :documentation => "Documentation:\nDescribes a string set operation for a specific object state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "MaxLength_Attribute", :type => "UML::Integer", :documentation => "Documentation:\nAn optional parameter that gives the maximum length of the input string that is supported by the operation."
    end
    klass "SetStringOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a string set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AllowedValues", :type => "BICEPS_ParticipantModel::AllowedValuesType_1", :aggregation => :shared, :documentation => "Documentation:\nAn OPTIONAL list of currently allowed string values that can be requested. If the list is empty, then there is not limitation." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "SetValueOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractOperationDescriptor"], :documentation => "Documentation:\nDescribes a numeric set operation for a specific object state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetValueOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a numeric set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "AllowedRange", :type => "BICEPS_ParticipantModel::Range", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe currently allowed ranges that can be requested.\n\nNOTE—The given ranges need to be a (strict or non-strict) subset of the technical range of the referenced descriptor." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "StringMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nA string METRIC represents a textual status or annotation information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "StringMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a string METRIC." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "MetricValue", :type => "BICEPS_ParticipantModel::StringMetricValue", :aggregation => :shared, :documentation => "Documentation:\nOPTIONAL current value of the METRIC." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "StringMetricValue", :parents => ["BICEPS_ParticipantModel::AbstractMetricValue"], :documentation => "Documentation:\nString value of a METRIC state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Value_Attribute", :type => "UML::String", :documentation => ""
      property "inv_StringMetricState_MetricValue", :type => "BICEPS_ParticipantModel::StringMetricState"
    end
    klass "SystemContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nThe context of an MDS that lists the possible relationship of a POC MEDICAL DEVICE into its usage environment by means of context descriptors. Context descriptors do not contain any stateful information. They only assert that the underlying MDS can provide corresponding context state information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "EnsembleContext", :type => "BICEPS_ParticipantModel::EnsembleContextDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe ensemble context indicates that the POC MEDICAL DEVICE can provide information about the ensemble(s) that it is associated with. An ensemble represents an arbitrary grouping of POC MEDICAL DEVICE. The semantics depend on the ensemble itself.\n\nA SERVICE PROVIDER SHALL NOT insert or delete the context descriptor ELEMENT during runtime, except when the whole MDS appears/disappears." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "LocationContext", :type => "BICEPS_ParticipantModel::LocationContextDescriptor", :aggregation => :shared, :documentation => "Documentation:\nThe location context indicates that the POC MEDICAL DEVICE can provide information about the location(s) that it is associated with.\n\nA SERVICE PROVIDER SHALL NOT insert or delete the context descriptor ELEMENT during runtime, except when the whole MDS appears/disappears." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "MeansContext", :type => "BICEPS_ParticipantModel::MeansContextDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe means context indicates that the POC MEDICAL DEVICE can provide information about utilized means.\n\nA SERVICE PROVIDER SHALL NOT insert or delete the context descriptor ELEMENT during runtime, except when the whole MDS appears/disappears." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "OperatorContext", :type => "BICEPS_ParticipantModel::OperatorContextDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe operator context indicates that the POC MEDICAL DEVICE can provide information about the operator(s) that it is associated with.\n\nA SERVICE PROVIDER SHALL NOT insert or delete the context descriptor ELEMENT during runtime, except when the whole MDS appears/disappears." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "PatientContext", :type => "BICEPS_ParticipantModel::PatientContextDescriptor", :aggregation => :shared, :documentation => "Documentation:\nThe patient context indicates that the POC MEDICAL DEVICE is able to process information about the patient that it is associated with.\n\nA SERVICE PROVIDER SHALL NOT insert or delete the context descriptor ELEMENT during runtime, except when the whole MDS appears/disappears." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "WorkflowContext", :type => "BICEPS_ParticipantModel::WorkflowContextDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nThe workflow context indicates that the POC MEDICAL DEVICE can provide information about the workflow step(s) that it is associated with.\n\nA SERVICE PROVIDER SHALL NOT insert or delete the context descriptor ELEMENT during runtime, except when the whole MDS appears/disappears." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MdsDescriptor_SystemContext", :type => "BICEPS_ParticipantModel::MdsDescriptor"
    end
    klass "SystemContextState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nCorresponding state of pm:SystemContextDescriptor. This state comes with no additional attributes." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SystemSignalActivation", :documentation => "Documentation:\nDefines a tuple consisting of an pm:AlertSignalManifestation and an pm:AlertActivation to describe the alert activation state of a certain ALERT SIGNAL manifestation.\n\nExample: ./@Manifestation is \"Aud\" and ./@State is \"Psd\" means that any audible alert activation is paused.\n" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "Manifestation_Attribute", :type => "BICEPS_ParticipantModel::AlertSignalManifestation", :lower => 1, :documentation => "Documentation:\nSee pm:AlertSignalManifestation."
      property "State_Attribute", :type => "BICEPS_ParticipantModel::AlertActivation", :lower => 1, :documentation => "Documentation:\nSee pm:AlertActivation."
      property "inv_AlertSystemState_SystemSignalActivation", :type => "BICEPS_ParticipantModel::AlertSystemState"
    end
    klass "TranslationType_1", :documentation => "Documentation:\nSet of alternative or equivalent representations." do
      property "Code_Attribute", :type => "BICEPS_ParticipantModel::CodeIdentifier", :lower => 1, :documentation => "Documentation:\nA code as defined by pm:CodedValue/@Code."
      property "CodingSystemVersion_Attribute", :type => "UML::String", :documentation => "Documentation:\nA coding system version as defined by pm:CodedValue/@CodingSystemVersion."
      property "CodingSystem_Attribute", :type => "UML::Uri", :documentation => "Documentation:\nA coding system as defined by pm:CodedValue/@CodingSystem."
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_CodedValue_Translation", :type => "BICEPS_ParticipantModel::CodedValue"
    end
    klass "UdiType_1", :documentation => "Documentation:\nUDI fragments as defined by the FDA.\n\nNOTE 1—The amount of ELEMENTs is unbounded in order to support the provision of UDIs from different jurisdictions.\nNOTE 2—If needed, the UDI's distinct identification code can be inserted as an extension to the MetaData object." do
      property "DeviceIdentifier", :type => "UML::String", :lower => 1, :documentation => "Documentation:\nA mandatory, fixed portion of a UDI that identifies the labeler and the specific version or model of a device." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "HumanReadableForm", :type => "UML::String", :lower => 1, :documentation => "Documentation:\nUDI human readable form as printed on the barcode." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Issuer", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nOrganization that has issued the UDI." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Jurisdiction", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :documentation => "Documentation:\nJurisdiction that the UDI is valid for. If no value is defined, then the UDI is not bound to a specific jurisdiction." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MetaDataType_1_Udi", :type => "BICEPS_ParticipantModel::MetaDataType_1"
    end
    klass "VmdDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor"], :documentation => "Documentation:\nVmdDescriptor describes a VMD. A VMD is an abstraction for a module (medical-related subsystem) of an MDS. According to IEEE 11073-10201, an MDS with one VMD is a single purpose POC MEDICAL DEVICE in contrast to an MDS with multiple VMDs that has multiple purposes.\n\nExample of a multiple purpose POC MEDICAL DEVICE: an anesthesia workstation (one MDS) with a ventilation unit (one VMD), a patient monitoring unit (another VMD), and gas delivery/monitor system (another VMD). In the IEEE 11073-10201 a VMD might not be a hardware module, it also can be pure software." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ApprovedJurisdictions", :type => "BICEPS_ParticipantModel::ApprovedJurisdictions", :aggregation => :shared, :documentation => "Documentation:\nList of regions in which the the VMD is approved to be operated. If the list does not contain any entries, then the VMD is not approved for any region. If the list is not specified, then the VMD is approved to be operated in any region." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Channel", :type => "BICEPS_ParticipantModel::ChannelDescriptor", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nOrdered list of CHANNELs that allow hierarchical information organization of METRICs or ALERT SYSTEMs. The list is ordered by the position of the CHANNEL in the list where the ELEMENT with a lower list index has a higher clinical relevance than any entry with a higher list index. The SERVICE PROVIDER defines the clinical relevance and MAY reorder the list at any time." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_MdsDescriptor_Vmd", :type => "BICEPS_ParticipantModel::MdsDescriptor"
    end
    klass "VmdState", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentState"], :documentation => "Documentation:\nThe state of a VMD." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "OperatingJurisdiction", :type => "BICEPS_ParticipantModel::OperatingJurisdiction", :aggregation => :shared, :documentation => "Documentation:\nThe current region information that is configured for the VMD. See also pm:OperatingJurisdiction. OperatingJurisdiction SHALL NOT be inserted if there is no pm:VmdDescriptor/pm:ApprovedJurisdictions list present." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "WorkflowContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide workflow information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_SystemContextDescriptor_WorkflowContext", :type => "BICEPS_ParticipantModel::SystemContextDescriptor"
    end
    klass "WorkflowContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state to identify a step in a clinical workflow." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "WorkflowDetail", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1", :aggregation => :shared, :documentation => "Documentation:\nA workflow step for a clinical treatment or diagnostic procedure or monitoring procedure." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "WorkflowDetailType_1", :documentation => "Documentation:\nA workflow step for a clinical treatment or diagnostic procedure or monitoring procedure." do
      property "AssignedLocation", :type => "BICEPS_ParticipantModel::LocationReference", :aggregation => :shared, :documentation => "Documentation:\nLocation the order assigned to." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "DangerCode", :type => "BICEPS_ParticipantModel::CodedValue", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nIdentifier and textual descriptions of patient immanent risks, e.g., infectious diseases." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Extension", :type => "ExtensionPoint::ExtensionType", :aggregation => :shared do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "Patient", :type => "BICEPS_ParticipantModel::PersonReference", :aggregation => :shared, :lower => 1, :documentation => "Documentation:\nSubject of the order." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "PerformedOrderDetail", :type => "BICEPS_ParticipantModel::PerformedOrderDetailType_1", :aggregation => :shared, :documentation => "Documentation:\nRecent state of order details after order has been performed." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RelevantClinicalInfo", :type => "BICEPS_ParticipantModel::ClinicalInfo", :aggregation => :shared, :upper => Float::INFINITY, :documentation => "Documentation:\nClinical information that is relevant for the order." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "RequestedOrderDetail", :type => "BICEPS_ParticipantModel::RequestedOrderDetailType_1", :aggregation => :shared, :documentation => "Documentation:\nInitial order details at the time of order release." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "VisitNumber", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :aggregation => :shared, :documentation => "Documentation:\nReference key of inpatient stay or outpatient visit of the patient administration system." do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "inv_WorkflowContextState_WorkflowDetail", :type => "BICEPS_ParticipantModel::WorkflowContextState"
    end
    enumeration "AccessLevelType_1", :documentation => "Documentation:\nAccessLevel defines a user group to whom access to the operation is granted. The implied value SHALL be \"Usr\".\n\t\t\t\t\t\t\n__R5054: Access to the invocation of the operation SHALL be restricted to the defined user group by a SERVICE CONSUMER.__" do
      property "value_Attribute", :type => "UML::String"
      literal "CSUsr", :documentation => "Documentation:\nCSUsr = Clinical Super User. Individuals or entity accountable to the RESPONSIBLE ORGANIZATION that configure clinical relevant settings of a POC MEDICAL DEVICE."
      literal "Oth", :documentation => "Documentation:\nOth = Other. Access is restricted by other means (e.g., an extension)."
      literal "RO", :documentation => "Documentation:\nRO = Responsible Organization. Access is restricted to a RESPONSIBLE ORGANIZATION."
      literal "SP", :documentation => "Documentation:\nSP = Service Personnel. Access is restricted to SERVICE PERSONNEL."
      literal "Usr", :documentation => "Documentation:\nUsr = User. Any person interacting with (i.e., operating or handling) the POC MEDICAL DEVICE."
    end
    enumeration "AlertActivation", :documentation => "Documentation:\nThe activation state of any ALERT SYSTEM ELEMENT, i.e., pm:AlertSystemState, pm:AlertConditionState, pm:LimitAlertConditionState, and pm:AlertSignalState.\n\nSpecial meanings MAY apply depending on the ALERT SYSTEM ELEMENT." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Off", :documentation => "Documentation:\nThe ALERT SYSTEM ELEMENT is not operating."
      literal "On", :documentation => "Documentation:\nThe ALERT SYSTEM ELEMENT is operating."
      literal "Psd", :documentation => "Documentation:\nPsd = Paused. The ALERT SYSTEM ELEMENT is temporarily not operating."
    end
    enumeration "AlertConditionKind", :documentation => "Documentation:\nAlertConditionKind categorizes ALERT CONDITIONs by their origin." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Oth", :documentation => "Documentation:\nOth = Other. The condition arises from another origin, e.g., equipment-user advisory conditions like \"room temperature high\"."
      literal "Phy", :documentation => "Documentation:\nPhy = Physiological. The condition arises from a patient-related variable. Examples: \"blood pressure high\" or \"minute volume low\"."
      literal "Tec", :documentation => "Documentation:\nTec = Technical. The condition arises from a monitored equipment-related or ALERT SYSTEM-related variable. Examples: \"battery low\" or \"sensor unplugged\"."
    end
    enumeration "AlertConditionMonitoredLimits", :documentation => "Documentation:\nIndicates which limits of a pm:LimitAlertCondition ELEMENT are monitored to trigger ALERT SIGNALs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "All", :documentation => "Documentation:\nBoth alert limits are monitored."
      literal "HiOff", :documentation => "Documentation:\nHiOff = Hi-Off. High-limit violation detection is either currently turned off if the state possesses a high-limit value or is not supported at all."
      literal "LoOff", :documentation => "Documentation:\nLoOff = Low-Off. Low-limit violation detection is either currently turned off if the state possesses a low-limit value or is not supported at all."
      literal "None", :documentation => "Documentation:\nNo alert limits are monitored. \n\nNOTE—This flag is not equal to the activation state \"Off\" that pm:AlertConditionState/@ActivationState provides, although the result w.r.t. to alert signalization is the same."
    end
    enumeration "AlertConditionPriority", :documentation => "Documentation:\nAlertConditionPriority categorizes ALERT CONDITIONs into priorities.\n\nAlertConditionPriority can be used to distinguish the severity of the potential or actual hazard that exists if an ALERT CONDITION is present.\n\nNOTE—The priority is assigned through risk analysis." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Hi", :documentation => "Documentation:\nHi = High. Immediate response to remove the ALERT CONDITION is required."
      literal "Lo", :documentation => "Documentation:\nLo = Low. Awareness of the ALERT CONDITION is required."
      literal "Me", :documentation => "Documentation:\nMe = Medium. Prompt response to remove the ALERT CONDITION is required."
      literal "None", :documentation => "Documentation:\nNo awareness of the ALERT CONDITION is required."
    end
    enumeration "AlertSignalManifestation", :documentation => "Documentation:\nAlertSignalManifestation categorizes ALERT SIGNALs by the way they can be recognized by the alerted human, e.g., the nurse." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Aud", :documentation => "Documentation:\nAud = Audible. The ALERT SIGNAL manifests in an audible manner, i.e., the alert can be heard. Example: an alarm sound."
      literal "Oth", :documentation => "Documentation:\nOth = Other. The ALERT SIGNAL manifests in a manner not further specified."
      literal "Tan", :documentation => "Documentation:\nTan = Tangible. The ALERT SIGNAL manifests in a tangible manner, i.e., the alert can be felt. Example: vibration."
      literal "Vis", :documentation => "Documentation:\nVis = Visible. The ALERT SIGNAL manifests in a visible manner, i.e., the alert can be seen. Example: a red flashing light."
    end
    enumeration "AlertSignalPresence", :documentation => "Documentation:\nGeneration state of an ALERT SIGNAL." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Ack", :documentation => "Documentation:\nAck = Acknowledged. \"Acknowledged\" indicates that an ALERT SIGNAL is currently not generated due to an acknowledgment even if the ALERT CONDITION is still present. Acknowledged signals are those, where an auditory ALERT SIGNAL that is related to a currently active ALERT CONDITION, is inactive until the ALERT CONDITION is no longer present."
      literal "Latch", :documentation => "Documentation:\nLatch = Latched. \"Latched\" indicates that an ALERT SIGNAL is currently generated even if the ALERT CONDITION is no longer present."
      literal "Off", :documentation => "Documentation:\nIndicates that an ALERT SIGNAL is currently not generated."
      literal "On", :documentation => "Documentation:\nIndicates that an ALERT SIGNAL is currently generated."
    end
    enumeration "AlertSignalPrimaryLocation", :documentation => "Documentation:\nAlertSignalPrimaryLocation defines where the primary ALERT SIGNAL is generated." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Loc", :documentation => "Documentation:\nLoc = Local. The ALERT SIGNAL is perceivable on the machine where the ALERT CONDITION has been detected."
      literal "Rem", :documentation => "Documentation:\nRem = Remote. The ALERT SIGNAL is perceivable on a remote machine."
    end
    enumeration "CalibrationState", :documentation => "Documentation:\nCalibration state of a component." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Cal", :documentation => "Documentation:\nCal = Calibrated. Defines that a component is calibrated."
      literal "No", :documentation => "Documentation:\nNo = Not Calibrated. Defines that a component is not calibrated."
      literal "Oth", :documentation => "Documentation:\nOth = Other. The calibration state is defined by other means."
      literal "Req", :documentation => "Documentation:\nReq = Calibration Required. Defines that a component requires a calibration."
      literal "Run", :documentation => "Documentation:\nRun = Running. Defines that a calibration for a component is running."
    end
    enumeration "CalibrationType", :documentation => "Documentation:\nType of a calibration method." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Gain", :documentation => "Documentation:\nGain calibration"
      literal "Offset", :documentation => "Documentation:\nOffset calibration."
      literal "TP", :documentation => "Documentation:\nTwo point calibration."
      literal "Unspec", :documentation => "Documentation:\nUnspecified calibration type."
    end
    enumeration "CanDeescalateType_1", :documentation => "Documentation:\nIndicates if an alert condition can deescalate from one priority to another." do
      property "value_Attribute", :type => "UML::String"
      literal "Lo", :documentation => "Documentation:\nLo = Low. Alert condition can deescalate to low priority."
      literal "Me", :documentation => "Documentation:\nMe = Medium. Alert condition can deescalate to medium priority."
      literal "None", :documentation => "Documentation:\nNo = None. Alert condition can deescalate to condition with no priority."
    end
    enumeration "CanEscalateType_1", :documentation => "Documentation:\nIndicates if an alert condition can escalate from one priority to another." do
      property "value_Attribute", :type => "UML::String"
      literal "Hi", :documentation => "Documentation:\nHi = High. Alert condition can escalate to high priority."
      literal "Lo", :documentation => "Documentation:\nLo = Low. Alert condition can escalate to low priority."
      literal "Me", :documentation => "Documentation:\nMe = Medium. Alert condition can escalate to medium priority."
    end
    enumeration "ChargeStatusType_1", :documentation => "Documentation:\nCurrent charge status of the battery." do
      property "value_Attribute", :type => "UML::String"
      literal "ChB", :documentation => "Documentation:\nChB = Charging Battery. Battery is currently supplied with electric energy from an external circuit. See also IEC 60050-482 International Electrotechnical Vocabulary, 482-05-27."
      literal "DEB", :documentation => "Documentation:\nDEB = Discharged Empty Battery. Discharged secondary battery. See also IEC 60050-482 International Electrotechnical Vocabulary, 482-05-31."
      literal "DisChB", :documentation => "Documentation:\nDisChB = Discharging Battery. Battery delivers, to an external electric circuit and under specified conditions, electric energy produced in the cells. See also IEC 60050-482 International Electrotechnical Vocabulary, 482-03-23"
      literal "Ful", :documentation => "Documentation:\nFul = Full. All available active material is in a state such that the charging under the selected conditions produces no significant increase of capacity. See also IEC 60050-482 International Electrotechnical Vocabulary, 482-05-42."
    end
    enumeration "ComponentActivation", :documentation => "Documentation:\nActivation state of a component, i.e., any type that is derived from pm:AbstractComponentState and pm:AbstractMetricState." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Fail", :documentation => "Documentation:\nFail = Failure. The component has detected a failure and is not ready to be operated."
      literal "NotRdy", :documentation => "Documentation:\nNotRdy = Not Ready. The component is not ready to be operated and not operating, but initialization is ongoing."
      literal "Off", :documentation => "Documentation:\nThe component is inactive."
      literal "On", :documentation => "Documentation:\nThe component is operating."
      literal "Shtdn", :documentation => "Documentation:\nShtdn = Shutdown. The component is ceasing from being ready to be operated or operating, but not yet inactive."
      literal "StndBy", :documentation => "Documentation:\nStndBy = Stand By. The component is ready to be operated, but not currently operating."
    end
    enumeration "ContextAssociation", :documentation => "Documentation:\nDefines an association between an arbitrary context and an MDS." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Assoc", :documentation => "Documentation:\nAssoc = Associated. Context information is associated."
      literal "Dis", :documentation => "Documentation:\nDis = Disassociated. Context information is no longer associated."
      literal "No", :documentation => "Documentation:\nNo = Not Associated. There is currently no context information associated, such that there cannot be made any assumptions on the encompassing context."
      literal "Pre", :documentation => "Documentation:\nPre = Pre-Associated. Context information is in a pre-association state."
    end
    enumeration "CriticalityType_1", :documentation => "Documentation:\nPotential clinical harm if this clinical information is not considered while treating the patient. The implied value SHALL be \"Lo\"." do
      property "value_Attribute", :type => "UML::String"
      literal "Hi", :documentation => "Documentation:\nHi = High. Non-compliance might result in a severe, life-threatening, or fatal situation."
      literal "Lo", :documentation => "Documentation:\nLo = Low. Non-compliance does not result in a severe, life-threatening, or fatal situation."
    end
    enumeration "DerivationMethod", :documentation => "Documentation:\nIn some circumstances, e.g., in spot-check situations or when dealing with settings, METRIC values might be entered manually. DerivationMethod provides an enumeration to designate if a METRIC is set automatically or manually." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Auto", :documentation => "Documentation:\nAuto = Automatic derivation. The METRIC value is derived by an automatic mechanism (e.g., electronically measured)."
      literal "Man", :documentation => "Documentation:\nMan = Manual derivation. The METRIC is derived manually by a clinican/human."
    end
    enumeration "GenerationMode", :documentation => "Documentation:\nDescribes whether METRIC data is generated by real measurements or under unreal settings (demo or test data)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Demo", :documentation => "Documentation:\nDemo = Demo Data. A value that is arbitrary and is for demonstration purposes only."
      literal "Real", :documentation => "Documentation:\nReal = Real Data. A value that is generated under real conditions."
      literal "Test", :documentation => "Documentation:\nTest = Test Data. A value that is arbitrary and is for testing purposes only."
    end
    enumeration "KindType_1", :documentation => "Documentation:\nKind specifies the relationship between the METRIC and referenced containment tree entries. Referenced containment tree entries are defined in ./pm:Relation/@Entries." do
      property "value_Attribute", :type => "UML::String"
      literal "DCE", :documentation => "Documentation:\nDCE = Derived from containment tree entries. The METRIC is derived from the containment tree entries defined in ./pm:Relation/@Entries. If Kind is \"DCE\", ./pm:Relation/pm:Code MAY be set to classify the form of derivation."
      literal "ECE", :documentation => "Documentation:\nECE = Effect on containment tree entries. When changed, the METRIC has an effect on the containment tree entries defined in ./pm:Relation/@Entries. If Kind is \"ECE\", ./pm:Relation/pm:Code SHOULD be set to classify the effect."
      literal "Oth", :documentation => "Documentation:\nOth = Other. Relation is specified by other means, e.g., ./pm:Relation/pm:Code or extension element."
      literal "PS", :documentation => "Documentation:\nPS = Pre-setting. This METRIC is a pre-setting for the containment tree entries defined in ./pm:Relation/@Entries."
      literal "Rcm", :documentation => "Documentation:\nRcm = Recommendation. The METRIC is a recommendation for the containment tree entries defined in ./pm:Relation/@Entries."
      literal "SST", :documentation => "Documentation:\nSST = Set of summary statistics. The METRIC is part of a set of summary statistics for a sample where other METRICs that belong to the summary statistics are defined in ./pm:Relation/@Entries."
    end
    enumeration "LocalizedTextWidth", :documentation => "Documentation:\nLocalizedTextWidth indicates the width of a localized text based on the number of fullwidth characters in order to allow a SERVICE CONSUMER an effective filtering and querying for translations.\n\nIn the following, a line is defined as the content of the text from either the beginning of the text or the beginning of a previous line until the next occurance of period mark, question mark, exclamation mark, or paragraph." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "l", :documentation => "Documentation:\nA line has 16 or less fullwidth characters."
      literal "m", :documentation => "Documentation:\nA line has 12 or less fullwidth characters."
      literal "s", :documentation => "Documentation:\nA line has 8 or less fullwidth characters."
      literal "xl", :documentation => "Documentation:\nA line has 20 or less fullwidth characters."
      literal "xs", :documentation => "Documentation:\nA line has 4 or less fullwidth characters."
      literal "xxl", :documentation => "Documentation:\nA line has 21 or more fullwidth characters."
    end
    enumeration "MdsOperatingMode", :documentation => "Documentation:\nMdsOperatingMode defines the interpretation constraints of the data that is provided by an MDS." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Dmo", :documentation => "Documentation:\nDmo = Demo. The POC MEDICAL DEVICE operates in a mode that is intended for demonstration purposes only. Arbitrary values are generated."
      literal "Mtn", :documentation => "Documentation:\nMTN = Maintenance. The POC MEDICAL DEVICE operates in a mode that is intended for maintenance purposes only."
      literal "Nml", :documentation => "Documentation:\nNml = Normal. The POC MEDICAL DEVICE operates in a mode that supports the fulfillment of its clinical functions."
      literal "Srv", :documentation => "Documentation:\nSrv = Service. The POC MEDICAL DEVICE operates in a mode that is intended for services purposes only."
    end
    enumeration "MeasurementValidity", :documentation => "Documentation:\nLevel of validity of a measured value." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Calib", :documentation => "Documentation:\nCalib = Calibration Ongoing. A measured value where correctness can not be guaranteed, because a calibration is currently going on."
      literal "Inv", :documentation => "Documentation:\nInv = Invalid. A measured value that is incorrect from the perspective of the measuring device."
      literal "NA", :documentation => "Documentation:\nNA = Not Available. No value can be derived, e.g., if a sensor is not placed correctly."
      literal "Oflw", :documentation => "Documentation:\nOflw = Overflow. A measured value where correctness cannot be guaranteed as it is above all defined technical ranges."
      literal "Ong", :documentation => "Documentation:\nOng = Measurement Ongoing. Indicates that a new measurement is just being taken and therefore measured value is not available."
      literal "Qst", :documentation => "Documentation:\nQst = Questionable. A measured value where correctness can not be guaranteed."
      literal "Uflw", :documentation => "Documentation:\nUflw = Underflow. A measured value where correctness cannot be guaranteed as it is below all defined technical ranges."
      literal "Vld", :documentation => "Documentation:\nVld = Valid. A measured value that is correct from the perspective of the measuring device."
      literal "Vldated", :documentation => "Documentation:\nVldated = Validated Data. A measured value where the validity has been confirmed by an external actor, e.g., an operator, other than the POC MEDICAL DEVICE."
    end
    enumeration "MetricAvailability", :documentation => "Documentation:\nAvailability of the means that derives the METRIC state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Cont", :documentation => "Documentation:\nCont = Continuous. Without break, cessation, or interruption; without intervening time."
      literal "Intr", :documentation => "Documentation:\nIntr = Intermittent. Stopping or ceasing for a time; alternately ceasing and beginning again. \n\nExample: non-invasive blood pressure measurement."
    end
    enumeration "MetricCategory", :documentation => "Documentation:\nThe METRIC category makes it possible to distinguish between different manifestations of a METRIC like measurements, settings or recommendations. \n\nExample: if the respiratory rate can be adjusted and the ventilator is smart and provides a recommendation, there are likely be at least three METRICs with a type of \"Respiratory Rate\": \n\n- 1 METRIC with MetricCategory set to Measurement. This METRIC is the actual measured value.\n- 1 METRIC with MetricCategory set to Setting. This METRIC is the adjustable value.\n- 1 METRIC with MetricCategory set to Recommendation. This METRIC is the recommended value derived from some smart algorithm." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Clc", :documentation => "Documentation:\nClc = Calculation. The METRIC has been derived by calculation only."
      literal "Msrmt", :documentation => "Documentation:\nMsrmt = Measurement. The METRIC has been derived by measurement."
      literal "Preset", :documentation => "Documentation:\nPreset = Presetting. The METRIC has a value that is adjustable by some (local or remote) control means. Once the value is adjusted, it remains a Preset until committed, at which point it becomes a setting.\n\nRelated settings MAY be defined by using pm:AbstractMetricDescriptor/pm:Relation."
      literal "Rcmm", :documentation => "Documentation:\nRcmm = Recommendation. The METRIC is a proposal for a setting or presetting. The related setting or presetting MAY be defined by using pm:AbstractMetricDescriptor/pm:Relation."
      literal "Set", :documentation => "Documentation:\nSet = Setting. The METRIC has a value that is adjustable by some (local or remote) control means."
      literal "Unspec", :documentation => "Documentation:\nUnspec = Unspecified. None of the categories in MetricCategory is valid for the METRIC."
    end
    enumeration "OperatingMode", :documentation => "Documentation:\nMode of an operation state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Dis", :documentation => "Documentation:\nDis = Disabled. Object is disabled."
      literal "En", :documentation => "Documentation:\nEn = Enabled. Object is enabled"
      literal "NA", :documentation => "Documentation:\nNA = Not Available. Object is not available for interaction. This means that it is defined but currently not in a mode so that it can be interacted with."
    end
    enumeration "PatientType", :documentation => "Documentation:\nType of a patient." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Ad", :documentation => "Documentation:\nAd = Adult. Indicates an adult patient."
      literal "Ado", :documentation => "Documentation:\nAdo = Adolescent. Indicates an adolescent patient with approximate age range of 12 years to 21 years."
      literal "Inf", :documentation => "Documentation:\nInf = Infant. Indicates an infant patient with approximate age range of 1 month to 2 years."
      literal "Neo", :documentation => "Documentation:\nNeo = Neonatal. Indicates a neonatal patient with approximate age range of birth to 1 month."
      literal "Oth", :documentation => "Documentation:\nOth = Other. The patient type is designated by some other means."
      literal "Ped", :documentation => "Documentation:\nPed = Pediatric. Indicates a pediatric patient with approximate age range of 2 years to 12 years."
      literal "Unspec", :documentation => "Documentation:\nUnspec = Unspecified. Unspecified type."
    end
    enumeration "SafetyClassification", :documentation => "Documentation:\nSafetyClassification allows POC MEDICAL DEVICE manufacturers to limit their responsibility for the provided objects that allow informational use or use in clinical functions. It reflects the quality of the respective data from the risk management perspective of the data provider.\n\nEnumeration values prefixed with \"Med\" indicate that the manufacturer has considered a clinical function related to the object in its development process, particularly the risk management, software development, usability, and verification process." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "Inf", :documentation => "Documentation:\nInf = Informational. The descriptor and the related state information are intended to be used for information purposes only. They are not intended to be used in clinical functions."
      literal "MedA", :documentation => "Documentation:\nMedA = Medical Class A. The descriptor and related state information are intended to be used in clinical functions, specifically for general display in order to support patient and device monitoring. The displayed data is not intended to be used as sole source for diagnostic or therapeutic decisions. Deviations from this intended use are in the sole responsibility of the SERVICE CONSUMER."
      literal "MedB", :documentation => "Documentation:\nMedB = Medical Class B. The descriptor and related state information are intended to be used in clinical functions. The manufacturer has specified and considered a specific intended use for the data, which could result in non-serious injury. Deviations from this intended use are in the sole responsibility of the SERVICE CONSUMER."
      literal "MedC", :documentation => "Documentation:\nMedC = Medical Class C. The descriptor and related state information are intended to be used in clinical functions. The manufacturer has specified and considered a specific intended use for the data, which could result in serious injury. Deviations from this intended use are in the sole responsibility of the SERVICE CONSUMER."
    end
    enumeration "Sex", :documentation => "Documentation:\nSex of a human. \n\n\"Sex\" refers to the biological and physiological characteristics that define men and women, while \"Gender\" refers to the socially constructed roles, behaviors, activities, and attributes that a given society considers appropriate for men and women. See http://www.who.int/gender/whatisgender/en/index.html.\n\nNOTE—ISO/IEC 5218:2004 defines four CODEs that represent human sexes." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value_Attribute", :type => "UML::String"
      literal "F", :documentation => "Documentation:\nF = Female. Indicates a female patient."
      literal "M", :documentation => "Documentation:\nM = Male. Indicates a male patient."
      literal "Unkn", :documentation => "Documentation:\nUnkn = Unknown. Indicates that the sex is unknown for different reasons."
      literal "Unspec", :documentation => "Documentation:\nUnspec = Unspecified. Sex is not designated."
    end
    interface "DateOfBirthType_1", :documentation => "Documentation:\nDate of birth of the patient.\n\nIf the timepoint of birth matters, the value SHALL be populated with a time zone." do
      property "inv_PatientDemographicsCoreData_DateOfBirth", :type => "BICEPS_ParticipantModel::PatientDemographicsCoreData"
    end
    primitive "CodeIdentifier", :parents => ["UML::String"], :documentation => "Documentation:\nCodeIdentifier defines an arbitrary CODE identifier with a minimum length of 1 character." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "ExtensionType_1", :parents => ["UML::String"], :documentation => "Documentation:\nA character string as a unique identifier within the scope of pm:InstanceIdentifier/pm:Root.\nIf a non-null Extension exists, pm:InstanceIdentifier/pm:Root specifies a namespace (\"assigning authority\" or \"identifier type\").\n\n__R5008: Extension MAY be empty if pm:InstanceIdentifier/pm:Root is the complete unique identifier.__\n\n__R5009: If pm:InstanceIdentifier/pm:Root is not a complete unique identifier and Extension is not known, then Extension SHALL be populated with a null-flavor \"Unknown\".__\n\nNOTE—How the null-flavor \"Unknown\" is encoded, depends on the use-case and type of pm:InstanceIdentifier/pm:Root URI."
    primitive "Handle", :parents => ["UML::String"], :documentation => "Documentation:\nA HANDLE is used to efficiently identify an object in the MDIB." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "HandleRef", :parents => ["UML::String"], :documentation => "Documentation:\nHandleRef describes a HANDLE reference. It is used to form logical connections to ELEMENTs that possess a pm:Handle ATTRIBUTE.\n\nExample: a METRIC state is associated with a METRIC descriptor (pm:AbstractDescriptor/@Handle) by means of an ATTRIBUTE of type pm:HandleRef (see pm:AbstractState/@DescriptorHandle)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "LocalizedTextContent", :parents => ["UML::String"], :documentation => "Documentation:\nContent restriction for pm:LocalizedText ELEMENTs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "LocalizedTextRef", :parents => ["UML::String"], :documentation => "Documentation:\nLocalizedTextRef defines a reference to a localized text." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "QualityIndicator", :parents => ["UML::Float"], :documentation => "Documentation:\nIndicates the quality of a determined value, where 0 means lowest quality and 1 means high quality w.r.t. to the validity level." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "ReferencedVersion", :parents => ["BICEPS_ParticipantModel::VersionCounter"], :documentation => "Documentation:\nIn contrast to pm:VersionCounter, ReferencedVersion does not represent a version of an MDIB object, but a reference to a particular version of an MDIB object." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "RootType_1", :parents => ["UML::Uri"], :documentation => "Documentation:\nA unique identifier that guarantees the global uniqueness of the instance identifier. Root alone is allowed to build the entire instance identifier.\n\nIf ./@Extension is present, Root is the unique identifier for the \"namespace\" of the identifier in ./@Extension.\n\nIn situations where ./@Extension is known, but Root is not known, the implied value of a non-existent Root SHALL match the value defined in R0135. \n\nNOTE—Example: a POC MEDICAL DEVICE with an attached simple bar code scanner could create a new instance identifier with an unknown root and an extension that is set to the bar code number. Root is then applied later in time."
    primitive "SymbolicCodeName", :parents => ["UML::String"], :documentation => "Documentation:\nSymbolicCodeName is a symbolic, programmatic form of a pm:CodeIdentifier term.\n\nNOTE—SymbolicCodeName is the equivalent of the Reference ID attribute that is defined in IEEE 11073-10101." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "TimeZone", :parents => ["UML::String"], :documentation => "Documentation:\nTimeZone describes the time zone and DST setting of a clock in POSIX format (ISO/IEC/IEEE 9945).\n\nExample: CST6CDT,M3.2.0/2:00:00,M11.1.0/2:00:00, which would effect a change to daylight saving time at 2:00 AM on the second Sunday in March and change back at 2:00 AM on the first Sunday in November, and keep 6 hours time offset from GMT every year." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "Timestamp", :parents => ["UML::Integer"], :documentation => "Documentation:\nAn unsigned 64-bit integer value that represents a timestamp.\n\t\t\t\n__R5001: A timestamp SHALL count the milliseconds between the current time and midnight, January 1, 1970 UTC without leap seconds.__\n\n__R5002: Timestamps are an optional feature of the MDIB. If anywhere in the MDIB a timestamp is used, the SERVICE PROVIDER SHALL provide a pm:ClockDescriptor ELEMENT.__\n\nNOTE 1—Typically all systems assume that a day has 86400 seconds.\nNOTE 2—While the unit of time of pm:Timestamp is a millisecond, the granularity of the value depends on the hardware/software system and might be larger (e.g., tens of milliseconds)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "VersionCounter", :parents => ["UML::Integer"], :documentation => "Documentation:\nA version counter to provide versionized MDIB objects. The initial value of a version counter SHALL be \"0\".\n\n__R5003: VersionCounter values SHALL never be decremented.__" do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
  end
  package "ExtensionPoint" do
    applied_stereotype :instance_of => "XML_Schema::XML_Schema" do
      applied_tag :instance_of => "XML_Schema::XML_Schema::prefix", :value => "ext"
    end
    klass "ExtensionType" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "inv_AbstractDescriptor_Extension", :type => "BICEPS_ParticipantModel::AbstractDescriptor"
      property "inv_AbstractMetricValue_Extension", :type => "BICEPS_ParticipantModel::AbstractMetricValue"
      property "inv_AbstractState_Extension", :type => "BICEPS_ParticipantModel::AbstractState"
      property "inv_AnnotationType_1_Extension", :type => "BICEPS_ParticipantModel::AnnotationType_1"
      property "inv_BaseDemographics_Extension", :type => "BICEPS_ParticipantModel::BaseDemographics"
      property "inv_CalibrationInfo_Extension", :type => "BICEPS_ParticipantModel::CalibrationInfo"
      property "inv_CauseInfo_Extension", :type => "BICEPS_ParticipantModel::CauseInfo"
      property "inv_ClinicalInfo_Extension", :type => "BICEPS_ParticipantModel::ClinicalInfo"
      property "inv_CodedValue_Extension", :type => "BICEPS_ParticipantModel::CodedValue"
      property "inv_ContainmentTreeEntry_Extension", :type => "BICEPS_ParticipantModel::ContainmentTreeEntry"
      property "inv_ContainmentTree_Extension", :type => "BICEPS_ParticipantModel::ContainmentTree"
      property "inv_ImagingProcedure_Extension", :type => "BICEPS_ParticipantModel::ImagingProcedure"
      property "inv_InstanceIdentifier_Extension", :type => "BICEPS_ParticipantModel::InstanceIdentifier"
      property "inv_LocationDetail_Extension", :type => "BICEPS_ParticipantModel::LocationDetail"
      property "inv_LocationReference_Extension", :type => "BICEPS_ParticipantModel::LocationReference"
      property "inv_MdDescription_Extension", :type => "BICEPS_ParticipantModel::MdDescription"
      property "inv_MdState_Extension", :type => "BICEPS_ParticipantModel::MdState"
      property "inv_Mdib_Extension", :type => "BICEPS_ParticipantModel::Mdib"
      property "inv_Measurement_Extension", :type => "BICEPS_ParticipantModel::Measurement"
      property "inv_MetaDataType_1_Extension", :type => "BICEPS_ParticipantModel::MetaDataType_1"
      property "inv_MetricQualityType_1_Extension", :type => "BICEPS_ParticipantModel::MetricQualityType_1"
      property "inv_OperationGroupType_1_Extension", :type => "BICEPS_ParticipantModel::OperationGroupType_1"
      property "inv_OrderDetail_Extension", :type => "BICEPS_ParticipantModel::OrderDetail"
      property "inv_PersonReference_Extension", :type => "BICEPS_ParticipantModel::PersonReference"
      property "inv_PhysicalConnectorInfo_Extension", :type => "BICEPS_ParticipantModel::PhysicalConnectorInfo"
      property "inv_Range_Extension", :type => "BICEPS_ParticipantModel::Range"
      property "inv_RelationType_1_Extension", :type => "BICEPS_ParticipantModel::RelationType_1"
      property "inv_RemedyInfo_Extension", :type => "BICEPS_ParticipantModel::RemedyInfo"
      property "inv_TranslationType_1_Extension", :type => "BICEPS_ParticipantModel::TranslationType_1"
      property "inv_UdiType_1_Extension", :type => "BICEPS_ParticipantModel::UdiType_1"
      property "inv_WorkflowDetailType_1_Extension", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1"
    end
  end
  package "XML_Schema", :id => "_18_0_2_b9202e7_1509465344604_841107_3989" do
    stereotype "Root", :metaclasses => ["Class"], :id => "_18_0_2_b9202e7_1509465344609_957574_3996"
    stereotype "XML_Schema", :metaclasses => ["Package"], :id => "_18_0_2_b9202e7_1509465344606_390280_3992" do
      tag "targetNamespace", :id => "_18_0_2_b9202e7_1509465344616_478810_4014"
      tag "prefix", :id => "_18_0_2_b9202e7_1509465344617_466981_4016"
      tag "use_collection_containers", :id => "_18_0_2_b9202e7_1509465344617_926231_4017"
      tag "always_keep_compositions", :id => "_18_0_2_b9202e7_1509465344617_609527_4018"
      tag "object_id_only_if_referenced", :id => "_18_0_2_b9202e7_1509465344617_979349_4019"
      tag "append_id_to_reference_names", :id => "_18_0_2_b9202e7_1509465344617_28095_4020"
      tag "id_attribute_name", :id => "_18_0_2_b9202e7_1509465344617_736616_4021"
      tag "enforce_case_conventions", :id => "_18_0_2_b9202e7_1509465344617_449467_4022"
      tag "append_type_to_type_names", :id => "_18_0_2_b9202e7_1509465344617_741774_4023"
      tag "uppercase_attribute_names", :id => "_18_0_2_b9202e7_1509465344617_935249_4024"
      tag "include_documentation_annotations", :id => "_18_0_2_b9202e7_1509465344618_653900_4025"
      tag "documentation_inline", :id => "_18_0_2_b9202e7_1509465344618_870725_4026"
      tag "id_suffix_for_references", :id => "_18_0_2_b9202e7_1509465344618_911225_4027"
      tag "full_version", :id => "_18_0_2_b9202e7_1509465344618_221980_4028"
      tag "schema_filename", :id => "_18_0_2_b9202e7_1509465344618_798418_4029"
      tag "major_version", :id => "_18_0_2_b9202e7_1509465344618_225627_4030"
      tag "use_idrefs", :id => "_18_0_2_b9202e7_1509465344618_389188_4031"
      tag "id_suffix_for_plural_references", :id => "_18_0_2_b9202e7_1509465344618_543920_4032"
      tag "generate_attributes_as_elements", :id => "_18_0_2_b9202e7_1509465344618_194782_4033"
    end
    stereotype "extendable", :metaclasses => ["Interface"], :id => "_18_0_2_b9202e7_1509465344610_924890_3997"
    stereotype "list", :metaclasses => ["Class"]
    stereotype "partOfGroup", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_595438_4003" do
      tag "attributeGroup", :id => "_18_0_2_b9202e7_1509465344618_194782_4034"
    end
    stereotype "referenceElement", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_918780_4000"
    stereotype "simpleContent", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_795518_3998"
    stereotype "xmlElement", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_595438_4002"
  end
end
