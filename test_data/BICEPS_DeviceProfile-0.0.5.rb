project "BICEPS_DeviceProfile_Project", :version => "0.0.5", :generated_at => "2019-04-09 12:30:25 UTC" do
  model "Application", :imports => ["BICEPS_DeviceProfile", "BICEPS_ParticipantModel", "ExtensionPoint", "Gui_Builder_Profile", "XML_Schema"], :id => "application_1" do
    applied_stereotype :instance_of => "Gui_Builder_Profile::change_tracked"
  end
  package "BICEPS_DeviceProfile" do
    klass "MDIB", :documentation => "Documentation:\nRoot object that comprises the capability description of the represented MDSs in pm:MdDescription (descriptive part) as well as the current state in pm:MdState (state part)." do
      applied_stereotype :instance_of => "Gui_Builder_Profile::root"
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "instance_id", :type => "UML::Integer" do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:MdibVersionGroup"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "mdib_version", :type => "BICEPS_ParticipantModel::VersionCounter" do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:MdibVersionGroup"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
      property "pm_md_description", :type => "BICEPS_ParticipantModel::MdDescription" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_md_state", :type => "BICEPS_ParticipantModel::MdState" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "sequence_id", :type => "UML::Uri", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::partOfGroup" do
          applied_tag :instance_of => "XML_Schema::partOfGroup::attributeGroup", :value => "pm:MdibVersionGroup"
        end
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
      end
    end
  end
  package "BICEPS_ParticipantModel" do
    klass "AbstractAlertDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstractAlertDescriptor acts as a base class for all alert descriptors that contain static alert meta information.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AbstractAlertState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nAbstractAlertState acts as a base class for all alert states that contain dynamic/volatile alert meta information.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "activation_state", :type => "BICEPS_ParticipantModel::AlertActivation", :lower => 1
    end
    klass "AbstractComplexDeviceComponentDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nAbstractComplexDeviceComponentDescriptor adds an OPTIONAL pm:AlertSystemDescriptor and pm:ScoDescriptor to pm:AbstractDeviceComponentDescriptor.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_alert_system", :type => "BICEPS_ParticipantModel::AlertSystemDescriptor" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_sco", :type => "BICEPS_ParticipantModel::ScoDescriptor" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractComplexDeviceComponentState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nAbstractComplexDeviceComponentState acts as a base class for DEVICE COMPONENT states that have alerting and SCO capabilities.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AbstractContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstract base class for objects that specify that the MDS is able to provide context information that MAY be of relevance for the state data that is present at the communication interface at a certain point of time or time period.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AbstractContextState", :parents => ["BICEPS_ParticipantModel::AbstractMultiState"], :documentation => "Documentation:\nBase type of a context state. Every context state can be identified as valid by a validator instance. Moreover, a context state's lifecycle is determined by a start and end. AbstractContextState bundles these information.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "binding_end_time", :type => "BICEPS_ParticipantModel::Timestamp"
      property "binding_mdib_version", :type => "BICEPS_ParticipantModel::ReferencedVersion"
      property "binding_start_time", :type => "BICEPS_ParticipantModel::Timestamp"
      property "context_association", :type => "BICEPS_ParticipantModel::ContextAssociation"
      property "pm_identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_validator", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "unbinding_mdib_version", :type => "BICEPS_ParticipantModel::ReferencedVersion"
    end
    klass "AbstractDescriptor", :documentation => "Documentation:\nAbstractDescriptor defines foundational meta information of any object that is included in the descriptive part of the MDIB. Any descriptor object is derived from pm:AbstractDescriptor. The AbstractDescriptor's counterpart is pm:AbstractState.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "descriptor_version", :type => "BICEPS_ParticipantModel::VersionCounter"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "handle", :type => "BICEPS_ParticipantModel::Handle", :lower => 1
      property "pm_type", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "safety_classification", :type => "BICEPS_ParticipantModel::SafetyClassification"
    end
    klass "AbstractDeviceComponentDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstractDeviceComponentDescriptor describes a basic DEVICE COMPONENT.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_production_specification", :type => "BICEPS_ParticipantModel::ProductionSpecificationType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractDeviceComponentState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nState of a component that is part of an MDS.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "activation_state", :type => "BICEPS_ParticipantModel::ComponentActivation"
      property "operating_cycles", :type => "UML::Integer"
      property "operating_hours", :type => "UML::Integer"
      property "pm_calibration_info", :type => "BICEPS_ParticipantModel::CalibrationInfo" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_next_calibration", :type => "BICEPS_ParticipantModel::CalibrationInfo" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_physical_connector", :type => "BICEPS_ParticipantModel::PhysicalConnectorInfo" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAn abstract descriptor for a METRIC.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "activation_duration", :type => "UML::String"
      property "derivation_method", :type => "BICEPS_ParticipantModel::DerivationMethod"
      property "determination_period", :type => "UML::String"
      property "life_time_period", :type => "UML::String"
      property "max_delay_time", :type => "UML::String"
      property "max_measurement_time", :type => "UML::String"
      property "metric_availability", :type => "BICEPS_ParticipantModel::MetricAvailability", :lower => 1
      property "metric_category", :type => "BICEPS_ParticipantModel::MetricCategory", :lower => 1
      property "pm_body_site", :type => "BICEPS_ParticipantModel::CodedValue", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_relation", :type => "BICEPS_ParticipantModel::RelationType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_unit", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractMetricState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nAbstract state of a METRIC.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "activation_state", :type => "BICEPS_ParticipantModel::ComponentActivation"
      property "active_determination_period", :type => "UML::String"
      property "life_time_period", :type => "UML::String"
      property "pm_body_site", :type => "BICEPS_ParticipantModel::CodedValue", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_physical_connector", :type => "BICEPS_ParticipantModel::PhysicalConnectorInfo" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractMetricValue", :documentation => "Documentation:\nAbstract value of a METRIC.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "determination_time", :type => "BICEPS_ParticipantModel::Timestamp"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_annotation", :type => "BICEPS_ParticipantModel::AnnotationType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_metric_quality", :type => "BICEPS_ParticipantModel::MetricQualityType_1", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "start_time", :type => "BICEPS_ParticipantModel::Timestamp"
      property "stop_time", :type => "BICEPS_ParticipantModel::Timestamp"
    end
    klass "AbstractMultiState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nAbstractMultiState is derived from pm:AbstractState. In contrast to pm:AbstractState, AbstractMultiState possesses a HANDLE name. The HANDLE name uniquely identifies the state, which is required if the relation to a descriptor is ambiguous.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "handle", :type => "BICEPS_ParticipantModel::Handle", :lower => 1
      property "pm_category", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDescriptor"], :documentation => "Documentation:\nAbstract description of an operation that is exposed on the SCO.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "access_level", :type => "BICEPS_ParticipantModel::AccessLevelType_1"
      property "invocation_effective_timeout", :type => "UML::String"
      property "max_time_to_finish", :type => "UML::String"
      property "operation_target", :type => "BICEPS_ParticipantModel::HandleRef", :lower => 1
      property "retriggerable", :type => "UML::Boolean"
    end
    klass "AbstractOperationState", :parents => ["BICEPS_ParticipantModel::AbstractState"], :documentation => "Documentation:\nState of an operation that is exposed on the SCO.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "operating_mode", :type => "BICEPS_ParticipantModel::OperatingMode", :lower => 1
    end
    klass "AbstractSetStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractOperationDescriptor"], :documentation => "Documentation:\nAbstract description of an operation that is exposed on the SCO and is intended to be used to set a complete state.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_modifiable_data", :type => "UML::String", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AbstractState", :documentation => "Documentation:\nAbstractState defines foundational meta information of any object that is included in the state part of the MDIB. Any state object is derived from pm:AbstractState. The pm:AbstractState's counterpart is pm:AbstractDescriptor.", :is_abstract => true do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "descriptor_handle", :type => "BICEPS_ParticipantModel::HandleRef", :lower => 1
      property "descriptor_version", :type => "BICEPS_ParticipantModel::ReferencedVersion"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "state_version", :type => "BICEPS_ParticipantModel::VersionCounter"
    end
    klass "ActivateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes an activate operation that is exposed on the SCO. Activate operations are any parameterized operations that trigger an arbitrary action. The action that is triggered SHALL be defined by the pm:AbstractDescriptor/pm:Type ELEMENT." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_argument", :type => "BICEPS_ParticipantModel::ArgumentType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ActivateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of an activate operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "AlertConditionDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractAlertDescriptor"], :documentation => "Documentation:\nAn ALERT CONDITION contains the information about a potentially or actually HAZARDOUS SITUATION. \n\nExamples: a physiological alarm limit has been exceeded or a sensor has been unplugged." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "can_deescalate", :type => "BICEPS_ParticipantModel::CanDeescalateType_1"
      property "can_escalate", :type => "BICEPS_ParticipantModel::CanEscalateType_1"
      property "default_condition_generation_delay", :type => "UML::String"
      property "kind", :type => "BICEPS_ParticipantModel::AlertConditionKind", :lower => 1
      property "pm_cause_info", :type => "BICEPS_ParticipantModel::CauseInfo", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_source", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "priority", :type => "BICEPS_ParticipantModel::AlertConditionPriority", :lower => 1
    end
    klass "AlertConditionReference", :documentation => "Documentation:\nA list of HANDLE references that point to ALERT CONDITIONs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "list_item", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY
    end
    klass "AlertConditionState", :parents => ["BICEPS_ParticipantModel::AbstractAlertState"], :documentation => "Documentation:\nAlertConditionState contains the dynamic/volatile information of an ALERT CONDITION. See pm:AlertConditionDescriptor for static information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "actual_condition_generation_delay", :type => "UML::String"
      property "actual_priority", :type => "BICEPS_ParticipantModel::AlertConditionPriority"
      property "determination_time", :type => "BICEPS_ParticipantModel::Timestamp"
      property "presence", :type => "UML::Boolean"
      property "rank", :type => "UML::Integer"
    end
    klass "AlertSignalDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractAlertDescriptor"], :documentation => "Documentation:\nAlertSignalDescriptor represents an ALERT SIGNAL. An ALERT SIGNAL contains information about the way an ALERT CONDITION is communicated to a human. It is generated by an ALERT SYSTEM to indicate the presence or occurrence of an ALERT CONDITION.\n\nExample: a signal could be a lamp (see pm:AlertSignalDescriptor/pm:Manifestation) on a remote POC MEDICAL DEVICE, such as the nurses handheld device (see pm:AlertSignalDescriptor/pm:SignalDelegationSupported), which starts flashing when the heart rate is exceeding 150bmp (see pm:AlertSignalDescriptor/pm:ConditionSignaled) for more than 2 seconds (see pm:AlertSignalDescriptor/pm:DefaultSignalGenerationDelay), and keeps flashing until the nurse confirms the alarm, even if the alarm condition is not present anymore (see pm:AlertSignalDescriptor/pm:Latching)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "acknowledge_timeout", :type => "UML::String"
      property "acknowledgement_supported", :type => "UML::Boolean"
      property "condition_signaled", :type => "BICEPS_ParticipantModel::HandleRef"
      property "default_signal_generation_delay", :type => "UML::String"
      property "latching", :type => "UML::Boolean", :lower => 1
      property "manifestation", :type => "BICEPS_ParticipantModel::AlertSignalManifestation", :lower => 1
      property "max_signal_generation_delay", :type => "UML::String"
      property "min_signal_generation_delay", :type => "UML::String"
      property "signal_delegation_supported", :type => "UML::Boolean"
    end
    klass "AlertSignalState", :parents => ["BICEPS_ParticipantModel::AbstractAlertState"], :documentation => "Documentation:\nAlertSignalState contains the dynamic/volatile information of an ALERT SIGNAL. See pm:AlertSignalDescriptor for static information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "actual_signal_generation_delay", :type => "UML::String"
      property "location", :type => "BICEPS_ParticipantModel::AlertSignalPrimaryLocation"
      property "presence", :type => "BICEPS_ParticipantModel::AlertSignalPresence"
      property "slot", :type => "UML::Integer"
    end
    klass "AlertSystemDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractAlertDescriptor"], :documentation => "Documentation:\nAlertSystemDescriptor describes an ALERT SYSTEM to detect ALERT CONDITIONs and generate ALERT SIGNALs, which belong to specific ALERT CONDITIONs.\n\nALERT CONDITIONs are represented by a list of pm:AlertConditionDescriptor ELEMENTs and ALERT SIGNALs are represented by a list of pm:AlertSignalDescriptor ELEMENTs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "max_physiological_parallel_alarms", :type => "UML::Integer"
      property "max_technical_parallel_alarms", :type => "UML::Integer"
      property "pm_alert_condition", :type => "BICEPS_ParticipantModel::AlertConditionDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_alert_signal", :type => "BICEPS_ParticipantModel::AlertSignalDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "self_check_period", :type => "UML::String"
    end
    klass "AlertSystemState", :parents => ["BICEPS_ParticipantModel::AbstractAlertState"], :documentation => "Documentation:\nAlertSystemState contains the dynamic/volatile information of an ALERT SYSTEM. See pm:AlertSystemDescriptor for static information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "last_self_check", :type => "BICEPS_ParticipantModel::Timestamp"
      property "pm_system_signal_activation", :type => "BICEPS_ParticipantModel::SystemSignalActivation", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "present_physiological_alarm_conditions", :type => "BICEPS_ParticipantModel::AlertConditionReference"
      property "present_technical_alarm_conditions", :type => "BICEPS_ParticipantModel::AlertConditionReference"
      property "self_check_count", :type => "UML::Integer"
    end
    klass "AllowedValueType_1" do
      property "pm_characteristic", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_type", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_value", :type => "UML::String", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AllowedValuesType_1" do
      property "pm_value", :type => "UML::String", :lower => 1, :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "AnnotationType_1" do
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_type", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ApplyAnnotationType_1" do
      property "annotation_index", :type => "UML::Integer", :lower => 1
      property "sample_index", :type => "UML::Integer", :lower => 1
    end
    klass "ApprovedJurisdictions", :documentation => "Documentation:\nList of regions in which a DEVICE COMPONENT is approved to be operated. If the list does not contain any entries, then the DEVICE COMPONENT is not approved for any region." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_approved_jurisdiction", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ArgumentType_1" do
      property "pm_arg", :type => "UML::String", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_arg_name", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "BaseDemographics", :documentation => "Documentation:\nDefinition of basic demographic information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_birthname", :type => "UML::String" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_familyname", :type => "UML::String" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_givenname", :type => "UML::String" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_middlename", :type => "UML::String", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_title", :type => "UML::String" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "BatteryDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nFor battery-powered devices, battery information can be contained in this object." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_capacity_full_charge", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_capacity_specified", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_voltage_specified", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "BatteryState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nState of a battery of an MDS.\n\nThe current power source is designated by ./@ActivationState:\n\n- If ./@ActivationState equals \"On\", the device is running on battery\n- If ./@ActivationState equals \"Off\", the device is in mains operation and currently not able to be run on battery\n- If ./@ActivationState equals \"StndBy\", the device is in mains operation and can be switched to run on battery\n- If ./@ActivationState equals \"Fail\", the battery has a malfunction. Detailed error information SHOULD be communicated by using an ALERT SYSTEM.\n\nEnumerations \"Shtdn\" and \"NotRdy\" are undefined for BatteryState." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "charge_cycles", :type => "UML::Integer"
      property "charge_status", :type => "BICEPS_ParticipantModel::ChargeStatusType_1"
      property "pm_capacity_remaining", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_current", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_remaining_battery_time", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_temperature", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_voltage", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "CalibrationDocumentationType_1" do
      property "pm_calibration_result", :type => "BICEPS_ParticipantModel::CalibrationResultType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_documentation", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "CalibrationInfo", :documentation => "Documentation:\nProvides information in terms of component calibration. By default, it only maintains a calibration flag." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "component_calibration_state", :type => "BICEPS_ParticipantModel::CalibrationState"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_calibration_documentation", :type => "BICEPS_ParticipantModel::CalibrationDocumentationType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "time", :type => "BICEPS_ParticipantModel::Timestamp"
      property "type", :type => "BICEPS_ParticipantModel::CalibrationType"
    end
    klass "CalibrationResultType_1" do
      property "pm_code", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_value", :type => "BICEPS_ParticipantModel::Measurement", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "CauseInfo", :documentation => "Documentation:\nCause information for an ALERT CONDITION." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_description", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_remedy_info", :type => "BICEPS_ParticipantModel::RemedyInfo" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ChannelDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nChannelDescriptor describes a CHANNEL to group METRICs and alerts. It is used for organizational purposes only.\n\nExample: an example would be a blood pressure VMD with one CHANNEL to group together all METRICs that deal with the blood pressure (e.g., pressure value, pressure waveform). A second CHANNEL object could be used to group together METRICs that deal with heart rate." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_metric", :type => "BICEPS_ParticipantModel::AbstractMetricDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ChannelState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nThe state of a CHANNEL." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "ClinicalInfo", :documentation => "Documentation:\nThis type describes a minimal clinical observation." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_code", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_criticality", :type => "BICEPS_ParticipantModel::CriticalityType_1" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_description", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_related_measurement", :type => "BICEPS_ParticipantModel::RelatedMeasurementType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_type", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ClockDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nClockDescriptor describes the capabilities of an MDS regarding date/time handling and synchronization. The presense of a ClockDescriptor does not imply any specific hardware or software support." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_time_protocol", :type => "BICEPS_ParticipantModel::CodedValue", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "resolution", :type => "UML::String"
    end
    klass "ClockState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nState of a clock of an MDS." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "accuracy", :type => "UML::Float"
      property "critical_use", :type => "UML::Boolean"
      property "date_and_time", :type => "BICEPS_ParticipantModel::Timestamp"
      property "last_set", :type => "BICEPS_ParticipantModel::Timestamp"
      property "pm_active_sync_protocol", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_reference_source", :type => "UML::String", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "remote_sync", :type => "UML::Boolean", :lower => 1
      property "time_zone", :type => "BICEPS_ParticipantModel::TimeZone"
    end
    klass "CodedValue", :documentation => "Documentation:\nIn general, in an interoperability format, objects, attributes, and methods are identified by nomenclature codes. CodedValue offers the ability to represent such nomenclature codes.\n\nTwo CodedValue objects C1 and C2 are equivalent, if\n\n- C1/@Code equals C2/@Code\n- C1/@CodingSystem equals C2/@CodingSystem, both with expanded default values\n- C1/@CodingSystemVersion equals C2/@CodingSystemVersion\n- If there exists a CodedValue object T1 in C1/pm:Translation and a CodedValue object T2 in C2/pm:Translation such that T1 and T2 are equivalent, C1 and T2 are equivalent, or C2 and T1 are equivalent.\n\nNOTE 1—In case that ./@CodingSystem is not explicitly defined in CodedValue, it is replaced implicitly by a default identifier. The ./@CodingSystem ATTRIBUTE is then called \"expanded\". \nNOTE 2—As prescribed in ./@CodingSystemVersion, a version is set only if a unique version identification by ./@CodingSystem is not possible. Hence, there can be no implicit version mismatch.\nNOTE 3—Equivalence between CodedValue objects is not necessarily transitive.\n\t\t\t" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "code", :type => "BICEPS_ParticipantModel::CodeIdentifier", :lower => 1
      property "coding_system", :type => "UML::Uri"
      property "coding_system_version", :type => "UML::String"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_coding_system_name", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_concept_description", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_translation", :type => "BICEPS_ParticipantModel::TranslationType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "symbolic_code_name", :type => "BICEPS_ParticipantModel::SymbolicCodeName"
    end
    klass "DateDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "datatype", :type => "UML::Date"
    end
    klass "DateTimeDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "datatype", :type => "UML::Datetime"
    end
    klass "DistributionSampleArrayMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nDeclares a sample array that represents linear value distributions in the form of arrays containing scaled sample values. In contrast to real-time sample arrays, distribution sample arrays provide observed spatial values, not time points.\n\nNOTE—An example for a distribution sample array metric might be a fourier-transformed electroencephalogram to derive frequency distribution." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_distribution_range", :type => "BICEPS_ParticipantModel::Range", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_domain_unit", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_technical_range", :type => "BICEPS_ParticipantModel::Range", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "resolution", :type => "UML::Float", :lower => 1
    end
    klass "DistributionSampleArrayMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a distribution sample array METRIC descriptor. It contains a list of sample values. This sample array is used to transport spatial range information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_metric_value", :type => "BICEPS_ParticipantModel::SampleArrayValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_physiological_range", :type => "BICEPS_ParticipantModel::Range", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "EnsembleContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide ensemble information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "EnsembleContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state that identifies an ensemble of POC MEDICAL DEVICEs. How the ensemble is grouped and what meaning is conveyed by the ensemble, is determined by other means." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "EntryRef", :documentation => "Documentation:\nA list of CONTAINMENT TREE ENTRY handle references." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "list_item", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY
    end
    klass "EnumStringMetricDescriptor", :parents => ["BICEPS_ParticipantModel::StringMetricDescriptor"], :documentation => "Documentation:\nAn enumerated string METRIC represents a textual status or annotation information with a constrained set of possible values.\n\nExample: a ventilation mode." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_allowed_value", :type => "BICEPS_ParticipantModel::AllowedValueType_1", :lower => 1, :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "EnumStringMetricState", :parents => ["BICEPS_ParticipantModel::StringMetricState"], :documentation => "Documentation:\nState of an enumerated string METRIC." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "GYearDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "datatype", :type => "UML::String"
    end
    klass "GYearMonthDatatype", :implements => ["BICEPS_ParticipantModel::DateOfBirthType_1"] do
      property "datatype", :type => "UML::String"
    end
    klass "ImagingProcedure", :documentation => "Documentation:\nImagingProcedure provides identifiers used by the DICOM and HL7 standard to identify the requested imaging procedures resulting from an order in a the hospital. Often these identifiers are created/assigned by the main hospital information system or departmental information systems and are taken over into any medical images by DICOM equipment in the context of this procedure.\nThe listed ELEMENTs have been taken over from the IHE Radiology Technical Framework's RAD-4 transaction (\"Procedure Scheduled\") and re-uses the identifiers listed for the HL7 Version 2.5.1 IPC segment group of the OBR segment. Therefore, it is recommended to comply to the underlying HL7 and DICOM data types in order to have seamless integration with other clinical IT such as DICOM modalities or image archives (PACS).\n\nIn order to comply to the hierarchy behind the given identifiers, the following rules (taken from IHE) SHALL apply: if a Requested Procedure is comprised of multiple Scheduled Procedure Steps and/or if a Scheduled Procedure Step is comprised of multiple Protocol Codes, each applicable Scheduled Procedure Step / Protocol Code combination is included as a separate ProcedureDetails structure, i.e., the complex type \"ProcedureDetails\" occurs the same amount of times as there are different Scheduled Procedure Step IDs plus the amount of different Scheduled Procedure Step / Protocol Code combinations." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_accession_identifier", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_modality", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_protocol_code", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_requested_procedure_id", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_scheduled_procedure_step_id", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_study_instance_uid", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "InstanceIdentifier", :documentation => "Documentation:\nAn identifier that uniquely identifies a thing or object.\n\nExamples: object identifiers for medical record numbers, order ids, location ids, etc. InstanceIdentifier is defined in accordance to [InstanceIdentifier].\n\n./@Root and ./@Extension of an instance identifier do not identify the type of the object being identified, or the type of the association between the object and the identifier - they only form the identifier itself. The identifier type SHALL be expressed by ./Type." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "extension", :type => "BICEPS_ParticipantModel::ExtensionType_1"
      property "pm_identifier_name", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_type", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "root", :type => "BICEPS_ParticipantModel::RootType_1"
    end
    klass "LimitAlertConditionDescriptor", :parents => ["BICEPS_ParticipantModel::AlertConditionDescriptor"], :documentation => "Documentation:\nLimitAlertConditionDescriptor is a specialization of an ALERT CONDITION that is active if at least one limit for a referenced METRIC has been violated." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "auto_limit_supported", :type => "UML::Boolean"
      property "pm_max_limits", :type => "BICEPS_ParticipantModel::Range", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "LimitAlertConditionState", :parents => ["BICEPS_ParticipantModel::AlertConditionState"], :documentation => "Documentation:\nA state of a limit ALERT CONDITION." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "auto_limit_activation_state", :type => "BICEPS_ParticipantModel::AlertActivation"
      property "monitored_alert_limits", :type => "BICEPS_ParticipantModel::AlertConditionMonitoredLimits", :lower => 1
      property "pm_limits", :type => "BICEPS_ParticipantModel::Range", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "LocalizedText", :documentation => "Documentation:\nLocalizedText is a bundled ELEMENT to reference texts in different languages or to provide a text in a specific language.\n\nThe goal of text references is to shrink the overall size of the MDIB by only providing a single reference to a text file that translates a text into multiple languages instead of flooding the MDIB with all translated texts. Referenced texts can be requested by the LOCALIZATION SERVICE. If no LOCALIZATION SERVICE exist, the application can make use of LocalizedText to represent a text in a single language.\n\n__R5047: If ./@Lang and ./@Ref are present, then the text SHALL be only available in the language specified by ./@Lang.__\n\n__R5048: If ./@Lang is present and ./@Ref is not present, then ./@Lang SHALL specify the language of the LocalizedText's content. The Text is not available through the LOCALIZATION SERVICE.__\n\n__R5049: If ./@Lang is not present and ./@Ref is present, then the text SHALL be available through the LOCALIZATION SERVICE.__\n\n__R5050: If ./@Lang and ./@Ref are not present, then the language of the LocalizedText's content is unknown. The text SHALL NOT be available through the LOCALIZATION SERVICE.__" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "lang", :type => "UML::String"
      property "ref", :type => "BICEPS_ParticipantModel::LocalizedTextRef"
      property "text_width", :type => "BICEPS_ParticipantModel::LocalizedTextWidth"
      property "value", :type => "BICEPS_ParticipantModel::LocalizedTextContent", :lower => 1
      property "version", :type => "BICEPS_ParticipantModel::ReferencedVersion"
    end
    klass "LocationContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide information regarding the current spatial position." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "LocationContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state that identifies a location in a hospital." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_location_detail", :type => "BICEPS_ParticipantModel::LocationDetail" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "LocationDetail", :documentation => "Documentation:\nDetails about a location. This information is derived from the HL7 PV1-3 PL." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "bed", :type => "UML::String"
      property "building", :type => "UML::String"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "facility", :type => "UML::String"
      property "floor", :type => "UML::String"
      property "po_c", :type => "UML::String"
      property "room", :type => "UML::String"
    end
    klass "LocationReference", :documentation => "Documentation:\nA reference to an identifiable location with human readable location details." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1, :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_location_detail", :type => "BICEPS_ParticipantModel::LocationDetail" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "MdDescription", :documentation => "Documentation:\nMdDescription is the root container to represent the descriptive part of the MDIB. The descriptive part describes the capabilities provided by a POC MEDICAL DEVICE, e.g., which measurements, alerts and settings it provides. As the descriptive part does not change as frequently as the state part, it is well-known as the (almost) static part of the MDIB. The MdDescription's counterpart is pm:MdState." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "description_version", :type => "BICEPS_ParticipantModel::VersionCounter"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_mds", :type => "BICEPS_ParticipantModel::MdsDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "MdState", :documentation => "Documentation:\nMdState is the root container to represent the state part of the MDIB. The state part describes the values provided by a POC MEDICAL DEVICE, e.g., which measurement or alert values as well as patient demographics it provides. As the state part most often changes very frequently, it is well-known as the dynamic part of the MDIB. The MdState's counterpart is pm:MdDescription." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_state", :type => "BICEPS_ParticipantModel::AbstractState", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "state_version", :type => "BICEPS_ParticipantModel::VersionCounter"
    end
    klass "MdsDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor"], :documentation => "Documentation:\nMdsDescriptor represents an MDS that in turn represents a POC MEDICAL DEVICE such as an anesthesia workstation. It contains an abstraction of the hardware specification of a POC MEDICAL DEVICE plus a list of VMDs, contextual information and clock object.\n\nNOTE—The IEEE 11073-10201 has different specializations that are all representable by MdsDescriptor." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_approved_jurisdictions", :type => "BICEPS_ParticipantModel::ApprovedJurisdictions" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_battery", :type => "BICEPS_ParticipantModel::BatteryDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_clock", :type => "BICEPS_ParticipantModel::ClockDescriptor" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_meta_data", :type => "BICEPS_ParticipantModel::MetaDataType_1" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_system_context", :type => "BICEPS_ParticipantModel::SystemContextDescriptor" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_vmd", :type => "BICEPS_ParticipantModel::VmdDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "MdsState", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentState"], :documentation => "Documentation:\nDefinition of the state of an pm:MdsDescriptor." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "lang", :type => "UML::String"
      property "operating_mode", :type => "BICEPS_ParticipantModel::MdsOperatingMode"
      property "pm_operating_jurisdiction", :type => "BICEPS_ParticipantModel::OperatingJurisdiction" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "MeansContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide information about utilized means." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "MeansContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state to identify a means that is utilized by an MDS or a part of it." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "Measurement", :documentation => "Documentation:\nMeasurement describes a measurement and is used only for stateful object attributes that do not have a reference to a descriptor object.\n\nExample: Weight of a patient." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "measured_value", :type => "UML::Float", :lower => 1
      property "pm_measurement_unit", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "MetaDataType_1" do
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_expiration_date", :type => "UML::Datetime" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_lot_number", :type => "UML::String" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_manufacture_date", :type => "UML::Datetime" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_manufacturer", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_model_name", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_model_number", :type => "UML::String" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_serial_number", :type => "UML::String", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_udi", :type => "BICEPS_ParticipantModel::UdiType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "MetricQualityType_1" do
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "mode", :type => "BICEPS_ParticipantModel::GenerationMode"
      property "qi", :type => "BICEPS_ParticipantModel::QualityIndicator"
      property "validity", :type => "BICEPS_ParticipantModel::MeasurementValidity", :lower => 1
    end
    klass "NeonatalPatientDemographicsCoreData", :parents => ["BICEPS_ParticipantModel::PatientDemographicsCoreData"], :documentation => "Documentation:\nNeonatalPatientDemographicsCoreData constitutes patient demographics for neonates." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_birth_length", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_birth_weight", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_gestational_age", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_head_circumference", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_mother", :type => "BICEPS_ParticipantModel::PersonReference" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "NumericMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nSpecification of a METRIC descriptor type that represents a single numerical measurement and status information. Example: a heart rate measurement." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "averaging_period", :type => "UML::String"
      property "pm_technical_range", :type => "BICEPS_ParticipantModel::Range", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "resolution", :type => "UML::Float", :lower => 1
    end
    klass "NumericMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a numeric METRIC." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "active_averaging_period", :type => "UML::String"
      property "pm_metric_value", :type => "BICEPS_ParticipantModel::NumericMetricValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_physiological_range", :type => "BICEPS_ParticipantModel::Range", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "NumericMetricValue", :parents => ["BICEPS_ParticipantModel::AbstractMetricValue"], :documentation => "Documentation:\nNumeric value of a METRIC state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::Float"
    end
    klass "OperatingJurisdiction", :parents => ["BICEPS_ParticipantModel::InstanceIdentifier"], :documentation => "Documentation:\nThe current region information that is configured for a component. The preferred root SHOULD be https://unstats.un.org/unsd/methodology/m49, which addresses the \"Standard country or area codes for statistical use (M49)\". Example: a root of \"https://unstats.un.org/unsd/methodology/m49\" with an extension value of \"276\" addresses Germany." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "OperationGroupType_1" do
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "operating_mode", :type => "BICEPS_ParticipantModel::OperatingMode"
      property "operations", :type => "BICEPS_ParticipantModel::OperationRef"
      property "pm_type", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "OperationRef", :documentation => "Documentation:\nA list of operation handle references." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "list_item", :type => "BICEPS_ParticipantModel::HandleRef", :upper => Float::INFINITY
    end
    klass "OperatorContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide operator information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "OperatorContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state that identifies an operator of an MDS or a part of it." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_operator_details", :type => "BICEPS_ParticipantModel::BaseDemographics" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "OrderDetail", :documentation => "Documentation:\nDetails of an order that will be performed or that has been performed." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_end", :type => "UML::Datetime" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_imaging_procedure", :type => "BICEPS_ParticipantModel::ImagingProcedure", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_performer", :type => "BICEPS_ParticipantModel::PersonParticipation", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_service", :type => "BICEPS_ParticipantModel::CodedValue", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_start", :type => "UML::Datetime" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PatientContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS possesses a patient-device association." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "PatientContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nObserved information about a patient, e.g., demographics.\n\nNOTE—PatientContextState contains information that is typical for a header in an anamnesis questionnaire." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_core_data", :type => "BICEPS_ParticipantModel::PatientDemographicsCoreData" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PatientDemographicsCoreData", :parents => ["BICEPS_ParticipantModel::BaseDemographics"], :documentation => "Documentation:\nThe patient demographics data as defined in ISO/IEEE 11073-10201:2004 (6.10.1 Patient Demographics object).\n\t\t\t\n__R5012: If the POC MEDICAL DEVICE itself has patient-related observations (e.g., weight, height, etc.) as in- or output, these SHOULD be modelled as METRICs.__\n\nNOTE—In contrast to PatientDemographicsCoreData, METRICs provide a sophisticated observation description, e.g., regarding quality and time-related attributes.\n\n__R5013: The pm:PatientDemographicsCoreData type is intended to be used for information purposes only. Whenever a value is available, it is considered as valid. Invalid values SHALL not be transmitted.__" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_date_of_birth", :type => "BICEPS_ParticipantModel::DateOfBirthType_1" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_height", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_patient_type", :type => "BICEPS_ParticipantModel::PatientType" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_race", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_sex", :type => "BICEPS_ParticipantModel::Sex" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_weight", :type => "BICEPS_ParticipantModel::Measurement" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PerformedOrderDetailType_1", :parents => ["BICEPS_ParticipantModel::OrderDetail"] do
      property "pm_filler_order_number", :type => "BICEPS_ParticipantModel::InstanceIdentifier" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_resulting_clinical_info", :type => "BICEPS_ParticipantModel::ClinicalInfo", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PersonParticipation", :parents => ["BICEPS_ParticipantModel::PersonReference"], :documentation => "Documentation:\nA reference to an identifiable person with a name that participates in a role." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_role", :type => "BICEPS_ParticipantModel::CodedValue", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PersonReference", :documentation => "Documentation:\nA reference to an identifiable person with a name." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1, :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_name", :type => "BICEPS_ParticipantModel::BaseDemographics" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "PhysicalConnectorInfo", :documentation => "Documentation:\nPhysicalConnectorInfo defines a number in order to allow to guide the clinical user for a failure, e.g., in case of a disconnection of a sensor or an ultrasonic handpiece." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "number", :type => "UML::Integer"
      property "pm_label", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ProductionSpecificationType_1" do
      property "pm_component_id", :type => "BICEPS_ParticipantModel::InstanceIdentifier" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_production_spec", :type => "UML::String", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_spec_type", :type => "BICEPS_ParticipantModel::CodedValue", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "Range", :documentation => "Documentation:\nA range of decimal values which provides a lower and an upper bound as well as a step width." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "absolute_accuracy", :type => "UML::Float"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "lower", :type => "UML::Float"
      property "relative_accuracy", :type => "UML::Float"
      property "step_width", :type => "UML::Float"
      property "upper", :type => "UML::Float"
    end
    klass "RealTimeSampleArrayMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nDeclares a sample array that represents a real-time continuous waveform. An example would be an electrocardiogram (ECG) real-time wave." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_technical_range", :type => "BICEPS_ParticipantModel::Range", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "resolution", :type => "UML::Float", :lower => 1
      property "sample_period", :type => "UML::String", :lower => 1
    end
    klass "RealTimeSampleArrayMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a stream METRIC descriptor. It contains a list of sample values. This sample array is used to transport waveform stream information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_metric_value", :type => "BICEPS_ParticipantModel::SampleArrayValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_physiological_range", :type => "BICEPS_ParticipantModel::Range", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "RealTimeValueType", :documentation => "Documentation:\nDefines the real-time sample array value type comprising a whitespace separated list of decimal numbers." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "list_item", :type => "UML::Float", :upper => Float::INFINITY
    end
    klass "ReferenceRangeType_1" do
      property "pm_meaning", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_range", :type => "BICEPS_ParticipantModel::Range", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "RelatedMeasurementType_1" do
      property "pm_reference_range", :type => "BICEPS_ParticipantModel::ReferenceRangeType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_value", :type => "BICEPS_ParticipantModel::Measurement", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "validity", :type => "BICEPS_ParticipantModel::MeasurementValidity"
    end
    klass "RelationType_1" do
      property "entries", :type => "BICEPS_ParticipantModel::EntryRef", :lower => 1
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "kind", :type => "BICEPS_ParticipantModel::KindType_1", :lower => 1
      property "pm_code", :type => "BICEPS_ParticipantModel::CodedValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_identification", :type => "BICEPS_ParticipantModel::InstanceIdentifier" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "RemedyInfo", :documentation => "Documentation:\nRemedy information for a cause of an ALERT CONDITION." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_description", :type => "BICEPS_ParticipantModel::LocalizedText", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "RequestedOrderDetailType_1", :parents => ["BICEPS_ParticipantModel::OrderDetail"] do
      property "pm_placer_order_number", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_referring_physician", :type => "BICEPS_ParticipantModel::PersonReference" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_requesting_physician", :type => "BICEPS_ParticipantModel::PersonReference" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "SampleArrayValue", :parents => ["BICEPS_ParticipantModel::AbstractMetricValue"], :documentation => "Documentation:\nType that contains sequences of values, i.e., sample arrays.\n\nThe ./pmMetricQuality ELEMENT relates to all samples.\n\nNOTE 1—pm:Timestamp (see base: pm:AbstractMetricValue) refers to the first value of the array. The individual timestamps of the values can thus be computed from the sample rate (see pm:RealTimeSampleArrayMetricDescriptor).\nNOTE 2—If ./pmMetricQuality cannot be applied to all samples due to, e.g., some invalid values, a SERVICE PROVIDER can decide to set ./pmMetricQuality/@Validity to \"Qst\" or \"Inv\"." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_apply_annotation", :type => "BICEPS_ParticipantModel::ApplyAnnotationType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "samples", :type => "BICEPS_ParticipantModel::RealTimeValueType"
    end
    klass "ScoDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nScoDescriptor describes the capabilities of the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_operation", :type => "BICEPS_ParticipantModel::AbstractOperationDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "ScoState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nCorresponding state of pm:ScoDescriptor." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "invocation_requested", :type => "BICEPS_ParticipantModel::OperationRef"
      property "invocation_required", :type => "BICEPS_ParticipantModel::OperationRef"
      property "pm_operation_group", :type => "BICEPS_ParticipantModel::OperationGroupType_1", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "SetAlertStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes an alert state set operation for a specific alert state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetAlertStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of an alert state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetComponentStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes a component state set operation for a specific component state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetComponentStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a component state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetContextStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes a context state set operation for a specific context state in the MDIB that is exposed on SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetContextStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a context state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetMetricStateOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractSetStateOperationDescriptor"], :documentation => "Documentation:\nDescribes a METRIC state set operation for a specific METRIC state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetMetricStateOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a METRIC state set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetStringOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractOperationDescriptor"], :documentation => "Documentation:\nDescribes a string set operation for a specific object state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "max_length", :type => "UML::Integer"
    end
    klass "SetStringOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a string set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_allowed_values", :type => "BICEPS_ParticipantModel::AllowedValuesType_1" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "SetValueOperationDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractOperationDescriptor"], :documentation => "Documentation:\nDescribes a numeric set operation for a specific object state in the MDIB that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SetValueOperationState", :parents => ["BICEPS_ParticipantModel::AbstractOperationState"], :documentation => "Documentation:\nState of a numeric set operation that is exposed on the SCO." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_allowed_range", :type => "BICEPS_ParticipantModel::Range", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "StringMetricDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractMetricDescriptor"], :documentation => "Documentation:\nA string METRIC represents a textual status or annotation information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "StringMetricState", :parents => ["BICEPS_ParticipantModel::AbstractMetricState"], :documentation => "Documentation:\nState of a string METRIC." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_metric_value", :type => "BICEPS_ParticipantModel::StringMetricValue" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "StringMetricValue", :parents => ["BICEPS_ParticipantModel::AbstractMetricValue"], :documentation => "Documentation:\nString value of a METRIC state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
    end
    klass "SystemContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentDescriptor"], :documentation => "Documentation:\nThe context of an MDS that lists the possible relationship of a POC MEDICAL DEVICE into its usage environment by means of context descriptors. Context descriptors do not contain any stateful information. They only assert that the underlying MDS can provide corresponding context state information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_ensemble_context", :type => "BICEPS_ParticipantModel::EnsembleContextDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_location_context", :type => "BICEPS_ParticipantModel::LocationContextDescriptor" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_means_context", :type => "BICEPS_ParticipantModel::MeansContextDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_operator_context", :type => "BICEPS_ParticipantModel::OperatorContextDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_patient_context", :type => "BICEPS_ParticipantModel::PatientContextDescriptor" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_workflow_context", :type => "BICEPS_ParticipantModel::WorkflowContextDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "SystemContextState", :parents => ["BICEPS_ParticipantModel::AbstractDeviceComponentState"], :documentation => "Documentation:\nCorresponding state of pm:SystemContextDescriptor. This state comes with no additional attributes." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "SystemSignalActivation", :documentation => "Documentation:\nDefines a tuple consisting of an pm:AlertSignalManifestation and an pm:AlertActivation to describe the alert activation state of a certain ALERT SIGNAL manifestation.\n\nExample: ./@Manifestation is \"Aud\" and ./@State is \"Psd\" means that any audible alert activation is paused.\n" do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "manifestation", :type => "BICEPS_ParticipantModel::AlertSignalManifestation", :lower => 1
      property "state", :type => "BICEPS_ParticipantModel::AlertActivation", :lower => 1
    end
    klass "TranslationType_1" do
      property "code", :type => "BICEPS_ParticipantModel::CodeIdentifier", :lower => 1
      property "coding_system", :type => "UML::Uri"
      property "coding_system_version", :type => "UML::String"
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "UdiType_1" do
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_device_identifier", :type => "UML::String", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_human_readable_form", :type => "UML::String", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_issuer", :type => "BICEPS_ParticipantModel::InstanceIdentifier", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_jurisdiction", :type => "BICEPS_ParticipantModel::InstanceIdentifier" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "VmdDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentDescriptor"], :documentation => "Documentation:\nVmdDescriptor describes a VMD. A VMD is an abstraction for a module (medical-related subsystem) of an MDS. According to IEEE 11073-10201, an MDS with one VMD is a single purpose POC MEDICAL DEVICE in contrast to an MDS with multiple VMDs that has multiple purposes.\n\nExample of a multiple purpose POC MEDICAL DEVICE: an anesthesia workstation (one MDS) with a ventilation unit (one VMD), a patient monitoring unit (another VMD), and gas delivery/monitor system (another VMD). In the IEEE 11073-10201 a VMD might not be a hardware module, it also can be pure software." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_approved_jurisdictions", :type => "BICEPS_ParticipantModel::ApprovedJurisdictions" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_channel", :type => "BICEPS_ParticipantModel::ChannelDescriptor", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "VmdState", :parents => ["BICEPS_ParticipantModel::AbstractComplexDeviceComponentState"], :documentation => "Documentation:\nThe state of a VMD." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_operating_jurisdiction", :type => "BICEPS_ParticipantModel::OperatingJurisdiction" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "WorkflowContextDescriptor", :parents => ["BICEPS_ParticipantModel::AbstractContextDescriptor"], :documentation => "Documentation:\nContext descriptor to specify that the MDS is able to provide workflow information." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    klass "WorkflowContextState", :parents => ["BICEPS_ParticipantModel::AbstractContextState"], :documentation => "Documentation:\nA context state to identify a step in a clinical workflow." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "pm_workflow_detail", :type => "BICEPS_ParticipantModel::WorkflowDetailType_1" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    klass "WorkflowDetailType_1" do
      property "ext_extension", :type => "ExtensionPoint::ExtensionType" do
        applied_stereotype :instance_of => "XML_Schema::referenceElement"
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_assigned_location", :type => "BICEPS_ParticipantModel::LocationReference" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_danger_code", :type => "BICEPS_ParticipantModel::CodedValue", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_patient", :type => "BICEPS_ParticipantModel::PersonReference", :lower => 1 do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_performed_order_detail", :type => "BICEPS_ParticipantModel::PerformedOrderDetailType_1" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_relevant_clinical_info", :type => "BICEPS_ParticipantModel::ClinicalInfo", :upper => Float::INFINITY do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_requested_order_detail", :type => "BICEPS_ParticipantModel::RequestedOrderDetailType_1" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
      property "pm_visit_number", :type => "BICEPS_ParticipantModel::InstanceIdentifier" do
        applied_stereotype :instance_of => "XML_Schema::xmlElement"
      end
    end
    enumeration "AccessLevelType_1" do
      property "value", :type => "UML::String"
      literal "CSUsr"
      literal "Oth"
      literal "RO"
      literal "SP"
      literal "Usr"
    end
    enumeration "AlertActivation", :documentation => "Documentation:\nThe activation state of any ALERT SYSTEM ELEMENT, i.e., pm:AlertSystemState, pm:AlertConditionState, pm:LimitAlertConditionState, and pm:AlertSignalState.\n\nSpecial meanings MAY apply depending on the ALERT SYSTEM ELEMENT." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Off"
      literal "On"
      literal "Psd"
    end
    enumeration "AlertConditionKind", :documentation => "Documentation:\nAlertConditionKind categorizes ALERT CONDITIONs by their origin." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Oth"
      literal "Phy"
      literal "Tec"
    end
    enumeration "AlertConditionMonitoredLimits", :documentation => "Documentation:\nIndicates which limits of a pm:LimitAlertCondition ELEMENT are monitored to trigger ALERT SIGNALs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "All"
      literal "HiOff"
      literal "LoOff"
      literal "None"
    end
    enumeration "AlertConditionPriority", :documentation => "Documentation:\nAlertConditionPriority categorizes ALERT CONDITIONs into priorities.\n\nAlertConditionPriority can be used to distinguish the severity of the potential or actual hazard that exists if an ALERT CONDITION is present.\n\nNOTE—The priority is assigned through risk analysis." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Hi"
      literal "Lo"
      literal "Me"
      literal "None"
    end
    enumeration "AlertSignalManifestation", :documentation => "Documentation:\nAlertSignalManifestation categorizes ALERT SIGNALs by the way they can be recognized by the alerted human, e.g., the nurse." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Aud"
      literal "Oth"
      literal "Tan"
      literal "Vis"
    end
    enumeration "AlertSignalPresence", :documentation => "Documentation:\nGeneration state of an ALERT SIGNAL." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Ack"
      literal "Latch"
      literal "Off"
      literal "On"
    end
    enumeration "AlertSignalPrimaryLocation", :documentation => "Documentation:\nAlertSignalPrimaryLocation defines where the primary ALERT SIGNAL is generated." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Loc"
      literal "Rem"
    end
    enumeration "CalibrationState", :documentation => "Documentation:\nCalibration state of a component." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Cal"
      literal "No"
      literal "Oth"
      literal "Req"
      literal "Run"
    end
    enumeration "CalibrationType", :documentation => "Documentation:\nType of a calibration method." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Gain"
      literal "Offset"
      literal "TP"
      literal "Unspec"
    end
    enumeration "CanDeescalateType_1" do
      property "value", :type => "UML::String"
      literal "Lo"
      literal "Me"
      literal "None"
    end
    enumeration "CanEscalateType_1" do
      property "value", :type => "UML::String"
      literal "Hi"
      literal "Lo"
      literal "Me"
    end
    enumeration "ChargeStatusType_1" do
      property "value", :type => "UML::String"
      literal "ChB"
      literal "DEB"
      literal "DisChB"
      literal "Ful"
    end
    enumeration "ComponentActivation", :documentation => "Documentation:\nActivation state of a component, i.e., any type that is derived from pm:AbstractComponentState and pm:AbstractMetricState." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Fail"
      literal "NotRdy"
      literal "Off"
      literal "On"
      literal "Shtdn"
      literal "StndBy"
    end
    enumeration "ContextAssociation", :documentation => "Documentation:\nDefines an association between an arbitrary context and an MDS." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Assoc"
      literal "Dis"
      literal "No"
      literal "Pre"
    end
    enumeration "CriticalityType_1" do
      property "value", :type => "UML::String"
      literal "Hi"
      literal "Lo"
    end
    enumeration "DerivationMethod", :documentation => "Documentation:\nIn some circumstances, e.g., in spot-check situations or when dealing with settings, METRIC values might be entered manually. DerivationMethod provides an enumeration to designate if a METRIC is set automatically or manually." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Auto"
      literal "Man"
    end
    enumeration "GenerationMode", :documentation => "Documentation:\nDescribes whether METRIC data is generated by real measurements or under unreal settings (demo or test data)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Demo"
      literal "Real"
      literal "Test"
    end
    enumeration "KindType_1" do
      property "value", :type => "UML::String"
      literal "DCE"
      literal "ECE"
      literal "Oth"
      literal "PS"
      literal "Rcm"
      literal "SST"
    end
    enumeration "LocalizedTextWidth", :documentation => "Documentation:\nLocalizedTextWidth indicates the width of a localized text based on the number of fullwidth characters in order to allow a SERVICE CONSUMER an effective filtering and querying for translations.\n\nIn the following, a line is defined as the content of the text from either the beginning of the text or the beginning of a previous line until the next occurance of period mark, question mark, exclamation mark, or paragraph." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "l"
      literal "m"
      literal "s"
      literal "xl"
      literal "xs"
      literal "xxl"
    end
    enumeration "MdsOperatingMode", :documentation => "Documentation:\nMdsOperatingMode defines the interpretation constraints of the data that is provided by an MDS." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Dmo"
      literal "Mtn"
      literal "Nml"
      literal "Srv"
    end
    enumeration "MeasurementValidity", :documentation => "Documentation:\nLevel of validity of a measured value." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Calib"
      literal "Inv"
      literal "NA"
      literal "Oflw"
      literal "Ong"
      literal "Qst"
      literal "Uflw"
      literal "Vld"
      literal "Vldated"
    end
    enumeration "MetricAvailability", :documentation => "Documentation:\nAvailability of the means that derives the METRIC state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Cont"
      literal "Intr"
    end
    enumeration "MetricCategory", :documentation => "Documentation:\nThe METRIC category makes it possible to distinguish between different manifestations of a METRIC like measurements, settings or recommendations. \n\nExample: if the respiratory rate can be adjusted and the ventilator is smart and provides a recommendation, there are likely be at least three METRICs with a type of \"Respiratory Rate\": \n\n- 1 METRIC with MetricCategory set to Measurement. This METRIC is the actual measured value.\n- 1 METRIC with MetricCategory set to Setting. This METRIC is the adjustable value.\n- 1 METRIC with MetricCategory set to Recommendation. This METRIC is the recommended value derived from some smart algorithm." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Clc"
      literal "Msrmt"
      literal "Preset"
      literal "Rcmm"
      literal "Set"
      literal "Unspec"
    end
    enumeration "OperatingMode", :documentation => "Documentation:\nMode of an operation state." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Dis"
      literal "En"
      literal "NA"
    end
    enumeration "PatientType", :documentation => "Documentation:\nType of a patient." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Ad"
      literal "Ado"
      literal "Inf"
      literal "Neo"
      literal "Oth"
      literal "Ped"
      literal "Unspec"
    end
    enumeration "SafetyClassification", :documentation => "Documentation:\nSafetyClassification allows POC MEDICAL DEVICE manufacturers to limit their responsibility for the provided objects that allow informational use or use in clinical functions. It reflects the quality of the respective data from the risk management perspective of the data provider.\n\nEnumeration values prefixed with \"Med\" indicate that the manufacturer has considered a clinical function related to the object in its development process, particularly the risk management, software development, usability, and verification process." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "Inf"
      literal "MedA"
      literal "MedB"
      literal "MedC"
    end
    enumeration "Sex", :documentation => "Documentation:\nSex of a human. \n\n\"Sex\" refers to the biological and physiological characteristics that define men and women, while \"Gender\" refers to the socially constructed roles, behaviors, activities, and attributes that a given society considers appropriate for men and women. See http://www.who.int/gender/whatisgender/en/index.html.\n\nNOTE—ISO/IEC 5218:2004 defines four CODEs that represent human sexes." do
      applied_stereotype :instance_of => "XML_Schema::Root"
      property "value", :type => "UML::String"
      literal "F"
      literal "M"
      literal "Unkn"
      literal "Unspec"
    end
    interface "DateOfBirthType_1"
    primitive "CodeIdentifier", :parents => ["UML::String"], :documentation => "Documentation:\nCodeIdentifier defines an arbitrary CODE identifier with a minimum length of 1 character." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "ExtensionType_1", :parents => ["UML::String"]
    primitive "Handle", :parents => ["UML::String"], :documentation => "Documentation:\nA HANDLE is used to efficiently identify an object in the MDIB." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "HandleRef", :parents => ["UML::String"], :documentation => "Documentation:\nHandleRef describes a HANDLE reference. It is used to form logical connections to ELEMENTs that possess a pm:Handle ATTRIBUTE.\n\nExample: a METRIC state is associated with a METRIC descriptor (pm:AbstractDescriptor/@Handle) by means of an ATTRIBUTE of type pm:HandleRef (see pm:AbstractState/@DescriptorHandle)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "LocalizedTextContent", :parents => ["UML::String"], :documentation => "Documentation:\nContent restriction for pm:LocalizedText ELEMENTs." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "LocalizedTextRef", :parents => ["UML::String"], :documentation => "Documentation:\nLocalizedTextRef defines a reference to a localized text." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "QualityIndicator", :parents => ["UML::Float"], :documentation => "Documentation:\nIndicates the quality of a determined value, where 0 means lowest quality and 1 means high quality w.r.t. to the validity level." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "ReferencedVersion", :parents => ["BICEPS_ParticipantModel::VersionCounter"], :documentation => "Documentation:\nIn contrast to pm:VersionCounter, ReferencedVersion does not represent a version of an MDIB object, but a reference to a particular version of an MDIB object." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "RootType_1", :parents => ["UML::Uri"]
    primitive "SymbolicCodeName", :parents => ["UML::String"], :documentation => "Documentation:\nSymbolicCodeName is a symbolic, programmatic form of a pm:CodeIdentifier term.\n\nNOTE—SymbolicCodeName is the equivalent of the Reference ID attribute that is defined in IEEE 11073-10101." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "TimeZone", :parents => ["UML::String"], :documentation => "Documentation:\nTimeZone describes the time zone and DST setting of a clock in POSIX format (ISO/IEC/IEEE 9945).\n\nExample: CST6CDT,M3.2.0/2:00:00,M11.1.0/2:00:00, which would effect a change to daylight saving time at 2:00 AM on the second Sunday in March and change back at 2:00 AM on the first Sunday in November, and keep 6 hours time offset from GMT every year." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "Timestamp", :parents => ["UML::Integer"], :documentation => "Documentation:\nAn unsigned 64-bit integer value that represents a timestamp.\n\t\t\t\n__R5001: A timestamp SHALL count the milliseconds between the current time and midnight, January 1, 1970 UTC without leap seconds.__\n\n__R5002: Timestamps are an optional feature of the MDIB. If anywhere in the MDIB a timestamp is used, the SERVICE PROVIDER SHALL provide a pm:ClockDescriptor ELEMENT.__\n\nNOTE 1—Typically all systems assume that a day has 86400 seconds.\nNOTE 2—While the unit of time of pm:Timestamp is a millisecond, the granularity of the value depends on the hardware/software system and might be larger (e.g., tens of milliseconds)." do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
    primitive "VersionCounter", :parents => ["UML::Integer"], :documentation => "Documentation:\nA version counter to provide versionized MDIB objects. The initial value of a version counter SHALL be \"0\".\n\n__R5003: VersionCounter values SHALL never be decremented.__" do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
  end
  package "ExtensionPoint" do
    klass "ExtensionType" do
      applied_stereotype :instance_of => "XML_Schema::Root"
    end
  end
  package "XML_Schema", :id => "_18_0_2_b9202e7_1509465344604_841107_3989" do
    stereotype "Root", :metaclasses => ["Class"], :id => "_18_0_2_b9202e7_1509465344609_957574_3996"
    stereotype "XML_Schema", :metaclasses => ["Package"], :id => "_18_0_2_b9202e7_1509465344606_390280_3992" do
      tag "targetNamespace", :id => "_18_0_2_b9202e7_1509465344616_478810_4014"
      tag "prefix", :id => "_18_0_2_b9202e7_1509465344617_466981_4016"
      tag "use_collection_containers", :id => "_18_0_2_b9202e7_1509465344617_926231_4017"
      tag "always_keep_compositions", :id => "_18_0_2_b9202e7_1509465344617_609527_4018"
      tag "object_id_only_if_referenced", :id => "_18_0_2_b9202e7_1509465344617_979349_4019"
      tag "append_id_to_reference_names", :id => "_18_0_2_b9202e7_1509465344617_28095_4020"
      tag "id_attribute_name", :id => "_18_0_2_b9202e7_1509465344617_736616_4021"
      tag "enforce_case_conventions", :id => "_18_0_2_b9202e7_1509465344617_449467_4022"
      tag "append_type_to_type_names", :id => "_18_0_2_b9202e7_1509465344617_741774_4023"
      tag "uppercase_attribute_names", :id => "_18_0_2_b9202e7_1509465344617_935249_4024"
      tag "include_documentation_annotations", :id => "_18_0_2_b9202e7_1509465344618_653900_4025"
      tag "documentation_inline", :id => "_18_0_2_b9202e7_1509465344618_870725_4026"
      tag "id_suffix_for_references", :id => "_18_0_2_b9202e7_1509465344618_911225_4027"
      tag "full_version", :id => "_18_0_2_b9202e7_1509465344618_221980_4028"
      tag "schema_filename", :id => "_18_0_2_b9202e7_1509465344618_798418_4029"
      tag "major_version", :id => "_18_0_2_b9202e7_1509465344618_225627_4030"
      tag "use_idrefs", :id => "_18_0_2_b9202e7_1509465344618_389188_4031"
      tag "id_suffix_for_plural_references", :id => "_18_0_2_b9202e7_1509465344618_543920_4032"
      tag "generate_attributes_as_elements", :id => "_18_0_2_b9202e7_1509465344618_194782_4033"
    end
    stereotype "extendable", :metaclasses => ["Interface"], :id => "_18_0_2_b9202e7_1509465344610_924890_3997"
    stereotype "partOfGroup", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_595438_4003" do
      tag "attributeGroup", :id => "_18_0_2_b9202e7_1509465344618_194782_4034"
    end
    stereotype "referenceElement", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_918780_4000"
    stereotype "simpleContent", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_795518_3998"
    stereotype "xmlElement", :metaclasses => ["Property"], :id => "_18_0_2_b9202e7_1509465344610_595438_4002"
  end
  profile "Gui_Builder_Profile", :id => "_9_5_1_f2e0365_1132085180552_532309_18941" do
    applied_stereotype :instance_of => "Gui_Builder_Profile::options"
    association :properties => ["_17_0_4_2_78e0236_1386346399057_474802_3400", "Gui_Builder_Profile::File::binary_data"], :id => "_17_0_4_2_78e0236_1386346399057_554755_3399"
    association :properties => ["Gui_Builder_Profile::InvitationCode::additional_email_requirements", "Gui_Builder_Profile::StringCondition::additional_email_requirement_for"], :id => "_16_9_78e0236_1362444527650_795906_1494"
    association :properties => ["Gui_Builder_Profile::InvitationCode::roles_granted", "Gui_Builder_Profile::UserRole::invitations"], :association_class => "Gui_Builder_Profile::GrantedPermissions", :id => "ac__16_9_78e0236_1361655331501_974661_1486"
    association :properties => ["_18_0_2_6340208_1440773581304_321556_4138", "_18_0_2_6340208_1440773581305_932128_4139"], :id => "_18_0_2_6340208_1440773581304_487621_4137"
    association :properties => ["_18_0_2_6340208_1437670247479_848660_4283", "_18_0_2_6340208_1437670247479_258317_4284"], :id => "_18_0_2_6340208_1437670247479_960929_4282"
    association :properties => ["_16_9_78e0236_1362000053868_346349_1709", "_16_9_78e0236_1362000053869_30585_1710"], :id => "_16_9_78e0236_1362000053868_440794_1708"
    association :properties => ["Gui_Builder_Profile::Perspective::substitutions", "_16_9_78e0236_1361999994974_761045_1678"], :id => "_16_9_78e0236_1361999994973_620448_1676"
    association :properties => ["Gui_Builder_Profile::ProjectOptions::email_requirements", "Gui_Builder_Profile::StringCondition::email_requirement_for"], :id => "_16_9_78e0236_1362435833928_585337_1539"
    association :properties => ["Gui_Builder_Profile::ProjectOptions::password_requirements", "Gui_Builder_Profile::StringCondition::password_requirement_for"], :id => "_16_9_78e0236_1362435861830_956872_1548"
    association :properties => ["Gui_Builder_Profile::RichText::images", "_17_0_4_2_78e0236_1386346294429_948280_3299"], :id => "_17_0_4_2_78e0236_1386346294427_421114_3296"
    association :properties => ["_16_9_78e0236_1362000016908_686909_1688", "_16_9_78e0236_1362000016909_933287_1689"], :id => "_16_9_78e0236_1362000016908_599111_1687"
    association :properties => ["Gui_Builder_Profile::User::roles", "Gui_Builder_Profile::UserRole::users"], :association_class => "Gui_Builder_Profile::RolePermissions", :id => "ac__16_9_78e0236_1361377547806_696994_1711"
    klass "BinaryData", :id => "_16_9_28b0142_1350320910064_324080_1403" do
      property nil, :type => "Gui_Builder_Profile::File", :id => "_17_0_4_2_78e0236_1386346399057_474802_3400"
      property "data", :type => "UML::ByteString", :id => "_16_9_28b0142_1350322818987_206540_1405"
    end
    klass "Code", :id => "_16_9_28b0142_1305737761267_4412_1473" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String", :id => "_16_9_28b0142_1345232682459_819388_1563"
      property "language", :type => "Gui_Builder_Profile::LanguageType", :id => "_16_9_28b0142_1305737768320_72319_1474"
    end
    klass "File", :id => "_16_9_6620216_1310677254545_658641_1463" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "binary_data", :type => "Gui_Builder_Profile::BinaryData", :aggregation => :composite, :id => "_16_9_6620216_1310677254546_719122_1464"
      property "filename", :type => "UML::String", :id => "_16_9_6620216_1310677254546_312818_1465"
      property "mime_type", :type => "UML::String", :id => "_16_9_6620216_1310677254546_544129_1466"
    end
    klass "GrantedPermissions", :id => "_16_9_78e0236_1361655331501_974661_1486" do
      property "role_manager", :type => "UML::Boolean", :id => "_16_9_78e0236_1361655366955_945029_1526"
    end
    klass "InvitationCode", :id => "_16_9_78e0236_1361654692810_715969_1432" do
      property nil, :type => "Gui_Builder_Profile::Organization", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1440773581305_932128_4139"
      property nil, :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000053869_30585_1710"
      property "additional_email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362444527651_178908_1495"
      property "code", :type => "UML::String", :id => "_16_9_78e0236_1361654722176_380927_1450"
      property "expires", :type => "Gui_Builder_Profile::Timestamp", :id => "_16_9_78e0236_1361654732105_93745_1452"
      property "roles_granted", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361654888054_544647_1461"
      property "uses_remaining", :type => "UML::Integer", :id => "_16_9_78e0236_1361654751742_918976_1454"
    end
    klass "MultivocabularySubstitution", :id => "_16_9_6620216_1311349467640_10706_1828" do
      property nil, :type => "Gui_Builder_Profile::Perspective", :lower => 1, :id => "_16_9_78e0236_1361999994974_761045_1678"
      property "master_word", :type => "UML::String", :id => "_16_9_6620216_1311349486021_149646_1829"
      property "replacement_word", :type => "UML::String", :id => "_16_9_6620216_1311349518319_893941_1830"
    end
    klass "Organization", :id => "_18_0_2_6340208_1437670007691_142464_4253" do
      property nil, :type => "Gui_Builder_Profile::InvitationCode", :aggregation => :shared, :upper => Float::INFINITY, :id => "_18_0_2_6340208_1440773581304_321556_4138"
      property nil, :type => "Gui_Builder_Profile::Person", :aggregation => :shared, :upper => Float::INFINITY, :id => "_18_0_2_6340208_1437670247479_848660_4283"
      property "description", :type => "Gui_Builder_Profile::RichText", :id => "_18_0_2_6340208_1437670022410_877209_4276"
      property "name", :type => "UML::String", :id => "_18_0_2_6340208_1437670018818_673294_4274"
      property "org_user_limit", :type => "UML::Integer", :default_value => 2, :id => "_18_0_4_f920375_1482167552396_757856_4251"
    end
    klass "Person", :id => "_17_0_2_4_b9202e7_1402410462540_930340_2171" do
      property nil, :type => "Gui_Builder_Profile::Organization", :upper => Float::INFINITY, :id => "_18_0_2_6340208_1437670247479_258317_4284"
      property "email", :type => "UML::String", :id => "_16_9_78e0236_1361378907971_285264_1915"
      property "email_verified", :type => "UML::Boolean", :id => "_16_9_78e0236_1361378935636_408890_1917"
      property "first_name", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1402410477887_979887_2190"
      property "last_name", :type => "UML::String", :id => "_17_0_2_4_b9202e7_1402410490650_559652_2192"
    end
    klass "Perspective", :id => "_16_9_78e0236_1361999978172_908613_1658" do
      property nil, :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000053868_346349_1709"
      property nil, :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000016909_933287_1689"
      property "name", :type => "UML::String", :id => "_16_9_78e0236_1362000125819_873673_1725"
      property "substitutions", :type => "Gui_Builder_Profile::MultivocabularySubstitution", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1361999994973_454878_1677"
    end
    klass "ProjectOptions", :id => "_16_9_78e0236_1361379064628_70918_1919" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::singleton"
      property "email_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362435833929_936829_1540"
      property "password_requirements", :type => "Gui_Builder_Profile::StringCondition", :aggregation => :composite, :upper => Float::INFINITY, :id => "_16_9_78e0236_1362435861830_763541_1549"
      property "user_registration", :type => "Gui_Builder_Profile::UserRegistrationType", :default_value => "Open", :id => "_16_9_78e0236_1361379249201_791722_1950"
    end
    klass "RegularExpressionCondition", :parents => ["Gui_Builder_Profile::StringCondition"], :id => "_16_9_78e0236_1362435709826_262438_1513" do
      property "regular_expression", :type => "Gui_Builder_Profile::RegularExpression", :id => "_16_9_78e0236_1362435738776_707179_1516"
    end
    klass "RichText", :id => "_17_0_4_2_78e0236_1386346294425_836962_3291" do
      applied_stereotype :instance_of => "Gui_Builder_Profile::complex_attribute"
      property "content", :type => "UML::String", :id => "_17_0_4_2_78e0236_1386346294426_434823_3293"
      property "images", :type => "Gui_Builder_Profile::RichTextImage", :aggregation => :composite, :upper => Float::INFINITY, :id => "_17_0_4_2_78e0236_1386346294428_392424_3297"
      property "markup_language", :type => "Gui_Builder_Profile::MarkupType", :id => "_17_0_4_2_78e0236_1386346294427_626186_3294"
    end
    klass "RichTextImage", :id => "_17_0_4_2_78e0236_1386346294425_790237_3292" do
      property nil, :type => "Gui_Builder_Profile::RichText", :id => "_17_0_4_2_78e0236_1386346294429_948280_3299"
      property "image", :type => "Gui_Builder_Profile::File", :id => "_17_0_4_2_78e0236_1386346294427_443952_3295"
    end
    klass "RolePermissions", :id => "_16_9_78e0236_1361377547806_696994_1711" do
      property "role_manager", :type => "UML::Boolean", :id => "_16_9_78e0236_1361377597251_847958_1743"
    end
    klass "StringCondition", :id => "_16_9_78e0236_1362435638076_723054_1493", :is_abstract => true do
      property "additional_email_requirement_for", :type => "Gui_Builder_Profile::InvitationCode", :id => "_16_9_78e0236_1362444527652_678514_1496"
      property "description", :type => "UML::String", :id => "_16_9_78e0236_1362439385411_917299_1696"
      property "email_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions", :id => "_16_9_78e0236_1362435833929_239766_1541"
      property "failure_message", :type => "UML::String", :id => "_16_9_78e0236_1362435763496_769708_1518"
      property "password_requirement_for", :type => "Gui_Builder_Profile::ProjectOptions", :id => "_16_9_78e0236_1362435861830_154310_1550"
    end
    klass "User", :parents => ["Gui_Builder_Profile::Person"], :id => "_16_9_6620216_1311349088211_41010_1823" do
      property nil, :type => "Gui_Builder_Profile::Perspective", :upper => Float::INFINITY, :id => "_16_9_78e0236_1362000016908_686909_1688"
      property "email_confirmation_token", :type => "UML::String", :id => "_18_0_2_f920375_1426774368237_166134_4251"
      property "login", :type => "UML::String", :id => "_16_9_6620216_1311349134111_355250_1824"
      property "password_hash", :type => "UML::String", :id => "_16_9_6620216_1311349178120_340215_1825"
      property "password_reset_time_limit", :type => "Gui_Builder_Profile::Timestamp", :id => "_18_0_2_f920375_1426623768994_451284_4139"
      property "password_reset_token", :type => "UML::String", :id => "_18_0_2_f920375_1426623641990_599140_4137"
      property "roles", :type => "Gui_Builder_Profile::UserRole", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361377541576_508361_1703"
      property "salt", :type => "UML::String", :id => "_16_9_6620216_1311349196390_496663_1826"
      property "use_accessibility", :type => "UML::Boolean", :id => "_16_9_6620216_1311349222322_790429_1827"
    end
    klass "UserRole", :id => "_16_9_6620216_1311349580039_47554_1831" do
      property "invitations", :type => "Gui_Builder_Profile::InvitationCode", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361654888054_232347_1462"
      property "name", :type => "UML::String", :id => "_16_9_6620216_1311349591342_46442_1832"
      property "registration", :type => "Gui_Builder_Profile::RoleRegistrationType", :default_value => "Role Manager", :id => "_16_9_78e0236_1361377688965_687613_1769"
      property "users", :type => "Gui_Builder_Profile::User", :upper => Float::INFINITY, :id => "_16_9_78e0236_1361377541576_939654_1704"
    end
    enumeration "FacadeImplementationLanguages", :id => "_16_8_6620216_1283178501783_51776_1249" do
      property "value", :type => "UML::String"
      literal "Java", :id => "_16_8_6620216_1283178557628_106569_1250"
      literal "Ruby", :id => "_16_8_6620216_1283178566324_155772_1251"
    end
    enumeration "LanguageType", :id => "_16_9_28b0142_1305737723715_669375_1467" do
      property "value", :type => "UML::String"
      literal "Clojure", :id => "_16_9_28b0142_1305737723716_562372_1470"
      literal "Groovy", :id => "_16_9_28b0142_1305737723716_180112_1471"
      literal "Javascript", :id => "_16_9_28b0142_1305737723716_116462_1469"
      literal "Python", :id => "_16_9_28b0142_1305737723717_525399_1472"
      literal "Ruby", :id => "_16_9_28b0142_1305737723715_442867_1468"
    end
    enumeration "MarkupType", :id => "_17_0_4_2_78e0236_1386346253949_528969_3262" do
      property "value", :type => "UML::String"
      literal "HTML", :id => "_17_0_4_2_78e0236_1386346253951_61218_3266"
      literal "Kramdown", :id => "_18_0_2_6340208_1461702006551_892495_4251"
      literal "LaTeX", :id => "_17_0_4_2_78e0236_1386346253951_848506_3264"
      literal "Markdown", :id => "_17_0_4_2_78e0236_1386346253951_125387_3263"
      literal "Plain", :id => "_17_0_4_2_78e0236_1386346253951_470997_3265"
      literal "Textile", :id => "_17_0_4_2_78e0236_1386346253951_18150_3267"
    end
    enumeration "RoleRegistrationType", :id => "_16_9_78e0236_1361377755013_916086_1790" do
      property "value", :type => "UML::String"
      literal "Administrator", :id => "_16_9_78e0236_1361377828577_385709_1816"
      literal "Open", :id => "_16_9_78e0236_1361377757085_493086_1808"
      literal "Role", :id => "_16_9_78e0236_1361377761232_512485_1810"
      literal "Role Manager", :id => "_16_9_78e0236_1361377824614_582502_1814"
    end
    enumeration "UserRegistrationType", :id => "_16_9_78e0236_1361379303537_735379_1975" do
      property "value", :type => "UML::String"
      literal "Email", :id => "_16_9_78e0236_1361379315080_235305_1995"
      literal "Invitation", :id => "_16_9_78e0236_1361379375691_302758_1997"
      literal "Open", :id => "_16_9_78e0236_1361379305457_933520_1993"
    end
    primitive "BigString", :parents => ["UML::String"], :id => "_18_0_2_b9202e7_1428075370972_160109_4275"
    primitive "Date", :id => "_18_0_2_b9202e7_1428076910048_980742_4348"
    primitive "RegularExpression", :parents => ["UML::String"], :id => "_16_9_78e0236_1361379717284_94123_2076"
    primitive "Time", :id => "_18_0_2_b9202e7_1428076858153_303327_4329"
    primitive "Timestamp", :id => "_18_0_2_b9202e7_1428076787996_461235_4311"
    stereotype "change_tracked", :metaclasses => ["Package"], :id => "_16_9_28b0142_1320261650047_288289_1577"
    stereotype "complex_attribute", :metaclasses => ["Class"], :id => "_16_9_28b0142_1350587904204_746275_1536"
    stereotype "facade", :metaclasses => ["Interface"], :id => "_16_6_2_28b0142_1276094884676_922858_351"
    stereotype "options", :metaclasses => ["Package"], :id => "_16_9_28b0142_1284660904944_537040_1463" do
      tag "application_options", :id => "_18_0_2_b9202e7_1491226601808_704924_4254"
      tag "plugins", :id => "_18_0_2_b9202e7_1491226308408_96569_4251", :documentation => "A list of additional plugins that the generated code should include in the generated models.  Example: the inclusion of the :json_serializer plugin for Ruby Sequel models.  This string should be a list of plugins that are either comma, semicolon, or space seperated."
      tag "project_name", :id => "_16_9_78e0236_1366130322652_436960_1623"
      tag "require", :id => "_16_9_28b0142_1284660999929_840326_1469"
      tag "version", :id => "_16_9_6620216_1311350018593_840420_1847"
    end
    stereotype "root", :metaclasses => ["Class"], :id => "_16_6_2_28b0142_1276094884676_130193_349" do
      tag "Root Name", :id => "_16_6_2_28b0142_1276094884694_127513_360"
    end
    stereotype "singleton", :metaclasses => ["Class"], :id => "_16_9_78e0236_1361379210582_818602_1938"
  end
end
