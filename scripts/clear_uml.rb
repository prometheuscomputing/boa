def clear_uml
  (UmlMetamodel.classes(:no_imports => true)).each do |c|
    next unless c < Sequel::Model
    next if c.enumeration?
    if DB.tables.include?(c.table_name)
      c.delete
    end
  end
  nil
end
