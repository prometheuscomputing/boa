load File.join(__dir__, 'dsl.rb')
load File.join(__dir__, 'clear_uml.rb')
t = Time.now
clear_uml
# dsl_file = File.join(__dir__, '../test_data/CarExample-2.7.2.rb')
dsl_file = File.join(__dir__, '../test_data/BICEPS_ParticipantModelProject_0.6.3.rb')
Boa.from_dsl_file(dsl_file)
puts "Loading model took #{Time.now - t} seconds."
